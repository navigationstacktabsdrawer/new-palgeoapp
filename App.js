import React from 'react';
import {
  View,
  Text,
  Image,
  StyleSheet,
  StatusBar,
  Platform,
  PermissionsAndroid,
  AppState,
  BackHandler,
  LogBox,
} from 'react-native';
import AppIntroSlider from 'react-native-app-intro-slider';
import {NavigationContainer} from '@react-navigation/native';
import AsyncStorage from '@react-native-community/async-storage';
import {Root} from 'native-base';
import MainNavigator from './src/Navigation/MainNavigator';
import {navigationRef} from './src/components/RootNavigation';
import SplashScreen from 'react-native-splash-screen';
import VersionNumber from 'react-native-version-number';
import Const from './src/components/common/Constants';
import AskForUpdate from './src/components/common/AskForUpdate';
import WifiManager from 'react-native-wifi-reborn';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import Geolocation from 'react-native-geolocation-service';
import _BackgroundTimer from 'react-native-background-timer';
import OneSignal from 'react-native-onesignal';
import * as RootNavigation from './src/components/RootNavigation';
//import RNDisableBatteryOptimizationsAndroid from 'react-native-disable-battery-optimizations-android';
import GPSState from 'react-native-gps-state';
import axios from 'axios';
import googleFit, {Scopes} from 'react-native-google-fit';
import ProgressCircle from 'react-native-progress-circle';
var moment = require('moment');
import SpInAppUpdates, {
  UPDATE_TYPE,
  NeedsUpdateResponseAndroid,
  SemverVersion,
  NeedsUpdateResponse,
  IncomingStatusUpdateEvent,
  IAUUpdateKind,
} from 'sp-react-native-in-app-updates';
import AskForPermissions from './src/components/common/AskForPermissions';

import NetInfo from '@react-native-community/netinfo';
import {addTask, update_task} from './src/utils/helperFunctions';
import VersionCheck from 'react-native-version-check';
import CustomModal from './src/components/common/CustomModal';
import CustomLabel from './src/components/common/CustomLabel';
import {Linking} from 'react-native';
import {Alert} from 'react-native';
import ReactNativeForegroundService from '@supersami/rn-foreground-service';

//import AndroidWhitelist from 'react-native-android-whitelist';

const CONFIG_WHITELIST = {
  title: 'Android Whitelist',
  text: 'To ensure timely delivery of push notifications, please whitelist our app.',
  doNotShowAgainText: 'Do not show again',
  positiveText: 'Whitelist',
  negativeText: 'Cancel',
};

const HIGH_PRIORITY_UPDATE = 5;
const slides = [
  {
    key: '1',
    title: 'Welcome to PalGeo',
    text: 'Geofencing attendance system',
    image: require('./src/assets/intro1.png'),
    backgroundColor: '#59b2ab',
  },
  {
    key: '2',
    title: 'Secured Checkin',
    text: 'Checkin securely through face evaluation from location specified',
    image: require('./src/assets/intro2.png'),
    backgroundColor: '#59b2ab',
  },
  {
    key: '3',
    title: 'Staff Tasks & Reports',
    text: 'Get Staff Reports and Day to day tasks',
    image: require('./src/assets/intro3.png'),
    backgroundColor: '#febe29',
  },
  // {
  //   key: '4',
  //   title: 'Auto Startup',
  //   text: 'Allow the app to run in background even on phone reboot',
  //   image: require('./src/assets/intro3.png'),
  //   backgroundColor: '#febe29',
  // },
];
export default class App extends React.Component {
  constructor(props) {
    super(props);
    this.inAppUpdates = new SpInAppUpdates();
    this.state = {
      showRealApp: false,
      appIntro: '',
      newVersionRequired: false,
      inactive: true,
      position: {},
      bearer_token: '',
      show: false,
      locationPermissions: false,
      gpsInfo: [],
      netInfo: {},
      isOn: true,
      needUpdate: false,
      otherData: null,
      showUpdateModal: false,
      bytesDownloaded: 0,
      totalBytesToDownload: 0,
      percent: 0,
    };
    this.ConfigureOneSignal();
  }

  async componentDidMount() {
    await this.updateStatus(true, new Date());
    const bearer_token = await AsyncStorage.getItem('bearer_token');
    const institute_id = await AsyncStorage.getItem('institute_id');
    console.log('institute_id = ', institute_id);
    const StaffNo = await AsyncStorage.getItem('user_id');
    const geoOld = await AsyncStorage.getItem('geo_id');
    //const autoStart = await AsyncStorage.getItem('autoStart');
    console.disableYellowBox = true;
    //LogBox.ignoreAllLogs(true)
    // RNDisableBatteryOptimizationsAndroid.isBatteryOptimizationEnabled().then(
    //   (isEnabled) => {
    //     if (isEnabled) {
    //       RNDisableBatteryOptimizationsAndroid.openBatteryModal();
    //     }
    //     // AndroidWhitelist.alert(CONFIG_WHITELIST);
    //   },
    // );

    fetch(Const + 'api/Staff/IsCheckedInNew', {
      method: 'POST',
      withCredentials: true,
      credentials: 'include',
      headers: {
        Authorization: bearer_token,
        Accept: 'text/plain',
        'Content-Type': 'application/json-patch+json',
      },
      body: JSON.stringify({
        staffCode: StaffNo,
        instituteId: Number(institute_id),
      }),
    })
      .then((response) => response.json())
      .then(async (json) => {
        console.log('jOSN=-==', json);
        if (json.isCheckedIn && json.locationDetails.type === 'Circle') {
          await AsyncStorage.setItem('checked_out', 'no');
          if (geoOld) {
            return;
          }
          await AsyncStorage.setItem(
            'radius',
            json.locationDetails.radius.toString(),
          );
          await AsyncStorage.setItem(
            'geo_id',
            json.locationDetails.id.toString(),
          );
          await AsyncStorage.setItem(
            'coordinates',
            JSON.stringify(json.locationDetails.coordinates),
          );
        }
      })
      .catch((error) => console.log('error_IS_CHECKED_IN =>', error));
    SplashScreen.hide();
    const checked_out = await AsyncStorage.getItem('checked_out');
    AsyncStorage.getItem('locationPermissions').then((locationPermissions) => {
      console.log('locationPermissions ==>', locationPermissions);
      this.setState({locationPermissions: locationPermissions}, () => {
        if (
          this.state.locationPermissions === 'yes' ||
          this.state.locationPermissions
        ) {
          console.log('permissions granted');
        }
      });
    });
    this.unsubscribe = NetInfo.addEventListener(this.checkInternet);
    AppState.addEventListener('change', this.checkUserActivity);

    GPSState.addListener(this.checkGpsStatus);

    this.checkUpdateVersion();
    AsyncStorage.getItem('appIntro').then((appIntro) => {
      console.log('appIntro', appIntro);
      this.setState({appIntro: appIntro});
    });
  }

  checkInternet = async (state) => {
    let currentState = this.state.isOn;
    if (currentState !== state.isConnected) {
      currentState = state.isConnected;
    }
    const StaffNo = await AsyncStorage.getItem('user_id');
    const institute_id = await AsyncStorage.getItem('institute_id');
    const checked_out = await AsyncStorage.getItem('checked_out');
    // console.log('StaffNo', StaffNo);
    console.log('Is connected?', currentState);
    this.setState({isOn: state.isConnected});
    if (checked_out === 'yes' || !checked_out) {
      return;
    }
    const run = ReactNativeForegroundService.is_running();
    if (run && checked_out === 'no' && currentState) {
      update_task();
    }

    if (!currentState) {
      if (AppState.currentState === 'active') {
        Alert.alert(
          'Attention',
          'You should never turn off your internet connection during login hours!',
          [{text: 'OK', onPress: () => {}, style: 'cancel'}],
          {cancelable: true},
        );
      }
      console.log('I ran without internet connection dude!!');
      let temp = {
        AccessTime: new Date().toUTCString(),
        IsOn: currentState,
        StaffCode: StaffNo,
      };
      await AsyncStorage.setItem('netInfo', JSON.stringify(temp)).then(() =>
        this.setState({netInfo: temp}),
      );
      //return;
    } else {
      if (currentState && StaffNo && institute_id) {
        const gpsInfo = JSON.parse(await AsyncStorage.getItem('gpsInfo'));
        if (gpsInfo?.length > 0) {
          try {
            return axios
              .post(Const + 'api/Staff/MobileGpsOnOrOffOffline', {
                StaffCode: StaffNo,
                InstituteId: Number(institute_id),
                Data: gpsInfo,
              })
              .then((response) => {
                this.setState({gpsInfo: []});
                AsyncStorage.setItem('gpsInfo', JSON.stringify([]));
                console.log('response ==>', response.data);
                return;
                //this.unsubscribe();
              })
              .catch((err) => console.log('error_GPS', err));
          } catch (error) {
            console.log('error_GPS_INFO ==>', error);
          }
        } else {
          const netInfo = JSON.parse(await AsyncStorage.getItem('netInfo'));
          let t = netInfo;
          console.log('nnetInfo ==>', t);
          if (netInfo === {} || !netInfo) {
            return;
          }
          if (netInfo && netInfo != {}) {
            let temp1 = {
              AccessTime: new Date().toUTCString(),
              IsOn: true,
              StaffCode: StaffNo,
            };
            console.log('I ran dude!');
            try {
              const response = await axios.post(
                Const + 'api/Staff/MobileInternetOnOrOff',
                {
                  StaffCode: StaffNo,
                  InstituteId: Number(institute_id),
                  Data: [netInfo, temp1],
                },
              );
              this.setState({netInfo: {}});
              await AsyncStorage.setItem('netInfo', JSON.stringify({}));
            } catch (error) {
              console.log('error_NETINFO ==>', error);
            }
          }
        }
      }
    }
  };
  checkGpsStatus = async (status) => {
    const StaffNo = await AsyncStorage.getItem('user_id');
    const institute_id = await AsyncStorage.getItem('institute_id');
    const checked_out = await AsyncStorage.getItem('checked_out');
    console.log('GPS_state', status, checked_out);
    if (checked_out === 'yes' || !checked_out) {
      return;
    }
    const run = ReactNativeForegroundService.is_running();
    if (run && checked_out === 'no' && this.state.isOn) {
      if (status === 3 || status === 4) {
        update_task();
      }
    }
    if (status === 1 || status === 2) {
      if (AppState.currentState === 'active') {
        Alert.alert(
          'Attention',
          'You should never turn off your GPS connection during login hours!',
          [{text: 'OK', onPress: () => {}, style: 'cancel'}],
          {cancelable: true},
        );
      }
    }

    if (!this.state.isOn) {
      this.setState(
        {
          gpsInfo: [
            ...this.state.gpsInfo,
            {
              AccessTime: new Date().toUTCString(),
              IsGpsOn: status == 1 || status == 2 ? false : true,
              StaffCode: StaffNo,
            },
          ],
        },
        () => {
          const uniques = Object.values(
            this.state.gpsInfo.reduce((a, c) => {
              a[c.AccessTime] = c;
              return a;
            }, {}),
          );
          AsyncStorage.setItem('gpsInfo', JSON.stringify(uniques));
          return console.log('GPS)INFO', uniques);
        },
      );
      return;
    }

    //console.log(institute_id);
    //call api
    console.log('change in gps state');
    try {
      return axios
        .post(Const + 'api/Staff/MobileGpsOnOrOff', {
          StaffCode: StaffNo,
          InstituteId: Number(institute_id),
          IsGpsOn: status == 1 || status == 2 ? false : true,
        })
        .then((response) => console.log('JSON_GPS', response.data));
    } catch (error) {
      console.log('error_NET_ON_GPSERROR ==>', error);
    }
  };
  checkUserActivity = async (state) => {
    //console.log('app', state);
    const prevState = await AsyncStorage.getItem('app_status');
    const time = await AsyncStorage.getItem('app_change_time');
    //console.log('step 1', prevState);
    if (state !== prevState) {
      let now = moment().format('YYYY-MM-DD HH:mm:ss');

      let then = moment(new Date(time)).format('YYYY-MM-DD HH:mm:ss');

      var ms = moment(now, 'YYYY-MM-DD HH:mm:ss').diff(
        moment(then, 'YYYY-MM-DD HH:mm:ss'),
      );

      var minutes = moment.duration(ms).asMinutes();
      //console.log('duration', minutes);
      // if (parseInt(minutes) >= 30 && state === 'active') {
      //   await AsyncStorage.setItem('bearer_token', '');
      //   RootNavigation.navigate('MainNavigator');
      // }
      // if (parseInt(minutes) >= 3 && state === 'active') {
      //   await this.updateStatus(false, time);
      // }
    }
    await AsyncStorage.setItem('app_status', state);
    await AsyncStorage.setItem('app_change_time', new Date().toString());

    //console.log('app_status_previous', prevState, time);
  };

  successLocation = (position) => {
    this.setState({position});
    //console.log('accuracy_watch', position[0]);
  };

  errorLocation = (error) => console.log(error);

  async componentWillUnmount() {
    const time = new Date();
    await this.updateStatus(false, time);
    OneSignal.removeEventListener('ids', this.onIds);
    OneSignal.removeEventListener('received', this.onReceived);
    OneSignal.removeEventListener('opened', this.onOpened);
    AppState.removeEventListener('change', this.checkUserActivity);

    if (this.unsubscribe) {
      this.unsubscribe();
      this.unsubscribe = null;
    }
    GPSState.removeListener(() => {
      console.log('GPS_state');
    });
  }
  ConfigureOneSignal = () => {
    OneSignal.setLogLevel(7, 0);
    OneSignal.inFocusDisplaying(2);
    OneSignal.init('cb52438d-c790-46e4-83de-effe08725aff', {
      kOSSettingsKeyAutoPrompt: true,
      kOSSettingsKeyInAppLaunchURL: false,
      kOSSettingsKeyInFocusDisplayOption: 2,
    });
    OneSignal.configure({});
    OneSignal.addEventListener('ids', this.onIds);
    OneSignal.addEventListener('received', this.onReceived);
    OneSignal.addEventListener('opened', this.onOpened);
  };

  updateStatus = async (app_state_now, time) => {
    console.log('app_status_now', app_state_now);
    const checked_out = await AsyncStorage.getItem('checked_out');
    const bearer_token = await AsyncStorage.getItem('bearer_token');
    const StaffNo = await AsyncStorage.getItem('user_id');
    const institute_id = await AsyncStorage.getItem('institute_id');
    const isFaceRequired = await AsyncStorage.getItem('isFaceRequired');
    //const app_status = await AsyncStorage.getItem('app_status');
    if (isFaceRequired === 'true') {
      if (checked_out === 'yes' || !checked_out) {
        return;
      }
      try {
        fetch(Const + 'api/Staff/UpdateStaffMobileAppActiveInactiveStatus', {
          method: 'POST',
          headers: {
            Authorization: 'Bearer ' + bearer_token,
            Accept: 'application/json, text/plain',
            'Content-Type': 'application/json-patch+json',
          },
          body: JSON.stringify({
            StaffCode: StaffNo,
            InstituteId: Number(institute_id),
            IsActive: app_state_now,
            UpdateTime: new Date(time).toUTCString(),
          }),
        }).then(() => AsyncStorage.setItem('app_status', 'active'));
      } catch (error) {
        console.log('error', error);
      }

      return;
    }
    if (isFaceRequired === 'false') {
      try {
        fetch(Const + 'api/Staff/UpdateStaffMobileAppActiveInactiveStatus', {
          method: 'POST',
          headers: {
            Authorization: 'Bearer ' + bearer_token,
            Accept: 'application/json, text/plain',
            'Content-Type': 'application/json-patch+json',
          },
          body: JSON.stringify({
            StaffCode: StaffNo,
            InstituteId: Number(institute_id),
            IsActive: app_state_now,
            UpdateTime: new Date(time).toUTCString(),
          }),
        }).then(() => AsyncStorage.setItem('app_status', 'active'));
      } catch (error) {
        console.log('error', error);
      }
      return;
    }
  };

  onIds(device) {
    try {
      AsyncStorage.setItem('device_token', device.userId);
      console.log('[device_token]', device.userId);
    } catch (e) {
      console.log(e);
    }
  }
  async onReceived(notification) {
    console.log(
      'Notification received: ',
      JSON.stringify(notification, null, 2),
    );
    try {
      const response = await fetch(Const + 'api/MobileApp/UpdateNotification', {
        method: 'POST',
        headers: {
          Accept: 'text/plain',
          'Content-Type': 'application/json-patch+json',
        },
        body: JSON.stringify({
          NotificationId: notification.payload.NotificationID,
          IsReceived: true,
          IsRead: false,
        }),
      });
      //RootNavigation.navigate('CircularList', {data: 'push'});
    } catch (error) {
      console.log('error', error);
    }
  }
  async onOpened(openResult) {
    console.log('opened', openResult);
    try {
      const response = await fetch(Const + 'api/MobileApp/UpdateNotification', {
        method: 'POST',
        headers: {
          Accept: 'text/plain',
          'Content-Type': 'application/json-patch+json',
        },
        body: JSON.stringify({
          NotificationId: openResult.notification.payload.NotificationID,
          IsReceived: true,
          IsRead: true,
        }),
      });
      RootNavigation.navigate('CircularList', {data: 'push'});
    } catch (error) {
      console.log('error', error);
    }
  }

  _renderItem = ({item}) => {
    return (
      <View style={styles.slide}>
        <View>
          <Image
            source={item.image}
            resizeMode="contain"
            style={styles.image}
          />
        </View>
        <View style={styles.headingContainer}>
          <Text style={styles.heading}>{item.title}</Text>
        </View>
        <View style={styles.descriptionContainer}>
          <Text style={styles.description}>{item.text}</Text>
        </View>
      </View>
    );
  };
  _onDone = async () => {
    try {
      //await Linking.openSettings();
      await AsyncStorage.setItem('appIntro', 'done');
      this.setState({showRealApp: true, appIntro: 'done'});
    } catch (e) {
      console.log(e);
    }
  };

  startUpdating = () => {
    if (this.state.needUpdate) {
      this.setState({showUpdateModal: true});
      let updateType;
      if (Platform.OS === 'android' && this.state.otherData) {
        const otherData = this.state.otherData;
        updateType = IAUUpdateKind.IMMEDIATE;
      }
      this.inAppUpdates.addStatusUpdateListener(this.onStatusUpdate);
      this.inAppUpdates.startUpdate({
        updateType, // android only, on iOS the user will be promped to go to your app store page
      });
    } else {
      //alert('doesnt look like we need an update');
    }
  };

  onStatusUpdate = (status) => {
    const {
      // status,
      bytesDownloaded,
      totalBytesToDownload,
    } = status;
    this.setState({bytesDownloaded, totalBytesToDownload}, () => {
      const percent = (bytesDownloaded * 100) / totalBytesToDownload;
      this.setState({percent});
    });
    // do something
    if (bytesDownloaded === totalBytesToDownload) {
      this.setState({showUpdateModal: false});
      this.inAppUpdates.installUpdate();
      alert('New Version Downloaded Successfully!');
    }
    console.log(`@@ ${JSON.stringify(status)}`);
  };

  checkUpdateVersion = () => {
    // this.inAppUpdates
    //   .checkNeedsUpdate({curVersion: VersionCheck.getCurrentVersion()})
    //   .then((result) => {
    //     //alert('update_result==>' + JSON.stringify(result));
    //     this.setState(
    //       {
    //         needUpdate: result.shouldUpdate,
    //         otherData: result,
    //       },
    //       () => {
    //         this.startUpdating();
    //       },
    //     );
    //   });

    VersionCheck.needUpdate().then(async (res) => {
      console.log('update needed ==>', res);
      if (res.isNeeded) {
        this.setState({newVersionRequired: true});
      } else {
        this.setState({newVersionRequired: false});
      }
    });
    // this.setState({showAlert: true});
    // fetch(
    //   Const +
    //     'api/MobileApp/IsMobileUpdateRequired?version=' +
    //     parseInt(VersionNumber.buildVersion),
    //   {
    //     method: 'GET',
    //     headers: {
    //       Accept: 'text/plain',
    //       'Content-Type': 'application/json-patch+json',
    //     },
    //   },
    // )
    //   .then((response) => response.json())
    //   .then((json) => {
    //     //console.log('version_json_check', json);
    //     if (json) {
    //       this.setState({newVersionRequired: true});
    //     } else {
    //       this.setState({newVersionRequired: false});
    //     }
    //   })
    //   .catch((error) => {
    //     this.setState({showAlert: false});
    //     console.error(error);
    //   });
  };
  renderDoneButton = () => {
    return (
      <View style={styles.nextContainer}>
        <Text style={styles.next}>Done</Text>
      </View>
    );
  };
  renderNextButton = () => {
    return (
      <View style={styles.nextContainer}>
        <Text style={styles.next}>Next</Text>
      </View>
    );
  };
  renderPrevButton = () => {
    return (
      <View style={styles.nextContainer1}>
        <Text style={styles.next}>Back</Text>
      </View>
    );
  };

  askForPermissions = async () => {
    if (Platform.OS == 'android') {
      if (Platform.Version <= 28 && Platform.Version >= 22) {
        const granted = await PermissionsAndroid.request(
          PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
          {
            title: 'Attention',
            message: 'Palgeo needs access to your location in background',
          },
        );
        console.log('Android  Location permission granted?' + granted);
        if (granted) {
          const enabled = await WifiManager.isEnabled();
          if (!enabled) WifiManager.setEnabled(true);
          //await this.setupGoogleFit();
          await AsyncStorage.setItem('locationPermissions', 'yes');
          this.setState({locationPermissions: true}, () => {
            console.log('prmissions is true');
            addTask();
          });
        } else {
          alert('Permission to access location denied');
        }
        return;
      }
      if (Platform.Version <= 29 && Platform.Version >= 28) {
        const granted = await PermissionsAndroid.requestMultiple(
          [
            PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
            PermissionsAndroid.PERMISSIONS.ACCESS_BACKGROUND_LOCATION,
          ],

          {
            title: 'Attention',
            message: 'Palgeo needs access to your location in background',
          },
        );
        console.log('Android  Location permission granted?' + granted);
        if (granted) {
          const enabled = await WifiManager.isEnabled();
          if (!enabled) WifiManager.setEnabled(true);
          //await this.setupGoogleFit();
          await AsyncStorage.setItem('locationPermissions', 'yes');
          this.setState({locationPermissions: true}, () => {
            console.log('prmissions is true');
            addTask();
          });
        }
        return;
      }
      if (Platform.Version >= 30) {
        const grantedWhileUsingApp = await PermissionsAndroid.request(
          PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
          {
            title: 'Attention',
            message: 'Palgeo needs access to your location while using the app',
          },
        );

        console.log(
          'Android  Location permission granted?' + grantedWhileUsingApp,
        );
        if (grantedWhileUsingApp) {
          Alert.alert(
            'Background location permission',
            'Allow location permission to get location updates in background',
            [
              {
                text: 'Open Settings',
                onPress: async () => {
                  const granted = await PermissionsAndroid.request(
                    PermissionsAndroid.PERMISSIONS.ACCESS_BACKGROUND_LOCATION,
                  );
                  if (granted) {
                    const enabled = await WifiManager.isEnabled();
                    if (!enabled) WifiManager.setEnabled(true);
                    //await this.setupGoogleFit();
                    await AsyncStorage.setItem('locationPermissions', 'yes');
                    this.setState({locationPermissions: true}, () => {
                      console.log('prmissions is true');
                      addTask();
                    });
                  }
                },
              },
              {
                text: 'Cancel',
                onPress: () => {},
              },
            ],
            {cancelable: false},
          );
        }
      } else {
        alert('Permission to access location denied');
      }
    } else if (Platform.OS == 'ios') {
      Geolocation.requestAuthorization('always');
    }
  };

  //backgroundTimer

  // backgroundCall = () => {

  // };
  goToPlayStore = async () => {
    const url = await VersionCheck.getStoreUrl();
    console.log('url', url);
    const oldUrl = 'market://details?id=' + VersionNumber.bundleIdentifier;
    Linking.openURL(url)
      .catch((e) =>
        alert('Something went wrong while updating the app. Please try again.'),
      )
      .then((success) => this.setState({newVersionRequired: false}));
  };

  render() {
    if (this.state.newVersionRequired) {
      return (
        <AskForUpdate
          goToPlayStore={this.goToPlayStore}
          onClose={() => this.setState({newVersionRequired: false})}
        />
      );
    }

    if (this.state.showUpdateModal) {
      return (
        <CustomModal isVisible={this.state.showUpdateModal} doNotShowDeleteIcon>
          <CustomLabel title={'Update in progress.. Please wait..'} />
          <View style={{justifyContent: 'center', alignItems: 'center'}}>
            <ProgressCircle
              percent={Number(this.state.percent)}
              radius={30}
              borderWidth={5}
              color="#f05760"
              shadowColor="#999"
              bgColor="#fff">
              <Text>{this.state.percent.toString()}</Text>
            </ProgressCircle>
          </View>
        </CustomModal>
      );
    }

    if (!this.state.locationPermissions) {
      return (
        <AskForPermissions
          onPress={this.askForPermissions}
          onPressCancel={() => {
            this.setState({locationPermissions: false});
            BackHandler.exitApp();
          }}
        />
      );
    }

    // if (this.state.show) {
    //   return (
    //     <MovingOutModal
    //       visible={this.state.show}
    //       onClose={() => this.setState({show: false})}
    //     />
    //   );
    // }

    if (this.state.appIntro == 'done') {
      return (
        <NavigationContainer ref={navigationRef}>
          <Root>
            <MainNavigator />
          </Root>
        </NavigationContainer>
      );
    } else if (this.state.appIntro == '') {
      return (
        <View style={{flex: 1, backgroundColor: '#ffffff'}}>
          <StatusBar backgroundColor="#ffffff" hidden />
          <Image
            source={require('./src/assets/ic_bg.png')}
            style={styles.introBg}
          />
          <View style={{flex: 1, marginBottom: hp('15')}}>
            <AppIntroSlider
              renderItem={this._renderItem}
              data={slides}
              onDone={this._onDone}
              activeDotStyle={{backgroundColor: '#f05760'}}
              dotStyle={{backgroundColor: '#f9bcbf'}}
              showDoneButton={true}
              showNextButton={true}
              showPrevButton={true}
              renderDoneButton={this.renderDoneButton}
              renderNextButton={this.renderNextButton}
              renderPrevButton={this.renderPrevButton}
            />
          </View>
        </View>
      );
    } else {
      return (
        <View style={{flex: 1, backgroundColor: '#ffffff'}}>
          <StatusBar backgroundColor="#ffffff" hidden />
          <Image
            source={require('./src/assets/ic_bg.png')}
            style={styles.introBg}
          />
          <View style={{flex: 1, marginBottom: hp('15')}}>
            <AppIntroSlider
              renderItem={this._renderItem}
              data={slides}
              onDone={this._onDone}
              activeDotStyle={{backgroundColor: '#f05760'}}
              dotStyle={{backgroundColor: '#f9bcbf'}}
              showDoneButton={true}
              showNextButton={true}
              showPrevButton={true}
              renderDoneButton={this.renderDoneButton}
              renderNextButton={this.renderNextButton}
              renderPrevButton={this.renderPrevButton}
            />
          </View>
        </View>
      );
    }
  }
}
const styles = StyleSheet.create({
  image: {
    width: 250,
    height: 250,
  },
  slide: {
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: '30%',
  },
  heading: {
    fontFamily: 'Poppins-SemiBold',
    fontSize: 22,
    color: '#000000',
    textAlign: 'center',
    textAlign: 'center',
  },
  description: {
    fontFamily: 'Poppins-Regular',
    fontSize: 13,
    color: '#555a6d',
    textAlign: 'center',
  },
  descriptionContainer: {
    marginLeft: '10%',
    marginRight: '10%',
  },
  headingContainer: {
    marginTop: '7%',
  },
  buttonCircle: {
    width: 40,
    height: 40,
    backgroundColor: 'rgba(0, 0, 0, .2)',
    borderRadius: 20,
    justifyContent: 'center',
    alignItems: 'center',
  },
  next: {
    fontFamily: 'Poppins-Regular',
    fontSize: 17,
  },
  previous: {
    fontFamily: 'Poppins-Regular',
    fontSize: 17,
  },
  next: {
    fontFamily: 'Poppins-SemiBold',
    fontSize: 17,
    color: '#f05760',
  },
  nextContainer: {
    paddingTop: '25%',
  },
  nextContainer1: {
    paddingTop: '26%',
  },
  introBg: {
    width: wp('100'),
    height: hp('100'),
    backgroundColor: '#f05760',
    position: 'absolute',
  },
});
