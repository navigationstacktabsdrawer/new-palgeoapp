import axios from 'axios';
import moment from 'moment';
import {Content} from 'native-base';
import React, {Component} from 'react';
import {FlatList} from 'react-native';
import {TouchableOpacity} from 'react-native';
import {TextInput} from 'react-native';
import {Text, View} from 'react-native';
import {CheckBox} from 'react-native-elements';
import DateTimePickerModal from 'react-native-modal-datetime-picker';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';
import {Colors} from '../../utils/configs/Colors';
import Const from '../common/Constants';
import {CustomCalendar} from '../common/CustomCalendar';
import CustomLabel from '../common/CustomLabel';
import CustomModalTextArea from '../common/CustomModalTextArea';
const SubThemeColor = Colors.secondary;
const ThemeColor = Colors.header;
const date = new Date(1800);

export class StaffDetailsMaster extends Component {
  state = {
    formData: [],
    date: moment(),
    currentIndex: 0,
  };
  componentDidMount() {
    this.retrieveData();
  }

  retrieveData = async () => {
    const url = Const + 'api/Master/GetFormControlsByFormId/1';
    try {
      const response = await axios.get(url);
      const {data} = response;
      if (data?.length > 0) {
        const newData = data.map((e) => {
          return {
            ...e,
            date: moment().format('YYYY-MM-DD'),
            showDateModal: false,
            value: '',
            checked: false,
          };
        });
        this.setState({formData: newData});
      }
    } catch (e) {
      alert('Error retreiving form data: ' + e.message);
    }
  };

  showDatePicker = (index) => {
    const {formData} = this.state;
    formData[index].showDateModal = !formData[index].showDateModal;
    this.setState({formData: [...formData], show: false});
  };

  setDate = (index, date) => {
    const {formData} = this.state;
    formData[index].date = moment(date).format('YYYY-MM-DD');
    formData[index].showDateModal = false;
    this.setState({formData: [...formData]}, () => this.showDatePicker(index));
  };

  setText = (index, text) => {
    const {formData} = this.state;
    formData[index].value = text;

    this.setState({formData: [...formData]});
  };

  setCheckBox = (index) => {
    const {formData} = this.state;
    formData[index].checked = !formData[index].checked;
    this.setState({formData: [...formData]});
  };

  render() {
    const {formData, currentIndex} = this.state;
    return (
      <Content style={{backgroundColor: 'white'}}>
        {this.state.show && (
          <DateTimePickerModal
            mode={'date'}
            date={new Date(formData[currentIndex].date)}
            onConfirm={(date) => this.setDate(currentIndex, date)}
            onCancel={() => this.showDatePicker(currentIndex)}
            style={{backgroundColor: SubThemeColor}}
            isVisible={this.state.show}
          />
        )}
        {formData?.length > 0 &&
          formData.map(
            (
              {
                key,
                label,
                controlType,
                options,
                date,
                showDateModal,
                value,
                checked,
              },
              index,
            ) => {
              return (
                <View key={key}>
                  <CustomLabel title={label} />
                  {controlType === 'datepicker' && (
                    <TouchableOpacity
                      onPress={() => {
                        this.setState({currentIndex: index}, () => {
                          this.setState({show: true});
                        });
                      }}
                      style={{width: wp('35')}}>
                      <CustomLabel
                        containerStyle={{
                          backgroundColor: SubThemeColor,
                          padding: 8,
                        }}
                        labelStyle={{
                          fontFamily: 'Poppins-Regular',
                          fontSize: 14,
                          color: ThemeColor,
                        }}
                        title={date}
                      />
                    </TouchableOpacity>
                  )}
                  {controlType === 'textbox' && (
                    <CustomModalTextArea
                      value={value}
                      onChangeText={(text) => this.setText(index, text)}
                    />
                  )}
                  {controlType === 'checkbox' && (
                    <CheckBox
                      title={label}
                      checked={checked}
                      onPress={() => this.setCheckBox(index)}
                      containerStyle={{backgroundColor: 'transparent'}}
                      checkedColor={Colors.header}
                    />
                  )}
                </View>
              );
            },
          )}
      </Content>
    );
  }
}

export default StaffDetailsMaster;
