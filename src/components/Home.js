import React, {Component} from 'react';
import {
  View,
  StyleSheet,
  Text,
  TouchableWithoutFeedback,
  Alert,
  Platform,
  FlatList,
  TouchableOpacity,
} from 'react-native';
import CustomHeader from './common/CustomHeader';
import SideBar from './common/Sidebar';
import {Container, Drawer, Card, Content, Row} from 'native-base';
import {Badge, ListItem} from 'react-native-elements';
import Loader from './common/Loader';
import AsyncStorage from '@react-native-community/async-storage';
import BackgroundTimer from 'react-native-background-timer';
import Geolocation from 'react-native-geolocation-service';
import Const from './common/Constants';
import DocumentPicker from 'react-native-document-picker';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import AwesomeAlert from 'react-native-awesome-alerts';
import Geocoder from 'react-native-geocoding';
import AskForUpdate from './common/AskForUpdate';
import ReactNativeForegroundService from '@supersami/rn-foreground-service';
import {
  addTask,
  getStatus,
  getAssignedLocations,
  validatePointWithinCircle,
} from '../utils/helperFunctions';
import VersionCheck from 'react-native-version-check';
import RNFetchBlob from 'rn-fetch-blob';
import {GOOGLE_MAPS_APIKEY} from '../utils/configs/Constants';
import CustomModal from './common/CustomModal';
import CustomLabel from './common/CustomLabel';
import CustomModalTextArea from './common/CustomModalTextArea';
import axios from 'axios';

import ImageCard from './common/ImageCard';
import CustomMap from './common/CustomMap';
import {CustomButton} from './common/CustomButton';
import {Colors} from '../utils/configs/Colors';
import {RefreshControl} from 'react-native';
import {Icon} from 'react-native-elements/dist/icons/Icon';

var moment = require('moment');

Geocoder.init(GOOGLE_MAPS_APIKEY);

export default class Home extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loader: false,
      checked_out: '',
      running_status: 'resume',
      newVersionRequired: false,
      position: {},
      bearer_token: '',
      StaffNo: '',
      institute_id: '',
      showAlert: false,
      showAlert1: false,
      error_message: '',
      modalVisible: false,
      modal: true,
      isPause: false,
      message: '',
      address: '',
      attachments: [],
      uploaded: false,
      current_travel_checkin: 'stopped',
      app_state: '',
      files: [],
      accuracy: '',
      latitude: 12.4,
      longitude: 78.3,
      appointments: [],
      appointment: null,
      modalVisible1: false,
      checkInTime: '9:00 AM',
      checkOutTime: '5:00 PM',
      curTime: new Date(),
      travelCheckIn: false,
      travelMpinPressed: false,
      qrCheckInPressed: false,
      geofence: {},
      checkedInRadius: 0,
      refreshing: false,
    };
  }

  successLocation = async (position) => {
    this.setState({
      position,
      accuracy: position.coords.accuracy,
      latitude: position.coords.latitude,
      longitude: position.coords.longitude,
    });
    //await AsyncStorage.setItem('userLocation', JSON.stringify(position.coords));
    console.log('accuracy_watch', position.coords);
  };

  errorLocation = (error) => console.log(error);

  async componentDidMount() {
    //loginAPI();
    this.interval = setInterval(() => {
      this.setState({
        curTime: new Date(),
      });
    }, 1000);

    await this.retrieveData();

    this.willFocusSubscription = this.props.navigation.addListener(
      'focus',
      this.focussedData,
    );
  }

  retrieveData = async () => {
    const assignedLocationsAll = await getAssignedLocations();
    console.log('assignedLocationsAll', assignedLocationsAll);
    const locationPermissionsIos = await AsyncStorage.getItem(
      'locationPermissionsIos',
    );
    if (
      Platform.OS === 'ios' &&
      (locationPermissionsIos === 'false' || !locationPermissionsIos)
    ) {
      Alert.alert(
        'Attention',
        'Enable location services(always) for proper working of the app',
        [
          {
            text: 'Open Settings',
            onPress: () =>
              Linking.openSettings().then(async () => {
                // const locationState = await GPSState.getStatus()
                // console.log(locationState, '===')
                const res = await Geolocation.requestAuthorization('always');
                //console.log('res===', res);
                if (res === 'granted') {
                  await AsyncStorage.setItem('locationPermissionsIos', 'true');
                }
              }),
          },
        ],
        {
          cancelable: false,
        },
      );
      //alert('Enable location services for proper working of the app')
    }

    this.watchID = Geolocation.watchPosition(
      this.successLocation,
      this.errorLocation,
      {
        enableHighAccuracy: true,
        accuracy: 'high',
        distanceFilter: 0,
        interval: 2000,
        fastestInterval: 1000,
      },
    );
    setTimeout(() => Geolocation.clearWatch(this.watchID), 4000);

    const checked_out = await AsyncStorage.getItem('checked_out');
    const bearer_token = await AsyncStorage.getItem('bearer_token');
    const StaffNo = await AsyncStorage.getItem('user_id');
    const institute_id = await AsyncStorage.getItem('institute_id');
    const profile_pic = await AsyncStorage.getItem('profile_pic');
    const running_status = await AsyncStorage.getItem('running_status');
    const geoOld = await AsyncStorage.getItem('geo_id');
    const currentTravelLocation = JSON.parse(
      await AsyncStorage.getItem('currentTravelLocation'),
    );
    const isFaceRequired = await AsyncStorage.getItem('isFaceRequired');
    const qrCheckin = await AsyncStorage.getItem('qrCheckin');
    const qrCheckOut = await AsyncStorage.getItem('qrCheckout');
    const travel_check_in = await AsyncStorage.getItem('travel_check_in');
    const actualCheckInTime = await AsyncStorage.getItem('actualCheckInTime');
    const isTravelCheckinWithMpin = await AsyncStorage.getItem(
      'isTravelCheckinWithMpin',
    );
    const travelCheckIn = await AsyncStorage.getItem('travelCheckIn');
    const qrCheckInPressed = await AsyncStorage.getItem('qrCheckInPressed');
    const travelMpinPressed = await AsyncStorage.getItem('travelMpinPressed');
    fetch(Const + 'api/Staff/IsCheckedInNew', {
      method: 'POST',
      withCredentials: true,
      credentials: 'include',
      headers: {
        Authorization: bearer_token,
        Accept: 'text/plain',
        'Content-Type': 'application/json-patch+json',
      },
      body: JSON.stringify({
        staffCode: StaffNo,
        instituteId: Number(institute_id),
      }),
    })
      .then((response) => response.json())
      .then(async (json) => {
        console.log('jOSN=-==', json);
        if (json && !json.isCheckedIn) {
          AsyncStorage.setItem('checked_out', 'yes');
          AsyncStorage.setItem('geo_id', '');
          AsyncStorage.setItem('radius', '');
          AsyncStorage.setItem('coordinates', JSON.stringify([]));
          AsyncStorage.setItem('current_travel_checkin', 'stopped');
          return;
        }
        if (json && json.isCheckedIn) {
          this.setState(
            {
              modeOfCheckIn: json.modeOfCheckIn,
              actualCheckInTime: moment
                .utc(json?.checkedInTime)
                .local()
                .format('DD.MM.YYYY h:mm a'),
              checked_out: 'no',
            },
            () => console.log('mode(retrieveData) ==', json.modeOfCheckIn),
          );
          await AsyncStorage.setItem('checked_out', 'no');
          await AsyncStorage.setItem(
            'actualCheckInTime',
            moment.utc(json?.checkedInTime).local().format('DD.MM.YYYY h:mm a'),
          );
          if (json?.shiftInfo) {
            await AsyncStorage.setItem(
              'checkInTime',
              moment.utc(json?.shiftInfo?.checkinTime).local().format('h:mm a'),
            );
            await AsyncStorage.setItem(
              'checkOutTime',
              moment
                .utc(json?.shiftInfo?.checkoutTime)
                .local()
                .format('h:mm a'),
            );
            this.setState({
              checkInTime: moment
                .utc(json?.shiftInfo?.checkinTime)
                .local()
                .format('h:mm a'),
              checkOutTime: moment
                .utc(json?.shiftInfo?.checkoutTime)
                .local()
                .format('h:mm a'),
            });
          }
          if (json?.locationDetails) {
            const latitudeChecked = JSON.parse(
              json?.locationDetails?.coordinates,
            );
            console.log('latitudeChecked', latitudeChecked[0]);
            this.setState({
              latitudeChecked: latitudeChecked[0].Latitude,
              longitudeChecked: latitudeChecked[0].Longitude,
            });
          }
          this.setState({
            geofence: json?.locationDetails,

            // actualCheckInTime: moment
            //   .utc(json?.checkedInTime)
            //   .local()
            //   .format('DD.MM.YYYY h:mm a'),
          });
          const run = ReactNativeForegroundService.is_running();
          if (!run)
            ReactNativeForegroundService.start({
              id: 144,
              title: 'Palgeo GeoAttendance App',
              message: '',
            });
          if (json.isTravelCheckIn) {
            await AsyncStorage.setItem('current_travel_checkin', 'running');
            await this.getAppointments();
            if (!currentTravelLocation) {
              await AsyncStorage.setItem(
                'currentTravelLocation',
                JSON.stringify(this.state.position.coords),
              );
            }
            return this.setState({current_travel_checkin: 'running'});
          }
          if (
            json?.locationDetails?.type === 'Circle' &&
            !json?.isTravelCheckIn
          ) {
            if (geoOld) {
              return;
            }
            await AsyncStorage.setItem(
              'radius',
              json.locationDetails.radius.toString(),
            );
            await AsyncStorage.setItem(
              'geo_id',
              json.locationDetails.id.toString(),
            );
            await AsyncStorage.setItem(
              'coordinates',
              JSON.stringify(json.locationDetails.coordinates),
            );
          }
          return;
        }
      })
      .catch((error) => console.log('error_IS_CHECKED_IN =>', error));
    const current_travel_checkin = await AsyncStorage.getItem(
      'current_travel_checkin',
    );
    const locations = JSON.parse(await AsyncStorage.getItem('locations'));
    this.setState({
      assignedLocationsAll,
      locations,
      checked_out,
      bearer_token,
      StaffNo,
      institute_id,
      running_status: running_status ? running_status : 'resume',
      current_travel_checkin: current_travel_checkin
        ? current_travel_checkin
        : 'stopped',
      travel_check_in: travel_check_in === 'yes' ? true : false,
      qrCheckin: qrCheckin === 'true' ? true : false,
      qrCheckOut: qrCheckOut === 'true' ? true : false,
      isTravelCheckinWithMpin: isTravelCheckinWithMpin === 'yes' ? true : false,
      isFaceRequired,
      profile_pic,
      actualCheckInTime,
      travelCheckIn: travelCheckIn === 'yes' ? true : false,
      qrCheckInPressed: qrCheckInPressed === 'yes' ? true : false,
      travelMpinPressed: travelMpinPressed === 'yes' ? true : false,
    });

    //let compareResult = false;

    console.log(
      'checked_out_mounted and_checkin current_travel',
      this.state.checked_out,
      this.state.current_travel_checkin,
    );

    AsyncStorage.getItem('locationPermissions')
      .catch((e) => console.log('async', e))
      .then((locationPermissions) => {
        console.log('locationPermissions ==>', locationPermissions);
        this.setState({locationPermissions: locationPermissions}, () => {
          if (
            this.state.locationPermissions === 'yes' ||
            this.state.locationPermissions
          ) {
            let compareArray = [];
            // if (locations && locations.length > 0) {
            //   let currentTime = moment.utc().format('HH:mm');
            //   locations.forEach((location) => {
            //     let checkoutTime = moment(location.checkOutTime, 'HH:mm').format(
            //       'HH:mm',
            //     );
            //     //console.log(currentTime, checkoutTime);
            //     compareResult = compareTwoTime(currentTime, checkoutTime);
            //     compareArray.push(compareResult);
            //     // console.log('compare', compareResult);
            //   });
            // }

            //console.log('permissions granted');
            const running = ReactNativeForegroundService.get_all_tasks();
            console.log('foreground_service===>', running);

            const run = ReactNativeForegroundService.is_running();

            console.log('service running ==>', run);

            if (
              checked_out === 'yes' &&
              (isFaceRequired === 'true' || isTravelCheckinWithMpin === 'yes')
            ) {
              if (current_travel_checkin === 'stopped') {
                ReactNativeForegroundService.remove_all_tasks();
                ReactNativeForegroundService.stop();
              }
            }
            if (!running['taskid']) {
              //ReactNativeForegroundService.remove_task('taskid');
              addTask();
            }
            if (isFaceRequired === 'false' && isTravelCheckinWithMpin == 'no') {
              ReactNativeForegroundService.start({
                id: 144,
                title: 'Palgeo GeoAttendance App',
                message: '',
              });
            }

            if (!run) {
              if (checked_out === 'no') {
                ReactNativeForegroundService.start({
                  id: 144,
                  title: 'Palgeo GeoAttendance App',
                  message: '',
                });
              }
            }
          }
        });
      });
  };

  focussedData = async () => {
    this.setState({loader: true});
    setTimeout(() => this.setState({loader: false}), 800);
    //this.checkUpdateVersion();
    const assignedLocationsAll = await getAssignedLocations();
    const checked_out = await AsyncStorage.getItem('checked_out');
    const bearer_token = await AsyncStorage.getItem('bearer_token');
    const StaffNo = await AsyncStorage.getItem('user_id');
    const institute_id = await AsyncStorage.getItem('institute_id');
    const running_status = await AsyncStorage.getItem('running_status');
    const isFaceRequired = await AsyncStorage.getItem('isFaceRequired');
    const travelCheckIn = await AsyncStorage.getItem('travelCheckIn');
    const qrCheckInPressed = await AsyncStorage.getItem('qrCheckInPressed');
    const travelMpinPressed = await AsyncStorage.getItem('travelMpinPressed');
    const actualCheckInTime = await AsyncStorage.getItem('actualCheckInTime');
    const isTravelCheckinWithMpin = await AsyncStorage.getItem(
      'isTravelCheckinWithMpin',
    );

    console.log('actualCheckInTime', actualCheckInTime);
    const current_travel_checkin = await AsyncStorage.getItem(
      'current_travel_checkin',
    );
    //const locations = JSON.parse(await AsyncStorage.getItem('locations'));
    console.log('current_travel_checkin', current_travel_checkin);
    this.setState({
      assignedLocationsAll,
      checked_out,
      bearer_token,
      StaffNo,
      institute_id,
      running_status: running_status ? running_status : 'resume',
      current_travel_checkin,
      travelCheckIn: travelCheckIn === 'yes' ? true : false,
      qrCheckInPressed: qrCheckInPressed === 'yes' ? true : false,
      travelMpinPressed: travelMpinPressed === 'yes' ? true : false,
    });
    if (current_travel_checkin === 'running') {
      await this.getAppointments();
    }
    if (current_travel_checkin === 'stopped') {
      this.setState({appointments: []});
    }

    fetch(Const + 'api/Staff/IsCheckedInNew', {
      method: 'POST',
      withCredentials: true,
      credentials: 'include',
      headers: {
        Authorization: bearer_token,
        Accept: 'text/plain',
        'Content-Type': 'application/json-patch+json',
      },
      body: JSON.stringify({
        staffCode: StaffNo,
        instituteId: Number(institute_id),
      }),
    })
      .then((response) => response.json())
      .then(async (json) => {
        if (json && json.isCheckedIn) {
          const running = ReactNativeForegroundService.get_all_tasks();
          const run = ReactNativeForegroundService.is_running();
          if (running['taskid'] && !run) {
            console.log('I am true');
            ReactNativeForegroundService.start({
              id: 144,
              title: 'Palgeo GeoAttendance App',
              message: '',
            });
          }
          this.setState(
            {
              modeOfCheckIn: json.modeOfCheckIn,
              actualCheckInTime: moment
                .utc(json?.checkedInTime)
                .local()
                .format('DD.MM.YYYY h:mm a'),
            },
            () => console.log('mode ==', json.modeOfCheckIn),
          );
          await AsyncStorage.setItem('checked_out', 'no');
          if (json?.shiftInfo) {
            await AsyncStorage.setItem(
              'checkInTime',
              moment.utc(json?.shiftInfo?.checkinTime).local().format('h:mm a'),
            );
            await AsyncStorage.setItem(
              'checkOutTime',
              moment
                .utc(json?.shiftInfo?.checkoutTime)
                .local()
                .format('h:mm a'),
            );
            this.setState({
              checkInTime: moment
                .utc(json?.shiftInfo?.checkinTime)
                .local()
                .format('h:mm a'),
              checkOutTime: moment
                .utc(json?.shiftInfo?.checkoutTime)
                .local()
                .format('h:mm a'),
            });
          }
          if (json?.locationDetails) {
            const latitudeChecked = JSON.parse(
              json?.locationDetails?.coordinates,
            );
            console.log('latitudeChecked', latitudeChecked[0]);
            this.setState({
              latitudeChecked: latitudeChecked[0].Latitude,
              longitudeChecked: latitudeChecked[0].Longitude,
            });
          }
          // this.setState({
          //   // checkInTime: moment
          //   //   .utc(json?.shiftInfo?.checkinTime)
          //   //   .local()
          //   //   .format('h:mm a'),
          //   // checkOutTime: moment
          //   //   .utc(json?.shiftInfo?.checkoutTime)
          //   //   .local()
          //   //   .format('h:mm a'),
          //   // latitudeChecked: latitudeChecked[0].Latitude,
          //   // longitudeChecked: latitudeChecked[0].Longitude,
          //   actualCheckInTime: moment
          //     .utc(json?.checkedInTime)
          //     .local()
          //     .format('DD.MM.YYYY h:mm a'),
          // });
        } else {
          this.setState({checked_out: 'yes'});
          await AsyncStorage.setItem('checked_out', 'yes');
        }
      })
      .catch((error) => console.log('error_IS_CHECKED_IN =>', error));
    console.log('I ran after fetch in focussed data');
    // this.setState({
    //   checked_out,
    //   bearer_token,
    //   StaffNo,
    //   institute_id,
    //   running_status: running_status ? running_status : 'resume',
    //   current_travel_checkin,
    //   travelCheckIn: travelCheckIn === 'yes' ? true : false,
    //   qrCheckInPressed: qrCheckInPressed === 'yes' ? true : false,
    //   travelMpinPressed: travelMpinPressed === 'yes' ? true : false,
    // });
    const running = ReactNativeForegroundService.get_all_tasks();
    const run = ReactNativeForegroundService.is_running();
    let compareResult = false;
    let compareArray = [];
    // if (locations && locations.length > 0) {
    //   let currentTime = moment.utc().format('HH:mm');
    //   locations.forEach((location) => {
    //     let checkoutTime = moment(location.checkOutTime, 'HH:mm').format(
    //       'HH:mm',
    //     );
    //     //console.log(currentTime, checkoutTime);
    //     compareResult = compareTwoTime(currentTime, checkoutTime);
    //     compareArray.push(compareResult);
    //     //console.log('compare', compareResult);
    //   });
    // }
    if (
      (isFaceRequired === 'true' || isTravelCheckinWithMpin === 'yes') &&
      checked_out === 'yes'
    ) {
      if (current_travel_checkin === 'stopped') {
        ReactNativeForegroundService.remove_all_tasks();
        ReactNativeForegroundService.stop();
      }
    }

    //console.log('task is running? ' + JSON.stringify(running, null, 4));
    if (!run) {
      // ReactNativeForegroundService.remove_task('taskid');
      // addTask();
      if (isFaceRequired === 'false' && isTravelCheckinWithMpin === 'no') {
        ReactNativeForegroundService.start({
          id: 144,
          title: 'Palgeo GeoAttendance App',
          message: '',
        });
      }
    }
    if (running['taskid'] && !run && checked_out === 'no') {
      console.log('I am true');
      ReactNativeForegroundService.start({
        id: 144,
        title: 'Palgeo GeoAttendance App',
        message: '',
      });
    }
  };

  getAppointments = async () => {
    const instituteId = this.state.institute_id;
    const StaffCode = this.state.StaffNo;
    const calendarView = 'day';
    const url = `${Const}api/GeoFencing/travelcheckin/Details/${instituteId}/${StaffCode}/${calendarView}`;
    // const url =
    //   'https://insproplus.com/palgeoapi/api/GeoFencing/travelcheckin/Details/27185/120220007/day';
    try {
      const response = await axios.get(url);
      console.log('appointments', response.data);
      if (response?.data?.length > 0) {
        const travelLocations = response.data.map((loc) => {
          return {
            latitude: loc.latitude,
            longitude: loc.longitude,
            address: loc.address,
            id: loc.appointmentId,
            status: loc.status,
          };
        });
        await AsyncStorage.setItem(
          'travelLocations',
          JSON.stringify(travelLocations),
        );
      }
      let data = [];
      data = response.data.sort(
        (e1, e2) =>
          new Date(e1.startDateTime).getTime() >
          new Date(e2.startDateTime).getTime(),
      );
      console.log('data ==>', data);
      this.setState({appointments: data});
    } catch (e) {
      alert(e.message);
    }
  };

  checkUpdateVersion = () => {
    //this.setState({showAlert: true});
    VersionCheck.needUpdate()
      .then(async (res) => {
        console.log('update needed ==>', res);
        if (res.isNeeded) {
          this.setState({newVersionRequired: true});
        } else {
          this.setState({newVersionRequired: false});
        }
      })
      .catch((e) => alert(e.toString()));
    // fetch(
    //   Const +
    //     'api/MobileApp/IsMobileUpdateRequired?version=' +
    //     parseInt(VersionNumber.buildVersion),
    //   {
    //     method: 'GET',
    //     headers: {
    //       Accept: 'text/plain',
    //       'Content-Type': 'application/json-patch+json',
    //     },
    //   },
    // )
    //   .then((response) => response.json())
    //   .then((json) => {
    //     console.log('version_json_check', json);
    //     if (json) {
    //       this.setState({newVersionRequired: true});
    //     } else {
    //       this.setState({newVersionRequired: false});
    //     }
    //   })
    //   .catch((error) => {
    //     //this.setState({showAlert: false});
    //     console.error(error);
    //   });
  };

  async componentWillUnmount() {
    this.props.navigation.removeListener(this.willFocusSubscription);
    Geolocation.clearWatch(this.watchID);
    BackgroundTimer.stopBackgroundTimer();
    clearInterval(this.interval);
    //this.subscip.remove();
  }

  openDrawer = () => {
    this.drawer._root.open();
  };
  openModal = async (status) => {
    const {assignedLocationsAll} = this.state;
    console.log(status);

    Geolocation.getCurrentPosition(
      (position) => {
        var coordinates = {
          latitude: parseFloat(position.coords.latitude),
          longitude: parseFloat(position.coords.longitude),
          accuracy: parseFloat(position.coords.accuracy),
        };
        Geocoder.from(coordinates.latitude, coordinates.longitude)
          .then((json) => {
            const {results} = json;
            const filteredResults = results.filter(
              (e) => !e.formatted_address.includes('+'),
            );
            let count = 0;
            const filteredLocations = assignedLocationsAll.filter(
              (e) => e.type !== 'Anonymous',
            );
            if (filteredLocations.length > 0) {
              filteredLocations.forEach((e) => {
                const check = validatePointWithinCircle(e, coordinates);
                console.log('check', check);
                if (check) {
                  return this.setState({
                    address: e.accessLocation,
                    // modalVisible: true,
                    isPause: status,

                    uploaded: false,
                    message: '',
                  });
                }
                count += 1;
              });
            }
            if (count === filteredLocations.length) {
              this.setState({
                modalVisible: true,
                isPause: status,
                address: filteredResults[0].formatted_address,
                uploaded: false,
                message: '',
              });
            } else {
              this.setState({modalVisible: true});
            }
            //console.log('address', JSON.stringify(json, null, 2));
          })
          .catch((error) => console.log(error));
      },
      (error) => {},
      {enableHighAccuracy: true, timeout: 2000, maximumAge: 1000},
    );
  };
  pauseOrResume = async (status) => {
    Geolocation.getCurrentPosition(
      (position) => {
        const currentLongitude = parseFloat(position.coords.longitude);
        const currentLatitude = parseFloat(position.coords.latitude);
        var coordinates = {
          latitude: currentLatitude,
          longitude: currentLongitude,
        };
        this.setState({showAlert: true, modalVisible: false});
        fetch(Const + 'api/GeoFencing/travelcheckin/pause/update', {
          method: 'POST',
          withCredentials: true,
          credentials: 'include',
          headers: {
            Authorization: 'Bearer ' + this.state.bearer_token,
            Accept: 'application/json, text/plain',
            'Content-Type': 'application/json-patch+json',
          },
          body: JSON.stringify({
            instituteId: Number(this.state.institute_id),
            StaffCode: this.state.StaffNo,
            coordinates,
            isCheckOut: false,
            isCheckIn: false,
            address: this.state.address,
            message: this.state.message,
            isPause: status,
            attachments: !status ? this.state.attachments : [],
          }),
        })
          .then((response) => response.json())
          .then((json) => {
            console.log('json_travel', json);
            if (status) {
              AsyncStorage.setItem('running_status', 'pause');
              this.setState({running_status: 'pause'});
            } else if (!status) {
              AsyncStorage.setItem('running_status', 'resume');
              this.setState({running_status: 'resume'});
            }
            if (json.status) {
              this.setState({
                showAlert1: true,
                error_message: json.message,
                showAlert: false,
              });
            } else {
              this.setState({
                showAlert1: true,
                error_message: json.message,
                showAlert: false,
              });
            }
          })
          .catch((error) => {
            this.setState({
              showAlert1: true,
              error_message: 'Unknown error occured',
              showAlert: false,
            });
          });
      },
      (error) => {
        console.log(error);
      },
      {enableHighAccuracy: true, timeout: 20000, maximumAge: 0},
    );
  };
  uploadAttachments = async () => {
    try {
      const results = await DocumentPicker.pickMultiple({
        includeBase64: true,
        type: [DocumentPicker.types.allFiles],
      });
      var tempArr = [];
      var files = [];
      console.log('files', results);
      results.forEach((res) => {
        RNFetchBlob.fs
          .readFile(res.uri, 'base64')
          .then((data) => {
            var Attachment = {
              fileName: res.name,
              fileType: res.type,
              attachment: data,
            };
            // console.log("Arr", Attachment)
            this.setState({
              attachments: [...this.state.attachments, Attachment],
              uploaded: true,
              files: [...this.state.files, res.name],
            });
            // tempArr.push(Attachment)
            // files.push(res.name)
          })
          .catch((err) => {});
        //console.log('files', files, tempArr)
      });
      //files = tempArr.map((tt) => tt.fileName)
    } catch (e) {
      console.log(e);
    }
    // let files = []
    // ImagePicker.openPicker({
    //   multiple: true,
    //   includeBase64: true,
    //   compressImageQuality: 0.5,
    // }).then((images) => {
    //   var tempArr = [];
    //   images.map((item, index) => {
    //     var attachmentObj = {
    //       fileName: item.path.replace(/^.*[\\\/]/, ''),
    //       fileType: item.mime,
    //       attachment: item.data,
    //     };
    //     tempArr.push(attachmentObj);
    //   });
    //  files = tempArr.map((tt) => tt.fileName)
    //   this.setState({attachments: tempArr, uploaded: true, files});
    // });
  };

  renderItem = ({item, index, section}) => {
    return (
      <ListItem bottomDivider>
        <ListItem.Content>
          <Badge
            badgeStyle={{
              minWidth: 25,
              minHeight: 25,
              borderRadius: 15,
              padding: 3,
            }}
            value={item.status || 'Not Completed'}
            status={
              item.status === 'Completed'
                ? 'success'
                : item.status === 'Postponed'
                ? 'warning'
                : 'error'
            }
          />
          <ListItem.Title>{item.title}</ListItem.Title>
          <ListItem.Subtitle>{item.companyName}</ListItem.Subtitle>
          <Text
            style={{
              fontSize: 14,
              fontWeight: '100',
              fontFamily: 'Poppins-Regular',
              color: 'rgba(0,0,0,0.6)',
            }}>{`${moment(item.startDateTime).format('h:mm a')} to ${moment(
            item.endDateTime,
          ).format('h:mm a')}`}</Text>
        </ListItem.Content>
        <ListItem.Chevron
          color={'black'}
          size={25}
          onPress={() =>
            this.props.navigation.navigate('AppointmentDetails', {item})
          }
        />
      </ListItem>
    );
  };

  handleCheckInOrOut = async () => {
    const {
      travelCheckIn,
      qrCheckInPressed,
      checked_out,
      qrCheckOut,
      qrCheckin,
      isTravelCheckinWithMpin,
      travel_check_in,
      travelMpinPressed,
      bearer_token,
      StaffNo,
      institute_id,
    } = this.state;
    console.log('All states ==>', {
      travelCheckIn,
      qrCheckInPressed,
      checked_out,
      qrCheckOut,
      qrCheckin,
      isTravelCheckinWithMpin,
      travel_check_in,
      travelMpinPressed,
    });

    if (checked_out === 'no') {
      const isCheckedIn = await getStatus(bearer_token, StaffNo, institute_id);
      if (!isCheckedIn) {
        await AsyncStorage.setItem('checked_out', 'yes');
        this.setState({checked_out: 'yes'}, () =>
          alert('Please check in first'),
        );
        return this.props.navigation.replace('Home');
      }
      if (
        (!travelCheckIn && !qrCheckInPressed && !travelMpinPressed) ||
        this.state.modeOfCheckIn === 'Auto CheckIn'
      ) {
        this.props.navigation.navigate('Checkout', {
          show: true,
        });
      } else {
        this.props.navigation.navigate('Checkout', {
          show: travelMpinPressed ? false : travelCheckIn,
          travelCheckOut: travelCheckIn,
          qrCheckin: qrCheckOut ? 'true' : 'false',
          show2: qrCheckInPressed,
          travel_check_in: travel_check_in ? 'yes' : 'no',
          isTravelCheckinWithMpin,
          showMpinAlert: travelMpinPressed,
        });
      }
      return;
    }
    const isCheckedIn = await getStatus(bearer_token, StaffNo, institute_id);
    console.log('isCheckedIn', isCheckedIn);
    if (isCheckedIn) return this.props.navigation.replace('Home');
    if (!travelCheckIn && !qrCheckInPressed && !travelMpinPressed) {
      //this.props.navigation.navigate('Checkin', {travel: false});
      this.props.navigation.navigate('Checkin', {
        show: true,
        qrCheckin: qrCheckin ? 'true' : 'false',

        travel: travel_check_in ? 'yes' : 'no',
        isTravelCheckinWithMpin,
      });
    } else {
      this.props.navigation.navigate('Checkin', {
        show: travelCheckIn,
        qrCheckin: qrCheckin ? 'true' : 'false',
        show2: qrCheckInPressed,
        travelCheckIn,
        travel: travel_check_in ? 'yes' : 'no',
        isTravelCheckinWithMpin,
        showMpinAlert: travelMpinPressed,
      });
    }
  };

  getTimeDiff = () => {
    return moment(this.state.actualCheckInTime, 'DD.MM.YYYY h:mm a').fromNow();
  };

  getLocationName = (locationName) => {
    let check = false;
    const {assignedLocationsAll} = this.state;
    assignedLocationsAll.forEach((e) => {
      if (e.accessLocation === locationName) {
        check = true;
      }
    });
    return check;
  };

  render() {
    const {appointment} = this.state;
    //console.log(this.state.geofence.coordinates);
    const innerComponent = (
      <>
        <View style={{marginTop: '3%'}}>
          <CustomLabel title={'Current Address'} />
          <CustomModalTextArea
            value={this.state.address}
            onChangeText={(address) => this.setState({address})}
            disabled
          />
        </View>
        <View style={{marginTop: '4%'}}>
          <CustomLabel title={'Message'} />
          <CustomModalTextArea
            value={this.state.message}
            onChangeText={(message) => {
              this.setState({message});
            }}
          />
        </View>
        {!this.state.isPause && (
          <View style={{marginTop: '4%'}}>
            <View style={styles.input}>
              <TouchableWithoutFeedback onPress={this.uploadAttachments}>
                <View>
                  {this.state.uploaded && (
                    // <Text style={styles.label}>
                    //   Uploaded Successfully
                    // </Text>
                    <Text style={styles.label}>
                      {this.state.files.join(',')}
                    </Text>
                  )}

                  {!this.state.uploaded && (
                    <Text style={styles.label}>Upload Attachments</Text>
                  )}
                </View>
              </TouchableWithoutFeedback>
            </View>
          </View>
        )}
        <TouchableWithoutFeedback
          onPress={() => this.pauseOrResume(this.state.isPause)}>
          <View style={styles.buttonContainer3}>
            <Text style={styles.footerText}>Submit</Text>
          </View>
        </TouchableWithoutFeedback>
      </>
    );

    if (this.state.loader) {
      return <Loader />;
    }
    if (this.state.newVersionRequired) {
      return <AskForUpdate />;
    }
    return (
      <View style={styles.container}>
        <Drawer
          ref={(ref) => {
            this.drawer = ref;
          }}
          content={<SideBar navigation={this.props.navigation} />}>
          <Container>
            <CustomHeader
              navigation={this.props.navigation}
              openDrawer={this.openDrawer}
            />

            <Content
              refreshing
              refreshControl={
                <RefreshControl
                  style={{backgroundColor: '#E0FFFF'}}
                  refreshing={this.state.refreshing}
                  onRefresh={() => this.retrieveData()}
                  tintColor="#ff0000"
                  title="Loading..."
                  titleColor="#00ff00"
                  //colors={['#ff0000', '#00ff00', '#0000ff']}
                  //progressBackgroundColor="#ffff00"
                />
              }>
              <CustomModal
                title={'Please fill the details before you submit'}
                isVisible={this.state.modalVisible}
                deleteIconPress={() => {
                  this.setState({modalVisible: false, address: ''});
                }}>
                {innerComponent}
              </CustomModal>

              <AwesomeAlert
                show={this.state.showAlert}
                showProgress={true}
                title="Loading"
                closeOnTouchOutside={false}
                closeOnHardwareBackPress={false}
              />
              <AwesomeAlert
                show={this.state.showAlert1}
                showProgress={false}
                title="Attention"
                message={this.state.error_message}
                closeOnTouchOutside={true}
                closeOnHardwareBackPress={false}
                showCancelButton={true}
                cancelText="Okay"
                onCancelPressed={() => {
                  this.setState({showAlert1: false});
                }}
                cancelButtonColor="#f05760"
                cancelButtonTextStyle={{
                  fontFamily: 'Poppins-Regular',
                  fontSize: 13,
                }}
                messageStyle={{fontFamily: 'Poppins-Regular', fontSize: 13}}
                titleStyle={{fontFamily: 'Poppins-SemiBold', fontSize: 14}}
              />
              <View style={{alignItems: 'center', margin: '4%'}}>
                <Text style={styles.text}>Palgeo Administrator Dashboard</Text>

                {this.state.checked_out === 'no' && (
                  <Text>
                    In: {this.state.checkInTime} Out: {this.state.checkOutTime}{' '}
                  </Text>
                )}
                {this.state.checked_out === 'no' && (
                  <View
                    style={{
                      width: '90%',
                      alignSelf: 'center',
                      alignItems: 'center',
                    }}>
                    <Row>
                      <ImageCard
                        width={wp('40')}
                        height={100}
                        source={{uri: this.state.profile_pic}}
                      />
                      <TouchableOpacity
                        disabled={this.state.modeOfCheckIn === 'Travel CheckIn'}
                        onPress={() => {
                          const coordinates = [
                            {
                              latitude: this.state.latitudeChecked,
                              longitude: this.state.longitudeChecked,
                            },
                          ];
                          const radius = parseInt(this.state.geofence.radius);
                          this.props.navigation.navigate('CircleMapView', {
                            coordinates: coordinates,
                            radius: radius,
                            route: 'Home',
                          });
                        }}>
                        <CustomMap
                          latitude={this.state?.latitudeChecked || 22.222}
                          longitude={this.state?.longitudeChecked || 78.2222}
                          appointment
                          width={wp('40')}
                          height={100}
                        />
                      </TouchableOpacity>
                    </Row>
                    <Text style={styles.text}>
                      You checked on{' '}
                      {moment(
                        this.state.actualCheckInTime,
                        'DD.MM.YYYY h:mm a',
                      ).format('DD.MM.YYYY')}{' '}
                      at{' '}
                      {moment(
                        this.state.actualCheckInTime,
                        'DD.MM.YYYY h:mm a',
                      ).format('h:mm a')}
                    </Text>
                    <Text
                      style={{
                        ...styles.text,
                        fontSize: 20,
                        fontWeight: 'bold',
                      }}>
                      You checked in {this.getTimeDiff()}
                    </Text>
                    <CustomButton
                      onPress={() => this.handleCheckInOrOut()}
                      title={'Check Out'}
                      color={Colors.header}
                      buttonStyle={{
                        backgroundColor: Colors.header,
                        padding: 8,
                        margin: 10,
                      }}
                    />
                  </View>
                )}
                {this.state.assignedLocationsAll?.length > 0 &&
                  this.state.checked_out !== 'no' && (
                    <Text
                      style={{
                        ...styles.text,
                        fontSize: 20,
                        fontWeight: 'bold',
                      }}>
                      Check In
                    </Text>
                  )}

                {this.state.assignedLocationsAll?.length > 0 &&
                  this.state.checked_out !== 'no' && (
                    <View
                      style={{
                        width: '100%',
                        alignSelf: 'center',
                        alignItems: 'center',
                      }}>
                      {this.state.locations?.length > 0 && (
                        <ImageCard
                          title={'Check In'}
                          onPress={() => {
                            this.setState(
                              {
                                travelMpinPressed: false,
                                qrCheckInPressed: false,
                                travelCheckIn: false,
                              },
                              async () => {
                                await AsyncStorage.setItem(
                                  'travelMpinPressed',
                                  'no',
                                );
                                await AsyncStorage.setItem(
                                  'qrCheckInPressed',
                                  'no',
                                );
                                await AsyncStorage.setItem(
                                  'travelCheckIn',
                                  'no',
                                );
                                this.handleCheckInOrOut();
                              },
                            );
                          }}
                          source={require('../assets/map.png')}
                        />
                      )}
                      {this.getLocationName('Travel Check In') && (
                        <ImageCard
                          title={'Travel Check In'}
                          onPress={() => {
                            this.setState(
                              {
                                travelMpinPressed: false,
                                qrCheckInPressed: false,
                                travelCheckIn: true,
                              },
                              async () => {
                                await AsyncStorage.setItem(
                                  'travelMpinPressed',
                                  'no',
                                );
                                await AsyncStorage.setItem(
                                  'qrCheckInPressed',
                                  'no',
                                );
                                await AsyncStorage.setItem(
                                  'travelCheckIn',
                                  'yes',
                                );
                                this.handleCheckInOrOut();
                              },
                            );
                          }}
                          source={require('../assets/location.png')}
                        />
                      )}
                      {this.getLocationName('Qr Check In') && (
                        <ImageCard
                          title={'QR Check In'}
                          onPress={() =>
                            this.setState(
                              {
                                travelMpinPressed: false,
                                travelCheckIn: false,
                                qrCheckInPressed: true,
                              },
                              async () => {
                                await AsyncStorage.setItem(
                                  'travelMpinPressed',
                                  'no',
                                );
                                await AsyncStorage.setItem(
                                  'qrCheckInPressed',
                                  'yes',
                                );
                                await AsyncStorage.setItem(
                                  'travelCheckIn',
                                  'no',
                                );
                                this.handleCheckInOrOut();
                              },
                            )
                          }
                          source={require('../assets/qr-code.png')}
                        />
                      )}
                      {this.getLocationName(
                        'Travel Check IN with Staff Code',
                      ) && (
                        <ImageCard
                          title={'Travel Mpin Check In'}
                          onPress={() =>
                            this.setState(
                              {
                                travelMpinPressed: true,
                                qrCheckInPressed: false,
                                travelCheckIn: false,
                              },
                              async () => {
                                await AsyncStorage.setItem(
                                  'travelMpinPressed',
                                  'yes',
                                );
                                await AsyncStorage.setItem(
                                  'qrCheckInPressed',
                                  'no',
                                );
                                await AsyncStorage.setItem(
                                  'travelCheckIn',
                                  'no',
                                );
                                this.handleCheckInOrOut();
                              },
                            )
                          }
                          source={require('../assets/password.png')}
                        />
                      )}
                    </View>
                  )}

                {this.state.current_travel_checkin == 'running' &&
                  this.state.checked_out == 'no' && (
                    <>
                      <Card style={styles.walletContainer}>
                        <View style={styles.row}>
                          <View style={{width: wp('65')}}>
                            <View style={styles.headingContainer}>
                              <Text style={styles.heading}>
                                Active Travel Checkin
                              </Text>
                            </View>
                          </View>
                          <View>
                            {this.state.running_status == 'resume' &&
                              this.state.current_travel_checkin ==
                                'running' && (
                                <TouchableWithoutFeedback
                                  onPress={() => this.openModal(true)}>
                                  <View style={styles.buttonContainer}>
                                    <Text style={styles.buttonText}>Pause</Text>
                                  </View>
                                </TouchableWithoutFeedback>
                              )}
                            {this.state.running_status == 'pause' &&
                              this.state.current_travel_checkin ==
                                'running' && (
                                <TouchableWithoutFeedback
                                  onPress={() => this.openModal(false)}>
                                  <View style={styles.buttonContainer1}>
                                    <Text style={styles.buttonText}>
                                      Resume
                                    </Text>
                                  </View>
                                </TouchableWithoutFeedback>
                              )}
                          </View>
                        </View>
                      </Card>
                    </>
                  )}
              </View>
              {this.state.appointments.length > 0 &&
                this.state.current_travel_checkin === 'running' && (
                  <>
                    <CustomLabel
                      labelStyle={{fontSize: 17, fontWeight: 'bold'}}
                      title={'Your Appointments'}
                    />
                    <FlatList
                      data={this.state.appointments}
                      keyExtractor={(item, index) => index + ''}
                      renderItem={this.renderItem}
                    />
                  </>
                )}
            </Content>
          </Container>
        </Drawer>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fbfbfb',
  },
  text: {
    fontFamily: 'Poppins-Regular',
    fontSize: 15,
  },
  heading: {
    fontFamily: 'Poppins-Regular',
    fontSize: 13,
  },
  walletContainer: {
    backgroundColor: '#ffffff',
    borderRadius: 10,
    elevation: 5,
    width: wp('93'),
    paddingTop: '3%',
    paddingLeft: '4%',
    paddingRight: '4%',
    paddingBottom: '3%',
    marginTop: '5%',
  },
  row: {
    flexDirection: 'row',
  },
  buttonContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    padding: 10,
    backgroundColor: 'red',
    borderRadius: 10,
    width: wp('20'),
  },
  buttonContainer1: {
    justifyContent: 'center',
    alignItems: 'center',
    padding: 10,
    backgroundColor: 'green',
    borderRadius: 10,
    width: wp('20'),
  },
  buttonText: {
    fontFamily: 'Poppins-Regular',
    fontSize: 13,
    color: '#fff',
  },
  center: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  headingContainer: {
    marginTop: 8,
  },
  buttonContainer3: {
    width: wp('80'),
    backgroundColor: '#f05760',
    justifyContent: 'center',
    alignItems: 'center',
    padding: 12,
    borderRadius: 30,
    marginTop: hp('3'),
  },
  footerText: {
    fontFamily: 'Poppins-Regular',
    fontSize: 14,
    color: '#ffffff',
  },
  input: {
    fontFamily: 'Poppins-Regular',
    fontSize: 13,
    backgroundColor: '#ffffff',
    paddingLeft: '5%',
    borderRadius: 10,
    borderColor: '#f1f1f1',
    borderWidth: 1.5,
    height: hp('7'),
    justifyContent: 'center',
  },
  item: {
    borderRadius: 10,
    backgroundColor: '#ffffff',
    height: hp('7'),
    borderColor: '#f1f1f1',
    borderWidth: 1.5,
  },
  item1: {
    borderRadius: 8,
    backgroundColor: '#ffffff',
    borderColor: '#f1f1f1',
    borderWidth: 1.5,
  },
  textarea: {
    borderLeftWidth: 0,
    borderBottomWidth: 0,
    borderTopWidth: 0,
    borderRightWidth: 0,
    borderRadius: 8,
  },
  labelContainer: {
    margin: '1.5%',
  },
  label: {
    fontFamily: 'Poppins-Regular',
    fontSize: 13,
    color: '#000000',
  },
});
