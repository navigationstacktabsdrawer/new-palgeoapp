import React, {Component} from 'react';
import {
  View,
  Text,
  StyleSheet,
  Dimensions,
  ScrollView,
  TouchableOpacity,
} from 'react-native';
import {Avatar, CheckBox, Overlay} from 'react-native-elements';
import ImageView from 'react-native-image-viewing';
import {Card, Container, Content, Thumbnail} from 'native-base';
import {heightPercentageToDP as hp} from 'react-native-responsive-screen';
import axios from 'axios';
import Const from '../common/Constants';
import moment from 'moment';
import {CustomButton} from '../common/CustomButton';
import CustomModalTextArea from '../common/CustomModalTextArea';
import CustomLabel from '../common/CustomLabel';
import {CustomList2} from '../common/CustomList2';
import {Colors} from '../../utils/configs/Colors';
import {ApprovalStages} from '../common/ApprovalStages';
import Timeline from 'react-native-timeline-flatlist';
import {Image} from 'react-native';
const SubThemeColor = Colors.secondary;
const ThemeColor = Colors.header;
export default class ApprovalScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      LeaveDates: [],
      LeaveDatesOriginal: [],
      approverArr: [],
      Visible: false,
      request: this.props.route?.params?.request,
      LeaveItemData: this.props.route?.params?.LeaveItemData,
      user_id: this.props.route?.params?.user_id || '',
      institute_id: this.props.route?.params?.institute_id || '',
      Comment: '',
      approved:
        this.props?.route?.params?.LeaveItemData.status === 1 ? true : false,
      rejected:
        this.props?.route?.params?.LeaveItemData.status === 2 ? true : false,
    };
    //console.log('props = ', this.props);
  }
  componentDidMount() {
    this.arrayModifier();
  }
  arrayModifier = async () => {
    console.log('hit');
    //let LeaveDates = this.state.LeaveDates;
    // let Arr = this.props.route.params
    //   ? this.props.route.params.request.leaveDates
    //     ? this.props.route.params.request.leaveDates
    //     : []
    //   : [];
    //Arr.map((item, index) => {
    //  LeaveDates.push(item.leaveDates);
    //});
    //console.log('Arr - ', Arr);
    // let tempImage =
    //   this.props.route.params.LeaveItemData.requestDetails
    //     .requestedLeaveAttachment;
    // let images = [];
    // images.push({tempImage});
    const requestDetails = this.props.route.params.LeaveItemData.requestDetails;
    let ll = [];
    if (requestDetails.length > 0) {
      const leavesDatesArray =
        this.props.route.params.LeaveItemData.requestDetails.find((e, i, s) => {
          const length =
            this.props.route.params.LeaveItemData.leaveCategory === 7
              ? 0
              : this.props.route.params.LeaveItemData.requestDetails.length;
          return e.version === length;
        });
      const filtered = leavesDatesArray.leaveDetails.filter(
        (e) => e.firstHalf || e.secondHalf,
      );

      ll =
        this.props.route.params.LeaveItemData.leaveCategory === 5
          ? leavesDatesArray.leaveDetails.map((e) => {
              return {
                id: e.id,
                datesSelected: e.leaveAppliedDate,
                firstHalf: e.firstHalf,
                fromTime: e.fromTime,
                toTime: e.toTime,
                secondHalf: e.secondHalf,
                // disableMorning: e.firstHalf ? false : true,
                // disableEvening: e.secondHalf ? false : true,
                // isModified: false,
              };
            })
          : filtered.map((e) => {
              return {
                id: e.id,
                datesSelected: e.leaveAppliedDate,
                firstHalf: e.firstHalf,

                secondHalf: e.secondHalf,
                disableMorning: e.firstHalf ? false : true,
                disableEvening: e.secondHalf ? false : true,
                isModified: false,
              };
            });
    }
    // const firstVersion = this.props.route.params.LeaveItemData.requestDetails.find((e, i, s) => {
    //   const length = 1
    //   return e.version === length;
    // });

    //console.error('data', data);
    // if (data.length === 1) {
    //   return this.setState({LeaveDates: Arr, approverArr: []});
    // }

    const currentUser =
      this.props.route.params.LeaveItemData.approvalDetails.find(
        (each) => each.approverStaffCode.trim() === this.state.user_id,
      );
    console.log('currentUser', currentUser);
    let approversData = [];
    approversData =
      this.props.route.params.LeaveItemData.approvalDetails.length > 0 &&
      this.props.route.params.LeaveItemData.approvalDetails.filter(
        (each) => each.isActive === false,
      );
    let approverModifications = [];
    let data = [];
    if (requestDetails.length > 0) {
      this.props.route.params.LeaveItemData.requestDetails.forEach((item) =>
        data.push(...item.leaveDetails),
      );
      approversData.length > 0 &&
        approversData.forEach((e) => {
          data.forEach((w) => {
            //console.log('w', w);
            if (w.ownerOfVersion === e.approverStaffCode.trim()) {
              approverModifications.push({
                firstHalf: w.firstHalf,
                secondHalf: w.secondHalf,
                fromTime: w.fromTime,
                toTime: w.toTime,
                datesSelected: w.leaveAppliedDate,
                approverStaffCode: w.ownerOfVersion.trim(),
              });
            }
          });
        });
    }
    //console.warn('ww', approverModifications);
    const mod =
      approversData.length > 0 &&
      approversData.map((item) => {
        return {
          approverLevel: item.level,
          approverName: item.approverName || 'No Name',
          designation: item.approverDesignation || '',
          imagePath: item.image,
          status: item.status,
          reason: item.comments || 'No Comment',
          leaveDates:
            approverModifications.length === 0
              ? []
              : approverModifications.filter(
                  (e) => e.approverStaffCode === item.approverStaffCode,
                ),
        };
      });

    // this.props.route.params.LeaveItemData.leaveApprovalsAndRequests.map(
    //   (item, index) => {
    //     //console.log('itemmm = ', item);
    //     data.push({
    //       approverLevel: item.approverLevel,
    //       approverName: item.approverName || 'No Name',
    //       designation: item.approverDesignation,
    //       imagePath: item.approverImageUrl,
    //       status: item.status,
    //       reason: item.approverComment || 'No Comment',
    //     });
    //   },
    // );
    // LeaveItemData.leaveApprovalsAndRequests[0]
    //.requestedLeaveAttachment
    this.setState(
      {
        LeaveDates: ll || [],
        LeaveDatesOriginal: ll || [],
        approverArr: mod || [],
        currentUser,
      },
      () => {
        console.log(
          'LeaveDates = ',
          this.state.LeaveDates === this.state.LeaveDatesOriginal,
        );
      },
    );
  };
  submitHandler = () => {
    if (this.state.LeaveItemData.leaveCategory !== 8) {
      if (!this.state.approved && !this.state.rejected) {
        return alert('Either approve or reject the request first');
      }
    }
    if (!this.state.Comment) {
      return alert('Enter your comment before submitting');
    }

    console.log('I am ready to be submitted');
    this.approverResponse();
  };

  getArrayBasedOnLeaveCategory = () => {
    //const cat = this.state.LeaveItemData.leaveCategory;
    // if (cat === 3) {
    if (this.state.LeaveDates?.length > 0) {
      return this.state.LeaveDates.map((e) => {
        return {
          id: e.id,
          firstHalf: e.firstHalf,
          secondHalf: e.secondHalf,
          datesSelected: e.datesSelected,
          fromTime: e.fromTime,
          toTime: e.toTime,
        };
      });
    }
    return [];
    // }
  };

  checkIsModified = (datesArray) => {
    const res = datesArray.some(
      (e) => e.isMorningModified || e.isEveningModified,
    );
    console.log('isModified ==', res);
    return res;
  };

  approverResponse = async () => {
    const url = `${Const}api/Leave/AddUpdateLeaveRequestsFromApprover`;

    // if (this.state.request?.requestTypeId === 8) {
    let bodyData = {
      //"UpdatedLeaveDates": [
      //  {
      //    "Id": 0,
      //    "DatesSelected": "2021-07-29T10:07:25.647Z",
      //    "UpdatedFromTime": "2021-07-29T10:07:25.647Z",
      //    "UpdatedToTime": "2021-07-29T10:07:25.647Z",
      //    "secondHalf": true,
      //    "secondHalf": true
      //  }
      //],
      UpdatedLeaveDates: this.getArrayBasedOnLeaveCategory(),
      LeaveRequestId: this.state.LeaveItemData.leaveRequestId || 0,
      LeaveTypeId: this.state.LeaveItemData.leaveTypeId,
      InstituteId: parseInt(this.state.institute_id),
      StaffCode: this.state.LeaveItemData.requesterStaffCode.trim() || 'string',
      ApproverStaffCode: this.state.user_id.toString(),
      LeaveApprovalStatus:
        this.state.LeaveItemData.leaveCategory === 8
          ? 1
          : this.state.approved
          ? 1
          : this.state.rejected
          ? 2
          : 0,
      ApproverLevel: this.state.currentUser.level,
      Priority: this.state.LeaveItemData.priority || 0,
      CreatedDate:
        this.props.route.params.LeaveItemData?.requestDetails[0]
          ?.leaveDetails[0]?.createdDate || new Date().toISOString(),
      IsRequestModified: this.checkIsModified(this.state.LeaveDates),
      // this.state.LeaveDates === this.state.LeaveDatesOriginal ? false : true,
      //CreatedDate: this.state.request.createdDate,
      //ModifiedDate: this.state.request.modifiedDate,
      ModifiedDate: new Date().toISOString(),
      ApproverComment: this.state.Comment,
      UpdatedFromDate:
        this.state.LeaveItemData?.requestDetails[0]?.leaveDetails[0]
          ?.fromTime ||
        this.state.request.fromDate ||
        new Date().toISOString(),
      UpdatedEndDate:
        this.state.LeaveItemData?.requestDetails[0]?.leaveDetails[0]?.toTime ||
        this.state.request.endDate ||
        new Date().toISOString(),
    };
    console.log(url, bodyData);
    try {
      const response = await axios.post(url, bodyData);

      //console.log('response - ', response.data);
      this.setState({AddUpdateLeaveRequestsFromApprover: response.data}, () => {
        if (!response.data.status) {
          return alert(response.data?.message);
        }
        alert(response.data?.message);
        return this.props.navigation.navigate('Leaves');
      });
    } catch (e) {
      alert('approverResponse = ', e.message);
      console.log('approverResponse = ', e.message);
    }
  };

  renderDetail = (rowData, sectionID, rowID) => {
    let title = <Text style={[styles.title]}>{rowData.title}</Text>;
    var desc = null;
    if (rowData.description && rowData.imageUrl)
      desc = (
        <View style={styles.descriptionContainer}>
          <Image source={{uri: rowData.imageUrl}} style={styles.image} />
          <Text style={[styles.textDescription]}>{rowData.description}</Text>
        </View>
      );

    return (
      <View style={{flex: 1}}>
        {title}
        {desc}
      </View>
    );
  };

  render() {
    const {request, LeaveItemData, LeaveDates, LeaveDatesOriginal} = this.state;
    //console.log(LeaveDates === LeaveDatesOriginal);
    return (
      <View style={styles.container}>
        <Container>
          <SubHeader
            title="Approval Request"
            showBack={true}
            backScreen="Leaves"
            showBell={false}
            navigation={this.props.navigation}
          />

          <Content>
            {LeaveItemData.leaveCategory === 8 ? (
              <View style={{padding: 10}}>
                <CustomLabel title={'Complaint'} />
                <CustomLabel
                  labelStyle={{fontFamily: 'Poppins-Regular'}}
                  title={request?.complaintSummary || ''}
                />
                <CustomLabel title={'Suggestion'} />
                <CustomModalTextArea
                  disabled
                  value={request?.suggestionSummary || ''}
                />
                <CustomLabel title={'Comment'} />
                <CustomModalTextArea
                  value={this.state.Comment}
                  onChangeText={(Comment) => this.setState({Comment})}
                />
                <CustomButton
                  color={ThemeColor}
                  title={'Submit'}
                  onPress={this.submitHandler}
                  marginTop={8}
                />
              </View>
            ) : (
              <View
                style={{
                  margin: 5,
                  borderRadius: 10,
                }}>
                {/* {request?.fromDate && request?.endDate && (
                  <View
                    style={{
                      width: '90%',
                      alignSelf: 'center',
                      flexDirection: 'row',
                      justifyContent: 'space-between',
                    }}>
                    <Text
                      style={[
                        styles.text,
                        {
                          color: ThemeColor,
                          fontSize: 15,
                          marginRight: 20,
                          marginBottom: 5,
                        },
                      ]}>
                      From:{' '}
                      {request?.fromDate
                        ? moment(request.fromDate).format('DD.MM.YYYY')
                        : 'No dateeeeee'}
                    </Text>
                    <Text
                      style={[
                        styles.text,
                        {
                          color: ThemeColor,
                          fontSize: 15,
                          marginRight: 20,
                          marginBottom: 5,
                        },
                      ]}>
                      To:{' '}
                      {request?.endDate
                        ? moment(request.endDate).format('DD.MM.YYYY')
                        : 'No dateeeee'}
                    </Text>
                  </View>
                )} */}
                {/* {request?.fromTime && request?.endTime && (
                  <View
                    style={{
                      width: '90%',
                      flexDirection: 'row',
                      alignSelf: 'center',
                      justifyContent: 'space-between',
                    }}>
                    <Text
                      style={[
                        styles.text,
                        {
                          color: ThemeColor,
                          fontSize: 12,
                          marginRight: 20,
                          marginBottom: 5,
                        },
                      ]}>
                      {request?.fromTime
                        ? moment(request.fromTime).format('h: mm a')
                        : 'No date'}
                    </Text>
                    <Text
                      style={[
                        styles.text,
                        {
                          color: ThemeColor,
                          fontSize: 12,
                          marginRight: 20,
                          marginBottom: 5,
                        },
                      ]}>
                      {request?.endTime
                        ? moment(request.endTime).format('h: mm a')
                        : 'No date'}
                    </Text>
                  </View>
                )} */}
                <View
                  style={{
                    width: '100%',
                    alignSelf: 'center',
                    flexDirection: 'row',
                    alignItems: 'center',
                    justifyContent: 'space-evenly',
                    //backgroundColor: 'blue',
                    padding: 10,
                  }}>
                  <View style={{alignItems: 'center', width: '20%'}}>
                    <Thumbnail
                      square
                      style={{borderRadius: 10}}
                      //source={{uri: LeaveItemData.requestedImageUrl}}
                      source={
                        LeaveItemData.requestedImageUrl
                          ? {
                              uri: LeaveItemData.requestedImageUrl,
                            }
                          : require('../../assets/ic_proile1.png')
                      }
                    />
                  </View>
                  <View
                    style={{
                      alignItems: 'flex-start',
                      paddingLeft: 10,
                      width: '40%',
                    }}>
                    {LeaveItemData?.requesterStaffName && (
                      <Text
                        style={[
                          styles.text,
                          {color: ThemeColor, textTransform: 'capitalize'},
                        ]}>
                        {LeaveItemData.requesterStaffName}
                      </Text>
                    )}
                    {LeaveItemData.requestedMailId && (
                      <Text style={[styles.text, {color: 'black'}]}>
                        {LeaveItemData.requestedMailId}
                      </Text>
                    )}
                    {LeaveItemData?.requestedMobileNumber && (
                      <Text style={([styles.text], {color: 'black'})}>
                        {LeaveItemData.requestedMobileNumber}
                      </Text>
                    )}
                  </View>

                  <View
                    style={{
                      alignItems: 'center',
                      width: '40%',
                    }}>
                    <Text style={[styles.text, {color: ThemeColor}]}>
                      {LeaveItemData.leaveCategoryName}
                    </Text>
                    <Text style={[styles.text, {color: ThemeColor}]}>
                      {LeaveItemData.leaveTypeName}
                    </Text>
                    {LeaveItemData.leaveCategory === 5 ? (
                      <Text style={[styles.text, {color: 'black'}]}>
                        {moment
                          .utc(
                            this.props.route.params.original.leaveDetails[0]
                              .fromTime,
                          )
                          .local()
                          .format('h:mm a')}{' '}
                        to{' '}
                        {moment
                          .utc(
                            this.props.route.params.original.leaveDetails[0]
                              .toTime,
                          )
                          .local()
                          .format('h:mm a')}
                      </Text>
                    ) : (
                      LeaveItemData?.noOfDays !== 0 && (
                        <Text
                          style={[
                            styles.text,
                            {
                              color: 'black',
                              fontSize: 14,
                              //width: '100%',
                              alignSelf: 'center',
                              //marginRight: 20,
                              marginVertical: 5,
                            },
                          ]}>
                          {LeaveItemData.noOfDays}{' '}
                          {LeaveItemData.noOfDays === 1 ? 'day' : 'days'}
                        </Text>
                      )
                    )}
                    {/* {request?.fromDate && request?.endDate && (
                      <Text
                        style={[
                          styles.text,
                          {
                            color: 'black',
                            fontSize: 14,
                            textAlign: 'center',
                            //width: '100%',
                            //alignSelf: 'center',
                            //marginRight: 20,
                            //marginVertical: 5,
                          },
                        ]}>
                        (
                        {request?.fromDate
                          ? moment(request.fromDate).format('DD.MM.YYYY')
                          : ''}{' '}
                        to{' '}
                        {request?.endDate
                          ? moment(request.endDate).format('DD.MM.YYYY')
                          : ''}
                        )
                      </Text>
                    )} */}
                    {/* <Text style={[styles.text, {color: 'black'}]}>2/4</Text> */}
                  </View>
                </View>
                {LeaveItemData?.reason && (
                  <Text
                    style={[
                      styles.text,
                      {
                        color: '#7B7B7B',
                        fontSize: 12,
                        width: '90%',
                        //marginRight: 20,
                        alignSelf: 'center',
                        marginTop: 5,
                      },
                    ]}>
                    {LeaveItemData?.reason || 'No reason mentioned '}
                  </Text>
                )}
                {LeaveItemData?.leaveAttachment ? (
                  <TouchableOpacity
                    onPress={() => {
                      this.setState({Visible: true});
                    }}
                    style={{
                      width: '100%',
                      alignSelf: 'center',
                      marginVertical: 10,
                    }}>
                    <Thumbnail
                      large
                      square
                      style={{
                        borderRadius: 10,
                        width: '100%',
                        height: 100,
                        resizeMode: 'contain',
                      }}
                      source={{
                        uri: LeaveItemData.leaveAttachment,
                      }}
                    />
                  </TouchableOpacity>
                ) : null}
                <Overlay
                  overlayStyle={{alignSelf: 'center', width: '80%'}}
                  isVisible={this.state.Visible}
                  onBackdropPress={() => this.setState({Visible: false})}>
                  <View
                    style={{width: '100%', height: 300, alignSelf: 'center'}}>
                    <Image
                      style={{
                        height: '100%',
                        width: '100%',
                        resizeMode: 'contain',
                      }}
                      source={{
                        uri: LeaveItemData.leaveAttachment,
                      }}
                    />
                  </View>
                </Overlay>
                {this.state.LeaveDates.length ? (
                  <View
                    style={{
                      marginVertical: 20,
                      alignItems: 'center',
                      width: '100%',
                    }}>
                    <CustomList2
                      width="98%"
                      title1={
                        LeaveItemData.leaveCategory === 5
                          ? 'Permission Asked'
                          : 'Leave Dates'
                      }
                      color={SubThemeColor}
                      headerColor={ThemeColor}>
                      <ScrollView nestedScrollEnabled={true}>
                        {this.state.LeaveDates.length ? (
                          this.state.LeaveDates.map((item, index) => {
                            //console.log(item);
                            return (
                              <View
                                key={index}
                                style={[
                                  styles.headerContainer,
                                  {
                                    width: '100%',
                                    height: 40,
                                    flexDirection: 'row',
                                    borderRadius: 15,
                                    //justifyContent: 'space-evenly',
                                    alignItems: 'center',
                                    marginTop: 5,
                                  },
                                ]}>
                                <View
                                  style={[
                                    styles.headerTitleContainer,
                                    {
                                      width: '24%',
                                      height: '100%',
                                      flex:
                                        LeaveItemData.leaveCategory === 5
                                          ? null
                                          : 1,
                                      borderRadius: 10,
                                    },
                                  ]}>
                                  <Text
                                    style={[
                                      styles.text,
                                      {
                                        color: 'black',
                                      },
                                    ]}>
                                    {/*{moment(item.DatesSelected).format('DD.MM.YYYY')}*/}
                                    {moment(item.datesSelected).format(
                                      'DD.MM.YYYY',
                                    )}
                                  </Text>
                                </View>

                                {LeaveItemData.leaveCategory === 5 && (
                                  <Text style={[styles.text, {color: 'black'}]}>
                                    {/*{moment(item.DatesSelected).format('DD.MM.YYYY')}*/}
                                    {moment
                                      .utc(
                                        this.state.LeaveDates[index].fromTime,
                                      )
                                      .local()
                                      .format('h:mm a')}
                                  </Text>
                                )}

                                {LeaveItemData.leaveCategory === 5 && (
                                  <Text
                                    style={[
                                      styles.text,
                                      {color: 'black', marginHorizontal: 10},
                                    ]}>
                                    {/*{moment(item.DatesSelected).format('DD.MM.YYYY')}*/}
                                    To
                                  </Text>
                                )}
                                {LeaveItemData.leaveCategory === 5 && (
                                  <Text
                                    style={[
                                      styles.text,
                                      {color: 'black', marginHorizontal: 10},
                                    ]}>
                                    {/*{moment(item.DatesSelected).format('DD.MM.YYYY')}*/}
                                    {moment
                                      .utc(this.state.LeaveDates[index].toTime)
                                      .local()
                                      .format('h:mm a')}
                                  </Text>
                                )}
                                {LeaveItemData.leaveCategory !== 5 && (
                                  <CheckBox
                                    title={'Morning'}
                                    containerStyle={{
                                      backgroundColor: 'transparent',
                                    }}
                                    checked={
                                      this.state.LeaveDates[index].firstHalf
                                    }
                                    checkedColor={Colors.header}
                                    disabled={item.disableMorning}
                                    onPress={() => {
                                      let modifiedArr = this.state.LeaveDates;
                                      if (modifiedArr[index].firstHalf) {
                                        modifiedArr[index].firstHalf = false;
                                        modifiedArr[
                                          index
                                        ].isMorningModified = true;
                                        this.setState({
                                          LeaveDates: [...modifiedArr],
                                        });

                                        //alert(item.firstHalf);
                                        console.log(this.state.LeaveDates);
                                      } else {
                                        modifiedArr[index].firstHalf = true;
                                        modifiedArr[
                                          index
                                        ].isMorningModified = false;
                                        this.setState({
                                          LeaveDates: [...modifiedArr],
                                        });
                                        //alert(item.firstHalf);
                                        console.log(this.state.LeaveDates);
                                      }
                                    }}
                                  />
                                )}

                                {LeaveItemData.leaveCategory !== 5 && (
                                  <CheckBox
                                    title={'Evening'}
                                    containerStyle={{
                                      backgroundColor: 'transparent',
                                    }}
                                    checked={
                                      this.state.LeaveDates[index].secondHalf
                                    }
                                    disabled={item.disableEvening}
                                    checkedColor={Colors.header}
                                    onPress={() => {
                                      let modifiedArr = this.state.LeaveDates;
                                      if (modifiedArr[index].secondHalf) {
                                        console.log('if');
                                        modifiedArr[index].secondHalf = false;
                                        modifiedArr[
                                          index
                                        ].isEveningModified = true;
                                        this.setState({
                                          LeaveDates: [...modifiedArr],
                                        });

                                        console.log(this.state.LeaveDates);
                                      } else {
                                        console.log('else');
                                        modifiedArr[index].secondHalf = true;
                                        modifiedArr[
                                          index
                                        ].isEveningModified = false;
                                        this.setState({
                                          LeaveDates: [...modifiedArr],
                                        });

                                        console.log(this.state.LeaveDates);
                                      }
                                    }}
                                  />
                                )}

                                {/*) : null}*/}
                              </View>
                            );
                          })
                        ) : (
                          <View
                            style={{
                              marginTop: 35,
                              alignItems: 'center',
                              width: '100%',
                            }}>
                            <Text>No Data Found</Text>
                          </View>
                        )}
                      </ScrollView>
                    </CustomList2>
                  </View>
                ) : null}
                {LeaveItemData.leaveCategory === 7 && (
                  <View style={{padding: 10}}>
                    <CustomLabel title="Missed Punch Details" />
                    {LeaveItemData.requestDetails?.length > 0 &&
                      LeaveItemData.requestDetails[0].leaveDetails.map((e) => {
                        return (
                          <>
                            <Card style={{padding: 10}}>
                              <View
                                style={{
                                  width: '100%',
                                  flexDirection: 'row',
                                  justifyContent: 'space-between',
                                  alignItems: 'center',
                                }}>
                                <CustomLabel
                                  containerStyle={{width: '60%'}}
                                  title={'Missed Punch Date'}
                                  labelStyle={{color: Colors.header}}
                                />
                                <CustomLabel
                                  title={moment(e.leaveAppliedDate).format(
                                    'DD.MM.YYYY',
                                  )}
                                />
                              </View>
                              <View
                                style={{
                                  width: '100%',
                                  flexDirection: 'row',
                                  justifyContent: 'space-between',
                                  alignItems: 'center',
                                }}>
                                <CustomLabel
                                  containerStyle={{width: '60%'}}
                                  title={'Scheduled Punch Time'}
                                  labelStyle={{color: Colors.header}}
                                />
                                <CustomLabel
                                  title={moment(
                                    e.scheduledMissedPunchTime,
                                  ).format('h:mm a')}
                                />
                              </View>
                              <View
                                style={{
                                  width: '100%',
                                  flexDirection: 'row',
                                  justifyContent: 'space-between',
                                  alignItems: 'center',
                                }}>
                                <CustomLabel
                                  containerStyle={{width: '60%'}}
                                  title={'Actual Punch Time'}
                                  labelStyle={{color: Colors.header}}
                                />
                                <CustomLabel
                                  title={moment(e.actualMissedPunchTime).format(
                                    'h:mm a',
                                  )}
                                />
                              </View>
                              <View
                                style={{
                                  width: '100%',
                                  flexDirection: 'row',
                                  justifyContent: 'space-between',
                                  alignItems: 'center',
                                }}>
                                <CustomLabel
                                  containerStyle={{width: '60%'}}
                                  title={'Type'}
                                  labelStyle={{color: Colors.header}}
                                />
                                <CustomLabel
                                  title={
                                    e.missedPunchIsCheckIn
                                      ? 'Check In'
                                      : 'Check Out'
                                  }
                                />
                              </View>
                            </Card>
                          </>
                        );
                      })}
                  </View>
                )}
                <View
                  style={{
                    flexDirection: 'row',
                    width: '90%',
                    justifyContent: 'space-between',
                    alignItems: 'center',
                    alignSelf: 'center',
                    marginBottom: 20,
                  }}>
                  {/* <CheckBox
                    title={'Approve'}
                    checked={this.state.approved}
                    checkedColor={ThemeColor}
                    onPress={() =>
                      this.setState({
                        approved: !this.state.approved,
                        rejected: false,
                      })
                    }
                  /> */}
                  {/* <CheckBox
                    title={'Reject'}
                    checked={this.state.rejected}
                    checkedColor={ThemeColor}
                    onPress={() =>
                      this.setState({
                        rejected: !this.state.rejected,
                        approved: false,
                      })
                    }
                  /> */}
                </View>

                <CustomLabel title={'Comment'} />
                <CustomModalTextArea
                  value={this.state.Comment}
                  onChangeText={(Comment) => this.setState({Comment})}
                />
                <View
                  style={{
                    flexDirection: 'row',
                    alignSelf: 'center',
                    width: '60%',
                    justifyContent: 'space-between',
                    paddingVertical: 10,
                  }}>
                  <CustomButton
                    color={ThemeColor}
                    title={'Approve'}
                    onPress={() =>
                      this.setState({approved: true, rejected: false}, () =>
                        this.submitHandler(),
                      )
                    }
                    marginTop={8}
                  />
                  <CustomButton
                    color={ThemeColor}
                    title={'Reject'}
                    onPress={() =>
                      this.setState({approved: false, rejected: true}, () =>
                        this.submitHandler(),
                      )
                    }
                    marginTop={8}
                  />
                </View>
                {/* {LeaveItemData?.leaveAttachment ? (
                  <ImageView
                    //images={images}
                    images={[LeaveItemData.leaveAttachment]}
                    imageIndex={0}
                    visible={this.state.Visible}
                    onRequestClose={() => this.setState({Visible: false})}
                  />
                ) : null} */}
                {/* {LeaveItemData?.leaveApprovalsAndRequests.length &&
                LeaveItemData?.leaveApprovalsAndRequests[0]
                  .requestedImageUrl ? (
                  <ImageView
                    images={[
                      LeaveItemData.leaveApprovalsAndRequests[0]
                        .requestedImageUrl,
                    ]}
                    imageIndex={0}
                    visible={this.state.Visible2}
                    onRequestClose={() => this.setState({Visible2: false})}
                  />
                ) : null} */}
              </View>
            )}

            <View style={{margin: 5}}>
              {this.state.approverArr.length ? (
                <ApprovalStages
                  onPress={() => {}}
                  title={'Approval Stages'}
                  headerColor={ThemeColor}
                  Arr={this.state.approverArr}
                  color={SubThemeColor}
                  width={'95%'}
                  reverse
                />
              ) : (
                <View>
                  <Text style={{alignSelf: 'center', marginTop: 10}}></Text>
                </View>
              )}
            </View>
          </Content>
        </Container>
      </View>
    );
  }
}
const images = [require('../../assets/taskall.jpg')];
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fbfbfb',
  },
  item: {
    borderRadius: 15,
    margin: 10,
    alignSelf: 'center',
    backgroundColor: '#ffffff',
    borderColor: '#f1f1f1',
    borderWidth: 0.5,
    shadowColor: 'silver',
    shadowOffset: {width: 0, height: 0},
    shadowOpacity: 0.3,
    height: hp('5'),
  },
  label: {
    fontFamily: 'Poppins-SemiBold',
    fontSize: 15,
    color: '#000000',
  },
  labelContainer: {
    margin: '1.5%',
  },
  dateContainer: {
    backgroundColor: ThemeColor,
  },
  text: {
    fontFamily: 'Poppins-Regular',
    fontSize: 15,
    color: 'white',
  },
  text2: {
    fontFamily: 'Poppins-Regular',
    fontSize: 15,
    color: 'black',
    fontWeight: 'bold',
  },
  buttonStyle: {
    backgroundColor: SubThemeColor,
    //backgroundColor: "rgba(0, 0, 0, 0.1)",
    width: 200,
    //marginLeft: -100,
    justifyContent: 'flex-start',
  },
  imageStyle: {
    width: 20,
    height: 20,
    resizeMode: 'contain',
    marginHorizontal: 10,
  },
  headerContainer: {
    width: '100%',
    //height: 40,
    flexDirection: 'row',
    borderRadius: 15,
    justifyContent: 'space-evenly',
    alignItems: 'center',
    //paddingHorizontal: 10,
    paddingVertical: 10,
  },
  headerTitleContainer: {
    justifyContent: 'center',
    alignItems: 'center',
  },
});
