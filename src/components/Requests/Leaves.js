import {
  Container,
  Content,
  Thumbnail,
  Fab,
  Icon,
  Button,
  Textarea,
} from 'native-base';
import React, {Component} from 'react';
import {
  ActivityIndicator,
  FlatList,
  RefreshControl,
  SectionList,
} from 'react-native';
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  Dimensions,
  Image,
} from 'react-native';
import ImageView from 'react-native-image-viewing';
import {CheckBox, Overlay} from 'react-native-elements';
import {heightPercentageToDP as hp} from 'react-native-responsive-screen';
import {CustomTabs} from '../common/CustomTabs';
import SubHeader from '../common/SubHeader';
import NoData from '../common/NoData';
import AsyncStorage from '@react-native-community/async-storage';
import axios from 'axios';
import Const from '../common/Constants';
import moment from 'moment';
import Loader from '../common/Loader';
import CustomModal from '../common/CustomModal';
import {CustomButton} from '../common/CustomButton';
import CustomModalTextArea from '../common/CustomModalTextArea';
import CustomLabel from '../common/CustomLabel';
import {ExpandableListView} from 'react-native-expandable-listview';
import {ApprovalStages} from '../common/ApprovalStages';
import {Colors} from '../../utils/configs/Colors';
import {TouchableWithoutFeedback} from 'react-native';
import {Alert} from 'react-native';
const SubThemeColor = Colors.secondary;
const ThemeColor = Colors.header;
const screenWidth = Dimensions.get('window').width;
const screenHeight = Dimensions.get('window').height;

export default class Leaves extends Component {
  constructor(props) {
    super(props);
    this.state = {
      TempIndex: -1,
      LeaveType: '',
      LeaveArr: [],
      FabButtons: [],
      refreshing: false,
      ApproverLeaveArray: [],
      approvedListArray: [],
      ApprovalStages: [],
      //ActiveTab: 'Requested',
      ActiveTab: 'To Approve',
      fab: false,
      ApiLoader: false,
      user_id: '',
      institute_id: '',
      modalVisible: false,
      ModifiedApproverLeaveArray: [],
      request: null,
      approved: false,
      rejected: false,
      ActiveTab2: 'Leave',
    };
  }

  componentDidMount() {
    this.retrieveData('');
    this.focus = this.props.navigation.addListener('focus', () => {
      this.retrieveData('focus');
    });
    //this.clearInterval = setInterval(() => {
    //  console.log('timer hit');
    //  this.retrieveData();
    //}, 20000);
  }
  componentWillUnmount() {
    this.props.navigation.removeListener(this.focus);
    clearInterval(this.clearInterval);
  }
  retrieveData = async (path) => {
    try {
      const institute_id = await AsyncStorage.getItem('institute_id');

      const user_id = await AsyncStorage.getItem('user_id');
      if (institute_id && user_id) {
        this.setState({institute_id, user_id, ActiveTab2: 'Leave'}, () => {
          // if (path == 'focus') {
          //this.setState({ActiveTab: 'Requested'});
          this.getLeaveArray(institute_id, user_id);
          // } else {
          this.FabButtons(institute_id, user_id);
          this.getapproverLeaveArray(institute_id, user_id);
        });
      }
    } catch (e) {
      alert('Error retrieving data');
    }
  };

  FabButtons = async (instituteId, user_id) => {
    const url = `${Const}api/Leave/GetMasterLeaveTypesForInstitute`;
    console.log(url);
    const body = {
      staffCode: user_id,
      InstituteId: Number(instituteId),
      Date: new Date().toISOString(),
    };
    //console.log(body);
    try {
      const response = await axios.post(url, body);
      //console.log(url, 'FabButtons', response.data);
      this.setState({FabButtons: response.data});
    } catch (e) {
      //alert(`FabButtons = ${e.message}`);
      console.log('FabButtons = ', e.message);
    }
  };
  getLeaveArray = async (instituteId, approverStaffCode, newArr) => {
    this.setState({ApiLoader: true});
    const url = `${Const}api/Leave/GetRequestedLeavesByStaff/${instituteId}/${approverStaffCode}`;
    console.log(url);
    try {
      const response = await axios.get(url);
      //
      //console.log('requests ==>', JSON.stringify(response.data, null, 4));

      //return;
      let data = [];
      if (response.data?.requests?.length > 0) {
        data = response.data.requests.sort(
          (e1, e2) =>
            new Date(e1.createdDate).getTime() <
            new Date(e2.createdDate).getTime(),
        );
      }

      this.setState({
        ApproverLeaveArray: newArr ? newArr : data,
        //ApproverLeaveArray: response.data,
        refreshing: false,
        ApiLoader: false,
      });
    } catch (e) {
      this.setState({ApiLoader: false});
      alert('Error retrieving requests:' + e);
      console.log('getLeaveArray = ', e.message);
    }
  };

  getapproverLeaveArray = async (instituteId, approverStaffCode) => {
    this.setState({ApiLoader: true});
    const url = `${Const}api/Leave/GetLeaveRequestsForApprover/${instituteId}/${approverStaffCode}`;
    console.log(url);
    try {
      const response = await axios.get(url);

      // return;
      let data = [];
      if (response.data?.requests?.length > 0) {
        data = response.data.requests.sort(
          (e1, e2) =>
            new Date(e1.createdDate).getTime() <
            new Date(e2.createdDate).getTime(),
        );
        data = data.filter((e) => e.status === 0);
      }
      //console.log(data.filter((e) => e.status == 0).length);
      this.setState({
        LeaveArr: data,
        refreshing: false,
        ApiLoader: false,
      });
      //this.setState({
      //  LeaveArr: [...this.state.LeaveArr, ...response.data],
      //  refreshing: false,
      //});
    } catch (e) {
      alert('Error retrieving requests:' + e);
      this.setState({ApiLoader: false});
      console.log('getapproverLeaveArray = ', e);
    }
  };

  openModal = async (item, i, original) => {
    this.setState({Loader: true, LeaveItemData: item});
    console.log('item - ', item);
    const request = await this.getRequest(item);
    console.log('requesttt = ', request);
    if (!request) {
      this.setState({Loader: false});
      return alert('No request found');
    }
    this.setState(
      {
        request,
      },
      () =>
        this.setState({Loader: false}, () => {
          console.log('LeaveItemData = ', this.state.LeaveItemData);
          this.props.navigation.navigate('ApprovalScreen', {
            request: this.state.request,
            LeaveItemData: this.state.LeaveItemData,
            user_id: this.state.user_id,
            institute_id: this.state.institute_id,
            original,
            //ApproverArr2:ApproverArr2
          });
        }),
    );
  };
  getLeaveName = (id) => {
    //console.log('fab id = ', id);
    const findId = this.state.FabButtons.find((item) => item.id === id);
    //console.log('findId = ', findId);
    return findId.name;
  };
  getRequest = async (item) => {
    const LeaveId = item.leaveRequestId;
    const instituteId = item.instituteId;
    //const StaffCode = await AsyncStorage.getItem('user_id');
    const StaffCode = item.requesterStaffCode;
    let url = '';
    if (item.leaveCategory === 8) {
      url =
        'https://insproplus.com/palgeoapi/api/Leave/GetComplaintDetailsById/' +
        LeaveId;
    } else {
      url = `${Const}api/Leave/GetLeaveRequestDetailsByRequestId/${LeaveId}/${instituteId}/${StaffCode}`;
      //url = `${Const}api/Leave/GetLeaveRequestDetailsByRequestId/${LeaveId}/${instituteId}/${this.state.request.staffCode}`;
    }
    console.log('url ===== ', url);
    try {
      const response = await axios.get(url);
      console.log('getRequesttt response = ', response.data);
      return response.data;
    } catch (e) {
      //alert('getRequest = ', e.message);
      console.log('getRequest = ', e.message);
      return null;
    }
  };

  renderEmpty = () => {
    let emptyMessage = null;
    if (!this.state.ApiLoader) {
      if (
        (this.state.LeaveArr.length === 0 &&
          this.state.ActiveTab === 'To Approve') ||
        (this.state.ApproverLeaveArray.length === 0 &&
          this.state.ActiveTab === 'Requested') ||
        (this.state.approvedListArray.length === 0 &&
          this.state.ActiveTab === 'Approved')
      ) {
        emptyMessage = (
          <CustomLabel
            containerStyle={{alignSelf: 'center'}}
            title={'No Requests found'}
          />
        );
      }
    }
    return emptyMessage;
  };

  renderItem = (item, index) => {
    console.log('item = ', item);
    return (
      <TouchableOpacity
        style={{backgroundColor: SubThemeColor}}
        disabled={this.state.ActiveTab === 'Requested'}
        onPress={() => this.openModal(item, index)}>
        <View
          key={index}
          style={{
            margin: 5,
            borderRadius: 10,
            backgroundColor: index % 2 ? 'white' : 'transparent',
          }}>
          <Text
            numberOfLines={4}
            style={[
              styles.text,
              {
                color: '#7B7B7B',
                fontSize: 12,
                width: '50%',
                marginRight: 20,
                alignSelf: 'center',
                marginTop: 5,
              },
            ]}>
            {item.reason || 'No comment'}
          </Text>
          <View
            style={{
              width: '100%',
              flexDirection: 'row',
              alignItems: 'center',
            }}>
            <View
              style={[
                styles.headerTitleContainer,
                {
                  alignItems: 'flex-start',
                  paddingLeft: 10,
                  flex: 0.5,
                },
              ]}></View>
            <View
              style={[styles.headerTitleContainer, {alignItems: 'flex-start'}]}>
              <Text
                style={[
                  styles.text,
                  {
                    color: 'black',
                    fontSize: 12,
                    marginRight: 20,
                    marginVertical: 5,
                  },
                ]}>
                No. of days {item.noOfDays.toString() || '0'}
              </Text>
              <Text
                style={[
                  styles.text,
                  {
                    color: ThemeColor,
                    fontSize: 12,
                    marginRight: 20,
                    marginBottom: 5,
                  },
                ]}>
                {item.modifiedDate
                  ? moment(item.modifiedDate).format('Do MMM, YYYY')
                  : item.createdDate
                  ? moment(item.createdDate).format('Do MMM, YYYY')
                  : ''}
              </Text>
            </View>
            <View
              style={[
                styles.headerTitleContainer,
                {alignItems: 'flex-end', marginRight: 20},
              ]}>
              <View
                style={{
                  backgroundColor: '#D8F4F4',
                  justifyContent: 'center',
                  alignItems: 'center',
                  borderRadius: 10,
                  paddingHorizontal: 8,
                  paddingVertical: 4,
                }}>
                <Text
                  style={[
                    styles.text,
                    {
                      color:
                        item.status === 1
                          ? 'green'
                          : item.status === 2
                          ? 'red'
                          : 'orange',
                      fontSize: 13,
                    },
                  ]}>
                  {/*{item.status === 0
                    ? 'Pending'
                    : item.status === 1
                    ? 'Approved'
                    : 'Unknown'}*/}
                  {item.overAllStatus === 0
                    ? 'Pending'
                    : item.overAllStatus === 1
                    ? 'Approved'
                    : 'Rejected'}
                </Text>
              </View>
            </View>
          </View>
        </View>
      </TouchableOpacity>
    );
  };
  renderItem2 = ({item, index}) => {
    const original = item.requestDetails.find(
      (e) => e.version == 1 || e.version == 0,
    );
    //console.log('original', original);
    const ApproverArr = item.approvalDetails.map((item2, index2) => {
      return {
        approverLevel: item2.approverLevel,
        designation: item2.approverDesignation,
        approverName: item2.approverStaffName,
        imagePath: item2.approverImage,
        reason: item2.approverComment,
        status: item2.leaveApprovalStatus,
      };
    });
    return (
      <TouchableWithoutFeedback
        onLongPress={() =>
          Alert.alert(
            'Attention',
            'Are you sure you want to delete the request?',
            [
              {
                text: 'Yes',
                onPress: () => {
                  let newArr = this.state.ApproverLeaveArray.filter(
                    (e, i) => index !== i,
                  );
                  this.getLeaveArray(
                    this.state.institute_id,
                    this.state.user_id,
                    newArr,
                  );
                },
              },
              {text: 'No', onPress: () => {}, style: 'cancel'},
            ],
            {cancelable: true},
          )
        }
        // disabled={this.state.ActiveTab === 'Requested'}
        onPress={() => {
          if (this.state.ActiveTab === 'Requested') {
            if (item.status === 1 || item.status === 2) {
              this.props.navigation.navigate('RequestDetails', {
                item,
              });
              return;
            }

            if (this.state.TempIndex == index) {
              this.setState({TempIndex: -1});
            } else {
              this.setState({TempIndex: index});
            }
            return;
          }
          if (this.state.ActiveTab === 'Approved') {
            this.props.navigation.navigate('RequestDetails', {
              item,
              path: 'approved',
            });
            return;
          }
          this.openModal(item, index, original);
        }}>
        <View>
          <View
            key={index}
            style={{
              margin: 5,
              borderRadius: 10,
              backgroundColor: index % 2 ? 'white' : SubThemeColor,
            }}>
            <View
              style={{
                width: '100%',
                alignSelf: 'center',
                flexDirection: 'row',
                //alignItems: 'center',
                paddingVertical: 10,
              }}>
              <View
                style={{
                  alignItems: 'center',
                  width: '10%',
                }}>
                <Text style={[styles.text, {color: 'black'}]}>
                  {index + 1 + '.'}
                </Text>
              </View>
              <View
                style={{
                  width: '45%',
                  //alignItems: 'center',
                  //flexDirection: 'row',
                  justifyContent: 'flex-start',
                }}>
                <View>
                  <Thumbnail
                    square
                    style={{borderRadius: 10}}
                    source={{uri: item.requestedImageUrl}}
                    defaultSource={require('../../assets/ic_proile1.png')}
                    // source={
                    //   item.requestedImageUrl
                    //     ? item.requestedImageUrl
                    //     : require('../../assets/ic_proile1.png')
                    // }
                    //source={require('../../assets/taskall.jpg')}
                  />
                </View>
                <View style={{marginHorizontal: 10, flex: 1}}>
                  <Text
                    style={[
                      styles.text,
                      {color: ThemeColor, textTransform: 'capitalize'},
                    ]}>
                    {item.requesterStaffName || 'No name'}
                  </Text>
                  <Text
                    style={[styles.text, {color: ThemeColor, fontSize: 12}]}>
                    {item.requestedDesignation || ' '}
                  </Text>
                  <Text
                    style={[styles.text, {color: ThemeColor, fontSize: 12}]}>
                    {item.requestedDepartment || ' '}
                  </Text>
                  {item?.requestedMailId && (
                    <Text
                      style={[styles.text, {color: ThemeColor, fontSize: 12}]}>
                      {item.requestedMailId}
                    </Text>
                  )}
                  {item?.requestedMobileNumber && (
                    <Text
                      style={[styles.text, {color: ThemeColor, fontSize: 12}]}>
                      {item.requestedMobileNumber}
                    </Text>
                  )}
                </View>
              </View>

              <View
                style={{
                  alignItems: 'center',
                  width: '45%',
                  //justifyContent: 'space-evenly',
                }}>
                {item?.leaveTypeName && (
                  <Text style={[styles.text, {color: ThemeColor}]}>
                    {item.leaveTypeName}
                    {/*{item.leaveTypeId ? this.getLeaveName(item.leaveTypeId) : null}*/}
                  </Text>
                )}
                {item.createdDate && (
                  <View style={{alignItems: 'center'}}>
                    <Text style={[styles.text, {color: 'black', fontSize: 14}]}>
                      {moment(item.createdDate).format('DD.MM.YYYY')}
                    </Text>
                    <Text style={[styles.text, {color: 'gray', fontSize: 12}]}>
                      {moment(item.createdDate).format('h:mm a')}
                    </Text>
                  </View>
                )}

                {item.leaveCategory === 5 ? (
                  <Text style={[styles.text, {color: 'black', fontSize: 13}]}>
                    {moment
                      .utc(original.leaveDetails[0].fromTime)
                      .local()
                      .format('h:mm a')}{' '}
                    to{' '}
                    {moment
                      .utc(original.leaveDetails[0].toTime)
                      .local()
                      .format('h:mm a')}
                  </Text>
                ) : (
                  item?.noOfDays !== 0 && (
                    <View>
                      <Text style={[styles.text, {color: 'black'}]}>
                        {item.noOfDays.toString()}{' '}
                        {item?.noOfDays <= 1 ? 'day' : 'days'}
                        {/*{item.leaveTypeId ? this.getLeaveName(item.leaveTypeId) : null}*/}
                      </Text>
                      {/* <Text style={[styles.text, {color: 'black'}]}>
                        {moment
                          .utc(original?.leaveDetails[0].fromDate)
                          .local()
                          .format('DD.MM.YYYY')}{' '}
                        to{' '}
                        {moment
                          .utc(original?.leaveDetails[0].endDate)
                          .local()
                          .format('DD.MM.YYYY')}
                      </Text> */}
                    </View>
                  )
                )}
                <CustomLabel
                  title={
                    moment(original?.leaveDetails[0].leaveAppliedDate).format(
                      'DD.MM.YYYY',
                    ) || ''
                  }
                  labelStyle={{color: Colors.header}}
                />
                <View
                  style={{
                    backgroundColor: '#D8F4F4',
                    justifyContent: 'center',
                    alignItems: 'center',
                    borderRadius: 10,
                    paddingHorizontal: 8,
                    paddingVertical: 4,
                  }}>
                  <Text
                    style={[
                      styles.text,
                      {
                        color:
                          item.status === 1
                            ? 'green'
                            : item.status === 2
                            ? 'red'
                            : 'orange',
                        fontSize: 13,
                      },
                    ]}>
                    {/*{item.overAllStatus === 0
                    ? 'Pending'
                    : item.overAllStatus === 1
                    ? 'Approved'
                    : 'Unknown'}*/}
                    {item.status === 0
                      ? 'Pending'
                      : item.status === 1
                      ? 'Approved'
                      : 'Rejected'}
                  </Text>
                </View>
              </View>
            </View>

            <Text
              numberOfLines={5}
              style={[
                styles.text,
                {
                  width: '90%',
                  alignSelf: 'center',

                  // fontSize: 18,
                  textTransform: 'capitalize',
                  color: 'gray',
                  fontStyle: 'italic',

                  //marginTop: 5,
                  paddingHorizontal: 10,
                },
              ]}>
              {item.reason ? `"${item.reason}"` : 'No comment/reason'}
            </Text>
            <View
              style={{
                width: '100%',
                alignSelf: 'center',
                flexDirection: 'row',
                alignItems: 'center',
                paddingHorizontal: 10,
                justifyContent: 'space-between',
              }}></View>
          </View>

          {/* {item?.requestedImageUrl && (
          <ImageView
            images={[item.requestedImageUrl]}
            imageIndex={0}
            visible={this.state.Visible}
            onRequestClose={() => this.setState({Visible: false})}
          />
        )} */}
          {this.state.TempIndex == index && (
            <View style={{margin: 5}}>
              {item.approvalDetails.length ? (
                <ApprovalStages
                  //horizontal
                  onPress={() => {}}
                  width="100%"
                  key={1}
                  title="Approval Stages"
                  color={SubThemeColor}
                  headerColor={ThemeColor}
                  Arr={ApproverArr}
                />
              ) : (
                <View>
                  <Text style={{alignSelf: 'center', marginTop: 10}}>
                    No Data Found
                  </Text>
                </View>
              )}
            </View>
          )}
        </View>
      </TouchableWithoutFeedback>
    );
  };
  _onRefresh() {
    this.setState({refreshing: true});
    if (this.state.ActiveTab == 'To Approve') {
      this.getapproverLeaveArray(this.state.institute_id, this.state.user_id);
      return;
    }
    if (this.state.ActiveTab == 'Approved') {
      this.getApprovedListArray(this.state.institute_id, this.state.user_id);
      return;
    }
    if (this.state.ActiveTab == 'Requested') {
      this.setState({ActiveTab2: 'Leave'});
      this.getLeaveArray(this.state.institute_id, this.state.user_id);

      return;
    }
  }

  getApprovedListArray = async (instituteId, approverStaffCode) => {
    this.setState({ApiLoader: true});
    const url = `${Const}api/Leave/GetApprovedLeaveRequestsByApprover/${instituteId}/${approverStaffCode}`;
    console.log(url);
    try {
      const response = await axios.get(url);

      // return;
      let data = [];
      if (response.data?.requests?.length > 0) {
        data = response.data.requests.sort(
          (e1, e2) =>
            new Date(e1.createdDate).getTime() <
            new Date(e2.createdDate).getTime(),
        );
        data = data.filter((e) => e.status === 1);
      }
      //console.log(data.filter((e) => e.status == 0).length);
      this.setState({
        approvedListArray: data,
        refreshing: false,
        ApiLoader: false,
      });
      //this.setState({
      //  LeaveArr: [...this.state.LeaveArr, ...response.data],
      //  refreshing: false,
      //});
    } catch (e) {
      alert('Error retrieving requests:' + e);
      this.setState({ApiLoader: false});
      console.log('getapproverLeaveArray = ', e);
    }
  };

  render() {
    if (this.state.Loader) {
      return <Loader />;
    }
    return (
      <View style={styles.container}>
        {/*{this.state.modalVisible && (
          <CustomModal
            isVisible={this.state.modalVisible}
            title={`Request Details`}
            deleteIconPress={() => this.setState({modalVisible: false})}>
            {requestComponent}
          </CustomModal>
        )}*/}
        <Container>
          <SubHeader
            title="R-Dashbord"
            showBack={true}
            backScreen="Home"
            showBell={false}
            navigation={this.props.navigation}
          />

          <View style={{flex: 1}}>
            <CustomTabs
              borderRadius={20}
              height={50}
              width={'95%'}
              textSize={15}
              color={ThemeColor}
              backgroundColor={SubThemeColor}
              ActiveTab={this.state.ActiveTab}
              tab1Width={'33%'}
              tab2Width={'33%'}
              tab3Width={'33%'}
              tab1="To Approve"
              tab2="Requested"
              tab3="Approved"
              onPress={(value) => {
                this.setState({ActiveTab: value}, function () {
                  if (this.state.ActiveTab == 'To Approve') {
                    this.getapproverLeaveArray(
                      this.state.institute_id,
                      this.state.user_id,
                    );
                    console.log('Selected tab = ', value);
                    return;
                  }
                  if (this.state.ActiveTab == 'Requested') {
                    this.getLeaveArray(
                      this.state.institute_id,
                      this.state.user_id,
                    );
                    console.log('Selected tab = ', value);
                    return;
                  }
                  if (this.state.ActiveTab == 'Approved') {
                    this.getApprovedListArray(
                      this.state.institute_id,
                      this.state.user_id,
                    );
                    console.log('Selected tab = ', value);
                    return;
                  }
                  //this.getRequestArray();
                });
              }}
            />

            <View
              style={{
                width: '95%',
                alignSelf: 'center',
                backgroundColor: 'white',
                borderRadius: 10,
                marginBottom: 20,
              }}>
              <View
                style={[styles.headerContainer, {backgroundColor: ThemeColor}]}>
                <View
                  style={[
                    styles.headerTitleContainer,
                    {alignItems: 'flex-start', paddingLeft: 10, flex: 0.5},
                  ]}>
                  <Text style={styles.text}>Sr.No</Text>
                </View>
                <View style={styles.headerTitleContainer}>
                  <Text style={styles.text}>{this.state.ActiveTab}</Text>
                </View>
                <View
                  style={[styles.headerTitleContainer, {alignItems: 'center'}]}>
                  <Text style={styles.text}>
                    {this.state.ActiveTab == 'Requested'
                      ? 'Date/Days'
                      : 'Type/Stage'}
                  </Text>
                </View>
              </View>
              {/*{this.state.ApiLoader ? (
                <View style={{alignSelf: 'center', margin: 10}}>
                  <ActivityIndicator color={ThemeColor} />
                </View>
              ) : null}*/}
              {this.state.ActiveTab == 'To Approve' &&
                this.state.LeaveArr?.length > 0 && (
                  <View
                    style={{
                      //paddingBottom: 20,
                      marginBottom: Dimensions.get('window').height * 0.2,
                    }}>
                    <FlatList
                      keyExtractor={(item, index) => index + ''}
                      data={this.state.LeaveArr}
                      renderItem={this.renderItem2}
                      refreshControl={
                        <RefreshControl
                          style={{backgroundColor: '#E0FFFF'}}
                          refreshing={this.state.refreshing}
                          onRefresh={this._onRefresh.bind(this)}
                          tintColor="#ff0000"
                          title="Loading..."
                          titleColor="#00ff00"
                          //colors={['#ff0000', '#00ff00', '#0000ff']}
                          //progressBackgroundColor="#ffff00"
                        />
                      }
                      refreshing={this.state.refreshing}
                      // ListEmptyComponent={() => (
                      //   <NoData
                      //     //containerStyle={{alignSelf: 'center'}}
                      //     title={'No requests found'}
                      //   />
                      // )}
                    />
                  </View>
                )}
              {this.state.ApiLoader && (
                <View>
                  <ActivityIndicator size={30} />
                  <CustomLabel
                    containerStyle={{alignSelf: 'center'}}
                    title={'Fetching requests'}
                  />
                </View>
              )}
              {this.state.ActiveTab == 'Requested' &&
                this.state.ApproverLeaveArray?.length > 0 && (
                  <View
                    style={{
                      //paddingBottom: 20,
                      marginBottom: Dimensions.get('window').height * 0.2,
                    }}>
                    <FlatList
                      data={this.state.ApproverLeaveArray}
                      renderItem={this.renderItem2}
                      keyExtractor={(item, i) => i + ''}
                      refreshControl={
                        <RefreshControl
                          style={{backgroundColor: '#E0FFFF'}}
                          refreshing={this.state.refreshing}
                          onRefresh={this._onRefresh.bind(this)}
                          tintColor="#ff0000"
                          title="Loading..."
                          titleColor="#00ff00"
                          //colors={['#ff0000', '#00ff00', '#0000ff']}
                          //progressBackgroundColor="#ffff00"
                        />
                      }
                      refreshing={this.state.refreshing}
                    />
                  </View>
                )}
              {this.state.ActiveTab == 'Approved' &&
                this.state.approvedListArray?.length > 0 && (
                  <View
                    style={{
                      //paddingBottom: 20,
                      marginBottom: Dimensions.get('window').height * 0.2,
                    }}>
                    <FlatList
                      data={this.state.approvedListArray}
                      renderItem={this.renderItem2}
                      keyExtractor={(item, i) => i + ''}
                      refreshControl={
                        <RefreshControl
                          style={{backgroundColor: '#E0FFFF'}}
                          refreshing={this.state.refreshing}
                          onRefresh={this._onRefresh.bind(this)}
                          tintColor="#ff0000"
                          title="Loading..."
                          titleColor="#00ff00"
                          //colors={['#ff0000', '#00ff00', '#0000ff']}
                          //progressBackgroundColor="#ffff00"
                        />
                      }
                      refreshing={this.state.refreshing}
                    />
                  </View>
                )}
              {/* {this.state.ActiveTab == 'Requested' &&
                this.state.ApproverLeaveArray?.length > 0 &&
                this.state.ApproverLeaveArray.map((item, index) => {
                  const original = item.requestDetails.find(
                    (e) => e.version == 1,
                  );
                  const ApproverArr = item.approvalDetails.map(
                    (item2, index2) => {
                      return {
                        approverLevel: item2.approverLevel,
                        designation: item2.approverDesignation,
                        approverName: item2.approverStaffName,
                        imagePath: item2.approverImage,
                        reason: item2.approverComment,
                        status: item2.leaveApprovalStatus,
                      };
                    },
                  );
                  return (
                    <View
                      style={{
                        width: '100%',
                        marginTop: 15,
                        borderRadius: 10,
                        elevation: 3,
                        shadowColor: ThemeColor,
                        shadowOffset: {width: 0, height: 0},
                        shadowOpacity: 0.5,
                        backgroundColor: index % 2 ? 'white' : SubThemeColor,
                      }}>
                      <TouchableOpacity
                        onPress={() => {
                          if (item.status === 1) {
                            this.props.navigation.navigate('RequestDetails', {
                              item,
                            });
                            return;
                          }
                          if (this.state.TempIndex == index) {
                            this.setState({TempIndex: -1});
                          } else {
                            this.setState({TempIndex: index});
                          }
                        }}
                        style={{
                          flexDirection: 'row',
                          width: '100%',
                          //alignItems: 'center',
                          justifyContent: 'space-around',
                          padding: 10,
                          marginBottom: 5,
                          borderRadius: 5,
                        }}>
                        <View
                          style={{
                            width: 30,
                            alignItems: 'center',
                            //justifyContent: 'center',
                          }}>
                          <Text style={[styles.text3]}>{index + 1}.</Text>
                        </View>
                        <View
                          style={{
                            //marginHorizontal: 15,
                            //fle;x: 1,
                            width: '50%',
                          }}>
                          <Text
                            numberOfLines={3}
                            style={[styles.text3, {color: 'gray'}]}>
                            {moment(item.createdDate).format('DD.MM.YYYY')}
                          </Text>
                          <Text
                            numberOfLines={3}
                            style={[
                              styles.text3,
                              {
                                fontSize: 18,
                                textTransform: 'capitalize',
                                color: ThemeColor,
                                fontWeight: 'bold',
                              },
                            ]}>
                            {item.leaveTypeName}
                          </Text>
                          {item.leaveCategory === 3 ||
                            (item.leaveCategory === 4 && (
                              <Text
                                numberOfLines={3}
                                style={[styles.text3, {color: 'gray'}]}>
                                {moment(
                                  original.leaveDetails[0].fromTime,
                                ).format('DD.MM.YYYY')}{' '}
                                to{' '}
                                {moment(original.leaveDetails[0].toTime).format(
                                  'DD.MM.YYYY',
                                )}
                              </Text>
                            ))}
                          <Text
                            numberOfLines={3}
                            style={[
                              styles.text3,
                              {
                                // fontSize: 18,
                                textTransform: 'capitalize',
                                color: 'gray',
                                fontStyle: 'italic',
                              },
                            ]}>
                            "{item.reason}"
                          </Text>
                        </View>

                        <View style={{width: '35%'}}>
                          {item.leaveCategory === 5 ? (
                            <Text
                              style={[
                                styles.text3,
                                {fontSize: 13, textAlign: 'center'},
                              ]}>
                              {moment
                                .utc(original.leaveDetails[0].fromTime)
                                .local()
                                .format('h:mm a')}{' '}
                              to{' '}
                              {moment
                                .utc(original.leaveDetails[0].toTime)
                                .local()
                                .format('h:mm a')}
                            </Text>
                          ) : (
                            item.noOfDays != 0 && (
                              <Text
                                style={[
                                  styles.text3,
                                  {fontSize: 13, textAlign: 'center'},
                                ]}>
                                {item.noOfDays}{' '}
                                {item.noOfDays === 1 ? 'day' : 'days'}
                              </Text>
                            )
                          )}

                          <Text
                            style={[
                              styles.text,
                              {
                                color:
                                  item.status === 1
                                    ? 'green'
                                    : item.status === 2
                                    ? 'red'
                                    : 'orange',
                                fontSize: 13,
                                backgroundColor: '#D8F4F4',
                                textAlign: 'center',
                                borderRadius: 10,
                                paddingHorizontal: 8,
                                paddingVertical: 4,
                              },
                            ]}>
                            {item.status === 0
                              ? 'Pending'
                              : item.status === 1
                              ? 'Approved'
                              : 'Rejected'}
                          </Text>
                        </View>
                      </TouchableOpacity>
                      {this.state.TempIndex == index && (
                        <View style={{margin: 5}}>
                          {item.approvalDetails.length ? (
                            <ApprovalStages
                              onPress={() => {}}
                              width="100%"
                              key={1}
                              title="Approval Stages"
                              color={SubThemeColor}
                              headerColor={ThemeColor}
                              Arr={ApproverArr}
                            />
                          ) : (
                            <View>
                              <Text
                                style={{alignSelf: 'center', marginTop: 10}}>
                                No Data Found
                              </Text>
                            </View>
                          )}
                        </View>
                      )}
                    </View>
                  );
                })} */}
              {this.renderEmpty()}
              {/* {!this.state.ApiLoader ? (
                (this.state.ApproverLeaveArray.length === 0 &&
                  this.state.ActiveTab === 'To Approve') ||
                (this.state.LeaveArr.length === 0 &&
                  this.state.ActiveTab === 'Requested') ? (
                  //<Text style={{alignSelf:"center",marginTop:10}}>No Data Available</Text>
                  <CustomLabel
                    containerStyle={{alignSelf: 'center'}}
                    title={'No Requests found'}
                  />
                ) : null
              ) : null} */}
            </View>
          </View>
          {this.state.fab && (
            <Overlay
              isVisible={this.state.fab}
              onBackdropPress={() => this.setState({fab: false})}
              overlayStyle={{
                position: 'absolute',
                bottom: 50,
                right: 0,
                backgroundColor: 'transparent',
                //width: 105
              }}>
              {this.state.FabButtons.map((item, index) => {
                return (
                  <Button
                    key={index}
                    onPress={() => {
                      this.setState({fab: false});
                      if (item.id == 3) {
                        this.props.navigation.navigate('LeaveRequest', {
                          SelectedLeaveId: item.id,
                        });
                      } else if (item.id == 4) {
                        this.props.navigation.navigate('OutsideDuty', {
                          SelectedLeaveId: item.id,
                        });
                      } else if (item.id == 5) {
                        this.props.navigation.navigate('Permission', {
                          SelectedLeaveId: item.id,
                        });
                      } else if (item.id == 8) {
                        this.props.navigation.navigate('Complaints', {
                          SelectedLeaveId: item.id,
                        });
                      } else if (item.id == 7) {
                        this.props.navigation.navigate('Punch', {
                          SelectedLeaveId: item.id,
                        });
                      } else {
                        null;
                      }
                    }}
                    style={styles.buttonStyle}>
                    <Image
                      style={styles.imageStyle}
                      source={
                        item.id == 3
                          ? require('../../assets/leave.png')
                          : item.id == 4
                          ? require('../../assets/od.png')
                          : item.id == 5
                          ? require('../../assets/permission.png')
                          : item.id == 8
                          ? require('../../assets/complaint.png')
                          : item.id == 7
                          ? require('../../assets/punch.png')
                          : null
                      }
                    />
                    <Text style={styles.text2}>{item.name}</Text>
                  </Button>
                );
              })}
            </Overlay>
          )}
        </Container>
        <TouchableOpacity
          onPress={() => this.setState({fab: !this.state.fab})}
          style={{
            width: 60,
            height: 60,
            borderRadius: 60,
            justifyContent: 'center',
            alignItems: 'center',
            backgroundColor: ThemeColor,
            position: 'absolute',
            bottom: 10,
            right: 10,
            zIndex: 2,
            marginLeft: screenWidth - 80,
            marginTop: screenHeight - 80,
            elevation: 3,
            shadowOffset: {width: 0, height: 0},
            shadowColor: SubThemeColor,
            shadowOpacity: 0.5,
          }}>
          <Text
            style={[
              styles.text,
              {color: SubThemeColor, fontSize: 18, fontWeight: 'bold'},
            ]}>
            +
          </Text>
        </TouchableOpacity>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fbfbfb',
  },
  item: {
    borderRadius: 15,
    margin: 10,
    alignSelf: 'center',
    backgroundColor: '#ffffff',
    borderColor: '#f1f1f1',
    borderWidth: 0.5,
    shadowColor: 'silver',
    shadowOffset: {width: 0, height: 0},
    shadowOpacity: 0.3,
    height: hp('5'),
  },
  label: {
    fontFamily: 'Poppins-SemiBold',
    fontSize: 15,
    color: '#000000',
  },
  labelContainer: {
    margin: '1.5%',
  },
  dateContainer: {
    backgroundColor: ThemeColor,
  },
  text: {
    fontFamily: 'Poppins-Regular',
    fontSize: 15,
    color: 'white',
  },
  text3: {
    fontFamily: 'Poppins-Regular',
    fontSize: 15,
  },
  text2: {
    fontFamily: 'Poppins-Regular',
    fontSize: 15,
    color: 'black',
    fontWeight: 'bold',
  },
  buttonStyle: {
    backgroundColor: SubThemeColor,
    //backgroundColor: "rgba(0, 0, 0, 0.1)",
    width: 200,
    //marginLeft: -100,
    justifyContent: 'flex-start',
  },
  imageStyle: {
    width: 20,
    height: 20,
    resizeMode: 'contain',
    marginHorizontal: 10,
  },
  headerContainer: {
    width: '100%',
    //height: 40,
    flexDirection: 'row',
    borderRadius: 15,
    justifyContent: 'space-evenly',
    alignItems: 'center',
    //paddingHorizontal: 10,
    paddingVertical: 10,
  },
  headerTitleContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  sectionHeaderStyle: {
    //backgroundColor: 'white',
    fontSize: 20,
    padding: 5,
    //marginTop: 20,
    color: ThemeColor,
  },
  sectionListItemStyle: {
    fontSize: 15,
    padding: 5,
    paddingVertical: 10,
    color: 'black',
    backgroundColor: 'white',
  },
  listItemSeparatorStyle: {
    height: 0.5,
    width: '100%',
    //backgroundColor: '#C8C8C8',
  },
});
