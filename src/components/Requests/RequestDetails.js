import moment from 'moment';
import {Card, Container, Content} from 'native-base';
import React from 'react';
import {useState} from 'react';
import {ScrollView} from 'react-native';
import {StyleSheet, Text, View} from 'react-native';
import {CheckBox, Icon} from 'react-native-elements';
import {Colors} from '../../utils/configs/Colors';
import {ApprovalStages} from '../common/ApprovalStages';
import CustomLabel from '../common/CustomLabel';
import {CustomList2} from '../common/CustomList2';
import CustomModalTextArea from '../common/CustomModalTextArea';
import SubHeader from '../common/SubHeader';

const RequestDetails = ({navigation, route}) => {
  const {item, path} = route?.params;
  console.log('item', item);
  const {requestDetails} = item;
  const {leaveCategory} = item;
  console.log('leaveCategory', leaveCategory);
  const {approvalDetails} = item;
  const lastVersion =
    (requestDetails?.length > 0 &&
      requestDetails.find((e) => e.version === requestDetails.length)) ||
    {};
  const firstVersion =
    (requestDetails?.length > 0 &&
      requestDetails.find((e) => e.version === 1 || e.version === 0)) ||
    {};
  const originalDetails = firstVersion?.leaveDetails || [];
  const details = lastVersion?.leaveDetails || [];
  let approvedDetails = [];
  if (path == 'approved') {
    approvedDetails = approvalDetails.map((item2, index2) => {
      return {
        approverLevel: item2.level,
        designation: item2.approverDesignation || '',
        approverName: item2.approverName,
        imagePath: item2.image,
        reason: item2.comments,
        status: item2.status,
      };
    });
  } else {
    approvedDetails = approvalDetails.map((item2, index2) => {
      return {
        approverLevel: item2.approverLevel,
        designation: item2.approverDesignation,
        approverName: item2.approverStaffName,
        imagePath: item2.approverImage,
        reason: item2.approverComment,
        status: item2.leaveApprovalStatus,
      };
    });
  }
  //console.log('approvedDetails', approvedDetails);
  return (
    <View style={styles.container}>
      <Container>
        <SubHeader
          title={
            (item.status === 1 ? 'Approved' : 'Rejected') + ' Request Details'
          }
          showBack={true}
          backScreen="Leaves"
          showBell={false}
          navigation={navigation}
        />

        <Content>
          {leaveCategory === 8 && (
            <View style={{padding: 10}}>
              <CustomLabel title="Complaint" />
              <CustomModalTextArea value={item.reason} disabled />
              <CustomLabel title="Summary" />
              <CustomModalTextArea value={item.complaintSummary} disabled />
            </View>
          )}
          {leaveCategory === 7 && (
            <View style={{padding: 10}}>
              <CustomLabel title="Missed Punch Details" />
              {originalDetails?.length > 0 &&
                originalDetails.map((e) => {
                  return (
                    <>
                      <Card style={{padding: 10}}>
                        <View
                          style={{
                            width: '100%',
                            flexDirection: 'row',
                            justifyContent: 'space-between',
                            alignItems: 'center',
                          }}>
                          <CustomLabel
                            containerStyle={{width: '60%'}}
                            title={'Missed Punch Date'}
                            labelStyle={{color: Colors.header}}
                          />
                          <CustomLabel
                            title={moment(e.leaveAppliedDate).format(
                              'DD.MM.YYYY',
                            )}
                          />
                        </View>
                        <View
                          style={{
                            width: '100%',
                            flexDirection: 'row',
                            justifyContent: 'space-between',
                            alignItems: 'center',
                          }}>
                          <CustomLabel
                            containerStyle={{width: '60%'}}
                            title={'Scheduled Punch Time'}
                            labelStyle={{color: Colors.header}}
                          />
                          <CustomLabel
                            title={moment(e.scheduledMissedPunchTime).format(
                              'h:mm a',
                            )}
                          />
                        </View>
                        <View
                          style={{
                            width: '100%',
                            flexDirection: 'row',
                            justifyContent: 'space-between',
                            alignItems: 'center',
                          }}>
                          <CustomLabel
                            containerStyle={{width: '60%'}}
                            title={'Actual Punch Time'}
                            labelStyle={{color: Colors.header}}
                          />
                          <CustomLabel
                            title={moment(e.actualMissedPunchTime).format(
                              'h:mm a',
                            )}
                          />
                        </View>
                        <View
                          style={{
                            width: '100%',
                            flexDirection: 'row',
                            justifyContent: 'space-between',
                            alignItems: 'center',
                          }}>
                          <CustomLabel
                            containerStyle={{width: '60%'}}
                            title={'Type'}
                            labelStyle={{color: Colors.header}}
                          />
                          <CustomLabel
                            title={
                              e.missedPunchIsCheckIn ? 'Check In' : 'Check Out'
                            }
                          />
                        </View>
                      </Card>
                    </>
                  );
                })}
            </View>
          )}
          {leaveCategory !== 8 && leaveCategory !== 7 && (
            <View>
              <CustomLabel
                title={'Applied Dates' + '(' + item.leaveTypeName + ')'}
                containerStyle={{padding: 10, margin: 10}}
              />
              <CustomList2
                // maxHeight="100%"
                width="95%"
                title1="Date"
                title2={
                  leaveCategory === 3 || leaveCategory === 4
                    ? 'Session 1'
                    : 'From'
                }
                title3={
                  leaveCategory === 3 || leaveCategory === 4
                    ? 'Session 2'
                    : 'To'
                }
                color={'#FFEDEE'}
                headerColor="#F15761">
                <ScrollView nestedScrollEnabled>
                  {originalDetails?.length > 0 &&
                    originalDetails.map((e, i) => {
                      return (
                        <View
                          key={i}
                          style={[
                            styles.headerContainer,
                            {
                              marginBottom: 10,
                            },
                          ]}>
                          <View style={[styles.headerTitleContainer, {}]}>
                            <Text style={[styles.text, {color: 'black'}]}>
                              {/*{moment(item.DatesSelected).format('DD.MM.YYYY')}*/}
                              {moment(e.leaveAppliedDate).format('DD.MM.YYYY')}
                            </Text>
                          </View>
                          <CheckBox
                            title={
                              leaveCategory === 5
                                ? moment
                                    .utc(e.fromTime)
                                    .local()
                                    .format('h:mm a')
                                : 'Morning'
                            }
                            containerStyle={{backgroundColor: 'transparent'}}
                            checked={leaveCategory === 5 ? true : e.firstHalf}
                            checkedIcon={
                              <Icon
                                name={
                                  leaveCategory === 5
                                    ? 'clockcircleo'
                                    : 'check-square-o'
                                }
                                type={
                                  leaveCategory === 5
                                    ? 'antdesign'
                                    : 'font-awesome'
                                }
                                color={Colors.header}
                              />
                            }
                            uncheckedIcon={
                              <Icon
                                name="squared-cross"
                                color={Colors.header}
                                type={'entypo'}
                              />
                            }
                            checkedColor={Colors.header}
                            disabled
                          />

                          <CheckBox
                            title={
                              leaveCategory === 5
                                ? moment.utc(e.toTime).local().format('h:mm a')
                                : 'Evening'
                            }
                            containerStyle={{backgroundColor: 'transparent'}}
                            checked={leaveCategory === 5 ? true : e.secondHalf}
                            checkedColor={Colors.header}
                            checkedIcon={
                              <Icon
                                name={
                                  leaveCategory === 5
                                    ? 'clockcircleo'
                                    : 'check-square-o'
                                }
                                type={
                                  leaveCategory === 5
                                    ? 'antdesign'
                                    : 'font-awesome'
                                }
                                color={Colors.header}
                              />
                            }
                            disabled
                            uncheckedIcon={
                              <Icon
                                name="squared-cross"
                                color={Colors.header}
                                type={'entypo'}
                              />
                            }
                          />
                        </View>
                      );
                    })}
                </ScrollView>
              </CustomList2>
              <CustomLabel
                title="Approved Dates"
                containerStyle={{padding: 10, margin: 10}}
              />
              <CustomList2
                // maxHeight="100%"
                width="95%"
                title1="Date"
                title2={
                  leaveCategory === 3 || leaveCategory === 4
                    ? 'Session 1'
                    : 'From'
                }
                title3={
                  leaveCategory === 3 || leaveCategory === 4
                    ? 'Session 2'
                    : 'To'
                }
                color={'#FFEDEE'}
                headerColor="#F15761">
                <ScrollView nestedScrollEnabled>
                  {details?.length > 0 &&
                    details.map((e, i) => {
                      return (
                        <View
                          key={i}
                          style={[
                            styles.headerContainer,
                            {
                              marginBottom: 10,
                            },
                          ]}>
                          <View style={[styles.headerTitleContainer, {}]}>
                            <Text style={[styles.text, {color: 'black'}]}>
                              {/*{moment(item.DatesSelected).format('DD.MM.YYYY')}*/}
                              {moment(e.leaveAppliedDate).format('DD.MM.YYYY')}
                            </Text>
                          </View>
                          <CheckBox
                            title={
                              leaveCategory === 5
                                ? moment
                                    .utc(e.fromTime)
                                    .local()
                                    .format('h:mm a')
                                : 'Morning'
                            }
                            containerStyle={{backgroundColor: 'transparent'}}
                            checked={leaveCategory === 5 ? true : e.firstHalf}
                            checkedColor={Colors.header}
                            checkedIcon={
                              <Icon
                                name={
                                  leaveCategory === 5
                                    ? 'clockcircleo'
                                    : 'check-square-o'
                                }
                                type={
                                  leaveCategory === 5
                                    ? 'antdesign'
                                    : 'font-awesome'
                                }
                                color={Colors.header}
                              />
                            }
                            uncheckedIcon={
                              <Icon
                                name="squared-cross"
                                color={Colors.header}
                                type={'entypo'}
                              />
                            }
                            disabled
                          />

                          <CheckBox
                            title={
                              leaveCategory === 5
                                ? moment.utc(e.toTime).local().format('h:mm a')
                                : 'Evening'
                            }
                            containerStyle={{backgroundColor: 'transparent'}}
                            checked={leaveCategory === 5 ? true : e.secondHalf}
                            checkedColor={Colors.header}
                            checkedIcon={
                              <Icon
                                name={
                                  leaveCategory === 5
                                    ? 'clockcircleo'
                                    : 'check-square-o'
                                }
                                type={
                                  leaveCategory === 5
                                    ? 'antdesign'
                                    : 'font-awesome'
                                }
                                color={Colors.header}
                              />
                            }
                            uncheckedIcon={
                              <Icon
                                name="squared-cross"
                                color={Colors.header}
                                type={'entypo'}
                              />
                            }
                            disabled
                          />
                        </View>
                      );
                    })}
                </ScrollView>
              </CustomList2>
              <View style={{margin: 10}}></View>
            </View>
          )}
          {approvalDetails?.length > 0 ? (
            <ApprovalStages
              width="95%"
              key={1}
              title="Approval Stages"
              color={'#FFEDEE'}
              headerColor={'#F15761'}
              Arr={approvedDetails}
            />
          ) : (
            <View>
              <Text style={{alignSelf: 'center', marginTop: 10}}>
                No Data Found
              </Text>
            </View>
          )}
        </Content>
      </Container>
    </View>
  );
};

export default RequestDetails;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fbfbfb',
  },
  headerContainer: {
    width: '100%',
    height: 40,
    flexDirection: 'row',
    borderRadius: 15,
    justifyContent: 'space-evenly',
    alignItems: 'center',
    paddingHorizontal: 10,
    marginTop: 5,
  },
  headerTitleContainer: {
    width: '24%',
    height: '100%',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 10,
  },
  text: {
    fontFamily: 'Poppins-Regular',
    fontSize: 15,
  },
});
