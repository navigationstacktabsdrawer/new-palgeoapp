import {Container, Content, Item, Picker, Thumbnail} from 'native-base';
import React, {Component} from 'react';
import {
  View,
  Text,
  SafeAreaView,
  StyleSheet,
  Dimensions,
  TouchableWithoutFeedback,
  TouchableOpacity,
  Alert,
  ActivityIndicator,
} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import SubHeader from '../common/SubHeader';
//import CustomPicker from '../common/CustomPicker';
//import {CustomList} from '../common/CustomList';
//import {CustomButton} from '../common/CustomButton';
import DatePicker from 'react-native-datepicker';
import moment from 'moment';
//import {CustomList2} from '../common/CustomList2';
import {CustomTextArea} from '../common/CustomTextArea';
import {ApprovalStages} from '../common/ApprovalStages';
import {CalenderView} from '../common/CalenderView';
import AsyncStorage from '@react-native-community/async-storage';
import axios from 'axios';
import Const from '../common/Constants';
import DateTimePickerModal from 'react-native-modal-datetime-picker';
import CustomPicker from '../common/CustomPicker';
import CustomLoader from '../Task Allocation/CustomLoader';
import CustomModal from '../common/CustomModal';
import CustomLabel from '../common/CustomLabel';
import {CheckBox} from 'react-native-elements';
import {Colors} from '../../utils/configs/Colors';
const SubThemeColor = Colors.secondary;
const ThemeColor = Colors.header;
const screenWidth = Dimensions.get('window').width;
export default class Permission extends Component {
  constructor(props) {
    super(props);
    this.state = {
      SelectedLeaveIdArr: [],
      LeaveType: '',
      date: moment().format('YYYY-MM-DD'),
      time: new Date(),
      time2: new Date(),
      ApprovalStages: [],
      showFirstTime: false,
      showSecondTime: false,
      Attachment: '',
      Loader: true,
      availedMonthCount: '00',
      availedYearCount: '00',
      //Attachment: {
      //  FileName: '',
      //  FileType: '',
      //  Attachment: '',
      //},
      SelectedLeaveId: this.props.route.params
        ? this.props.route.params.SelectedLeaveId
          ? this.props.route.params.SelectedLeaveId
          : ''
        : '',
    };
  }
  componentDidMount() {
    this.retrieveData();
  }
  retrieveData = async () => {
    try {
      const value = await AsyncStorage.getItem('bearer_token');
      if (value !== null) {
        //alert(value);
        this.setState({Token: value}, function () {});
      }
    } catch (error) {
      alert('Error retrieving data');
    }
    try {
      const value = await AsyncStorage.getItem('institute_id');
      if (value !== null) {
        this.setState({institute_id: value}, function () {});
      }
    } catch (error) {
      alert('Error retrieving data');
    }
    try {
      const value = await AsyncStorage.getItem('user_id');
      if (value !== null) {
        //alert(value);
        this.setState({StaffCode: value}, function () {
          this.setState({LeaveType: ''}, () => {
            this.GetLeaveTypeSubCategoriesByInstituteId();
          });
          this.GetMasterLeaveApproverDetails();
          this.getAvailedPermissions();
        });
      }
    } catch (error) {
      alert('Error retrieving data');
    }
  };

  getAvailedPermissions = async () => {
    const url = Const + 'api/Leave/PermissionsRequestedInfo';
    const body = {
      staffCode: this.state.StaffCode,
      InstituteId: this.state.institute_id,
      Date: new Date().toISOString(),
    };
    try {
      const response = await axios.post(url, body);
      const {data} = response;
      console.log('data', data);
      this.setState({
        availedMonthCount:
          data.currentMonth < 10 ? '0' + data.currentMonth : data.currentMonth,
        availedYearCount:
          data.currentYear < 10 ? '0' + data.currentYear : data.currentYear,
      });
    } catch (e) {
      alert('Error calculating availed permissions: ' + e.message);
    }
  };
  GetLeaveTypeSubCategoriesByInstituteId = async () => {
    const url =
      Const +
      'api/Leave/GetLeaveTypeSubCategoriesByInstituteId?instituteId=' +
      this.state.institute_id +
      '&leaveRequestId=' +
      this.state.SelectedLeaveId +
      '&staffCode=' +
      this.state.StaffCode;
    console.log('GetLeaveTypeSubCategoriesByInstituteId Url = ', url);
    try {
      const response = await axios.get(url);
      console.log('GetLeaveTypeSubCategoriesByInstituteId =', response.data);
      let SelectedLeaveIdArr = this.state.SelectedLeaveIdArr;
      response.data.map((item) => {
        SelectedLeaveIdArr.push({
          createdDate: item.createdDate,
          id: item.leaveId,
          instituteId: item.instituteId,
          isActive: item.isActive,
          name: item.leaveName,
          leaveRequestTypeId: item.leaveRequestTypeId,
          modifiedDate: item.modifiedDate,
          shortForm: item.shortForm,
        });
      });
      this.setState({Loader: false, SelectedLeaveIdArr}, function () {
        response.data.length == 1 &&
          this.setState({LeaveType: this.state.SelectedLeaveIdArr[0].id});
        response.data.length == 0 &&
          Alert.alert(
            'Alert',
            'You have not assigned any Permission. You can Select different Permission types.',
            [
              {
                text: 'Stay Here',
                onPress: () => console.log('stay here pressed'),
              },
              {
                text: 'Go Back',
                onPress: () => this.props.navigation.goBack(),
              },
            ],
            {
              cancelable: false,
            },
          );
      });
    } catch (error) {
      alert('Error: ' + error.message);
    }
  };
  GetMasterLeaveApproverDetails = async (id) => {
    //const url = `https://insproplus.com/palgeoapi/api/Leave/GetMasterLeaveApproverDetails/${this.state.institute_id}/${this.state.StaffCode}/${this.state.SelectedLeaveId}`;
    const url = `https://insproplus.com/palgeoapi/api/Leave/GetMasterLeaveApproverDetailsByMainType/${this.state.institute_id}/${this.state.StaffCode}/${this.state.SelectedLeaveId}`;

    console.log(url);
    try {
      const response = await axios.get(url);
      console.log('aproval stages', response.data);
      this.setState(
        {
          Loader: false,
          ApprovalStages: response.data,
          ApproverLoading: false,
        },
        function () {
          this.state.ApprovalStages.length == 0 &&
            Alert.alert(
              'Alert',
              'No approver found.',
              [
                //{
                //  text: 'Stay Here',
                //  onPress: () => console.log('cancel pressed'),
                //  style: 'cancel',
                //},
                {
                  text: 'Go Back',
                  onPress: () => this.props.navigation.goBack(),
                },
              ],
              {
                cancelable: false,
              },
            );
        },
      );
    } catch (error) {
      console.log('error', error);
      this.setState({ApproverLoading: false});
      alert(error.message);
    }
  };
  DateCheckerFunction() {
    if (new Date(this.state.time).getTime() > this.state.time2.getTime()) {
      alert('Start time cannot be more than end time!');
      this.setState({time2: this.state.time});
      return false;
    }
    return true;
  }
  AddUpdateLeaveRequestNew = async (NewLeavesArr) => {
    const result = this.DateCheckerFunction();
    if (!result) return;
    const device_token = await AsyncStorage.getItem('device_token');
    const url = Const + 'api/Leave/AddUpdateLeaveRequestNew';
    console.log(url);
    const fromDate = new Date(this.state.time).getTime();
    const endDate = new Date(this.state.time2).getTime();
    const diffSeconds = endDate - fromDate;
    const diffeHours = diffSeconds / 3600000;
    const difference = parseInt(diffeHours / 24) + 1;
    console.log('dd', difference);

    let bodyData = {
      Id: 0,
      MobileDeviceToken: device_token,
      InstituteId: parseInt(this.state.institute_id),
      StaffCode: this.state.StaffCode.toString(),
      LeaveTypeId: parseInt(this.state.LeaveType),
      LeaveDates: [
        {
          Id: 0,
          DatesSelected: new Date().toISOString(),
          FirstHalf: true,
          SecondHalf: true,
        },
      ],
      FromDate: this.state.time,
      EndDate: this.state.time2,
      FromTime: this.state.time,
      ToTime: this.state.time2,
      Reason: this.state.Reason,
      LeaveAttachment: this.state.Attachment,
      NoOfDays: 1,
      Status: 0,
      CreatedDate: new Date().toISOString(),
      ModifiedDate: new Date().toISOString(),
    };

    console.log(JSON.stringify(bodyData));
    try {
      const response = await axios.post(url, bodyData);
      console.log('working');
      console.log(JSON.stringify(response.data));
      if (response.data.status == true) {
        this.setState(
          {
            loader: false,
          },
          function () {
            this.props.navigation.navigate('Leaves');
            alert(response.data.message);
          },
        );
      } else {
        this.setState({loader: false});
        const dates = response.data.overlappingDatesInfo;
        if (dates?.length > 0) {
          this.setState({overlappingDatesInfo: dates}, () => {
            this.setState({showModal: true});
          });
        } else {
          alert(response.data.message);
        }
      }
    } catch (error) {
      this.setState({loader: false});
      alert(error.message);
    }
  };

  handleConfirm1 = (time) => {
    console.log('time', time);
    if (moment(time) < moment()) {
      return alert('Time cannot be less than current time');
    }
    this.setState({time, time2: time}, () => this.hideDatePicker1());
  };
  handleConfirm2 = (time) => {
    this.setState({time2: time}, () => this.hideDatePicker2());
  };

  hideDatePicker1 = () => {
    if (this.state.time && this.state.time2) {
      //this.DateCheckerFunction()
    }
    this.setState({showFirstTime: false});
  };
  hideDatePicker2 = () => {
    if (this.state.time && this.state.time2) {
      this.DateCheckerFunction();
    }
    this.setState({showSecondTime: false});
  };

  render() {
    if (this.state.Loader) {
      return <Loader />;
    }
    return (
      <View style={styles.container}>
        <Container>
          <SubHeader
            title="Permission"
            showBack={true}
            backScreen="Leaves"
            showBell={false}
            navigation={this.props.navigation}
          />
          {this.state.loader && (
            <CustomLoader loaderText={'Trying to raise your request'} />
          )}
          <Content>
            {this.state.showModal && (
              <CustomModal
                title={'Overlapping Request Dates'}
                isVisible={this.state.showModal}
                deleteIconPress={() => this.setState({showModal: false})}>
                {this.state.overlappingDatesInfo?.length &&
                  this.state.overlappingDatesInfo.map((item, i) => {
                    return (
                      <View
                        key={i}
                        style={{
                          flexDirection: 'row',
                          justifyContent: 'space-evenly',
                          width: '100%',
                          alignItems: 'center',
                          borderBottomColor: Colors.overlay,
                          borderBottomWidth: 1,
                          marginBottom: 5,
                        }}>
                        <View style={{width: '40%'}}>
                          <CustomLabel
                            //containerStyle={{width: '40%'}}
                            title={moment(item.date).format('DD.MM.YYYY')}
                          />
                          <CustomLabel title={item.leaveRequestType} />
                        </View>
                        <View style={{width: '40%'}}>
                          <CheckBox
                            containerStyle={{
                              padding: 0,
                              backgroundColor: 'transparent',
                              elevation: 0,
                            }}
                            title={'First Half'}
                            checked={item.isFirstHalfOverLapping}
                            disabled
                            checkedColor={Colors.header}
                          />
                          <CheckBox
                            containerStyle={{
                              padding: 0,
                              backgroundColor: 'transparent',
                              elevation: 0,
                            }}
                            title={'Second Half'}
                            checked={item.isSecondHalfOverLapping}
                            disabled
                            checkedColor={Colors.header}
                          />
                        </View>
                      </View>
                    );
                  })}
              </CustomModal>
            )}
            <View
              style={{
                justifyContent: 'center',
                alignItems: 'center',
                width: '95%',
                flexDirection: 'row',
                marginVertical: 10,
              }}>
              <CalenderView
                mainTitle="Total availed"
                title="CURRENT MONTH"
                date={this.state.availedMonthCount.toString() || '00'}
                backgroundColor="#FD8991"
                width={120}
              />
              <CalenderView
                mainTitle="Total availed"
                title="CURRENT YEAR"
                date={this.state.availedYearCount.toString() || '00'}
                backgroundColor="#FD8991"
                width={120}
              />
            </View>
            {this.state.SelectedLeaveIdArr.length > 1 && (
              <CustomPicker
                label="You can choose type here"
                selectedValue={this.state.LeaveType}
                options={this.state.SelectedLeaveIdArr}
                onValueChange={(value) => {
                  this.setState({LeaveType: value}, () => {
                    this.setState({ApproverLoading: true});
                  });
                }}
              />
            )}
            <View
              style={{
                width: '95%',
                justifyContent: 'space-evenly',
                alignItems: 'center',
                flexDirection: 'row',
                alignSelf: 'center',
                marginVertical: 15,
                //marginTop: "4%",
              }}>
              <TouchableOpacity
                onPress={() => this.setState({showFirstTime: true})}>
                <View style={{width: '100%'}}>
                  <View style={styles.labelContainer}>
                    <Text style={styles.label}>From Time</Text>
                    <View
                      style={{
                        width: screenWidth / 2.3,
                        backgroundColor: SubThemeColor,
                        height: 40,
                        borderRadius: 5,
                        justifyContent: 'center',
                        //alignItems: 'center',
                        paddingLeft: 10,
                      }}>
                      <Text style={[styles.label, {color: ThemeColor}]}>
                        {moment(this.state.time).format('h:mm a')}
                      </Text>
                    </View>
                  </View>
                  <DateTimePickerModal
                    isVisible={this.state.showFirstTime}
                    mode="time"
                    minimumDate={moment()}
                    onConfirm={this.handleConfirm1}
                    onCancel={this.hideDatePicker1}
                  />
                </View>
              </TouchableOpacity>
              <TouchableOpacity
                onPress={() => this.setState({showSecondTime: true})}>
                <View style={{width: '100%'}}>
                  <View style={styles.labelContainer}>
                    <Text style={styles.label}>To Time</Text>
                    <View
                      style={{
                        width: screenWidth / 2.3,
                        backgroundColor: SubThemeColor,
                        height: 40,
                        borderRadius: 5,
                        justifyContent: 'center',
                        paddingLeft: 10,
                        //alignItems: 'center',
                      }}>
                      <Text style={[styles.label, {color: ThemeColor}]}>
                        {moment(this.state.time2).format('h:mm a')}
                      </Text>
                    </View>
                  </View>
                  <DateTimePickerModal
                    isVisible={this.state.showSecondTime}
                    mode="time"
                    onConfirm={this.handleConfirm2}
                    onCancel={this.hideDatePicker2}
                  />
                </View>
              </TouchableOpacity>
            </View>

            <View style={{marginTop: 20}}>
              <CustomTextArea
                onPress2={() => {
                  this.setState({
                    Attachment: '',
                  });
                }}
                SelectedImage={(Attachment) => {
                  this.setState({Attachment});
                }}
                title="Reason"
                text={this.state.Attachment.FileName || 'No files attached'}
                backgroundColor={SubThemeColor}
                placeholderTextColor={ThemeColor}
                width="95%"
                placeholder="Write reason here"
                value={this.state.Reason}
                onChangeText={(text) => {
                  this.setState({Reason: text});
                }}
                onPress={() => {
                  if (
                    this.state.LeaveType == '' ||
                    this.state.LeaveType == null
                  ) {
                    alert(
                      'You have not assigned any permission for this type ',
                    );
                  } else if (this.state.Reason == '') {
                    alert('Please add reason');
                  } else {
                    this.setState({loader: true});
                    this.AddUpdateLeaveRequestNew();
                  }
                }}
              />
            </View>
            <View style={{marginVertical: 20}}>
              {this.state.ApprovalStages.length > 0 ||
              !this.state.ApproverLoading ? (
                <ApprovalStages
                  onPress={() => {}}
                  width="95%"
                  key={0}
                  title="Approval Stages"
                  color={SubThemeColor}
                  headerColor={ThemeColor}
                  Arr={this.state.ApprovalStages}
                />
              ) : (
                <View>
                  <ActivityIndicator />
                  <Text style={{alignSelf: 'center', marginTop: 10}}>
                    Fetching Approver List...
                  </Text>
                </View>
              )}
            </View>
          </Content>
        </Container>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fbfbfb',
  },
  text: {
    fontFamily: 'Poppins-Regular',
    fontSize: 15,
  },
  item: {
    borderRadius: 15,
    margin: 10,
    alignSelf: 'center',
    backgroundColor: '#ffffff',
    borderColor: '#f1f1f1',
    borderWidth: 0.5,
    shadowColor: 'silver',
    shadowOffset: {width: 0, height: 0},
    shadowOpacity: 0.3,
    height: hp('5'),
  },
  label: {
    fontFamily: 'Poppins-SemiBold',
    fontSize: 15,
    color: '#000000',
  },
  labelContainer: {
    margin: '1.5%',
  },
  dateContainer: {
    backgroundColor: ThemeColor,
  },
});
