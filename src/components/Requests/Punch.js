import {Container, Content, Item, Picker, Thumbnail} from 'native-base';
import React, {Component} from 'react';
import {
  View,
  Text,
  SafeAreaView,
  StyleSheet,
  Dimensions,
  TouchableWithoutFeedback,
  TouchableOpacity,
  TextInput,
  ScrollView,
  Alert,
} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import SubHeader from '../common/SubHeader';
import CustomPicker from '../common/CustomPicker';
import {CustomList} from '../common/CustomList';
import {CustomButton} from '../common/CustomButton';
import DatePicker from 'react-native-datepicker';
import moment from 'moment';
import {CustomList2} from '../common/CustomList2';
import {CustomTextArea} from '../common/CustomTextArea';
import {ApprovalStages} from '../common/ApprovalStages';
import {CalenderView} from '../common/CalenderView';
import axios from 'axios';
import Const from '../common/Constants';
import AsyncStorage from '@react-native-community/async-storage';
import Nodata from '../common/NoData';
import {ActivityIndicator} from 'react-native';
import CustomLoader from '../Task Allocation/CustomLoader';
import {Colors} from '../../utils/configs/Colors';
const SubThemeColor = Colors.secondary;
const ThemeColor = Colors.header;
export default class Punch extends Component {
  constructor(props) {
    super(props);
    this.state = {
      LeaveType: '',
      Loader: true,
      Reason: '',
      ActivityLoader: true,
      NewArr: [],
      data: [],
      SelectedLeaveIdArr: [],
      ApprovalStages: [],
      ApproverLoading: true,
      tempIndex2: -1,
      tempIndex2: -1,
      //Attachment: {
      //  FileName: '',
      //  FileType: '',
      //  Attachment: '',
      //},
      Attachment: '',
      SelectedLeaveId: this.props.route.params
        ? this.props.route.params.SelectedLeaveId
          ? this.props.route.params.SelectedLeaveId
          : ''
        : '',
      FromDate: new Date().toISOString(),
      ToDate: new Date().toISOString(),
      AddUpdatePunch: [],
    };
  }
  componentDidMount() {
    this.retrieveData();
  }
  retrieveData = async () => {
    try {
      const value = await AsyncStorage.getItem('bearer_token');
      if (value !== null) {
        //alert(value);
        this.setState({Token: value}, function () {});
      }
    } catch (error) {
      alert('Error retrieving data');
    }
    try {
      const value = await AsyncStorage.getItem('institute_id');
      if (value !== null) {
        this.setState({institute_id: value}, function () {});
      }
    } catch (error) {
      alert('Error retrieving data');
    }
    try {
      const value = await AsyncStorage.getItem('user_id');
      if (value !== null) {
        //alert(value);
        this.setState({StaffCode: value}, function () {
          this.setState({LeaveType: ''}, () => {
            this.GetLeaveTypeSubCategoriesByInstituteId();
          });
          this.AddUpdatePunch();
          this.GetMasterLeaveApproverDetails();
        });
      }
    } catch (error) {
      alert('Error retrieving data');
    }
  };
  GetLeaveTypeSubCategoriesByInstituteId = async (id) => {
    const url =
      Const +
      'api/Leave/GetLeaveTypeSubCategoriesByInstituteId?instituteId=' +
      this.state.institute_id +
      '&leaveRequestId=' +
      this.state.SelectedLeaveId +
      '&staffCode=' +
      this.state.StaffCode;
    console.log('GetLeaveTypeSubCategoriesByInstituteId Url = ', url);
    try {
      const response = await axios.get(url);
      console.log('GetLeaveTypeSubCategoriesByInstituteId =', response.data);
      this.setState(
        {
          Loader: false,
          SelectedLeaveIdArr: response.data,
          CustomListLoader: false,
        },
        function () {
          response.data.length == 1 &&
            this.setState({
              LeaveType: this.state.SelectedLeaveIdArr[0].leaveId,
            });
          response.data.length == 0 &&
            Alert.alert(
              'Alert',
              'You have not assigned any Missed Punch. You can Select different Missed Punch types.',
              [
                {
                  text: 'Stay Here',
                  onPress: () => console.log('stay here pressed'),
                },
                {
                  text: 'Go Back',
                  onPress: () => this.props.navigation.goBack(),
                },
              ],
              {
                cancelable: false,
              },
            );
        },
      );
    } catch (error) {
      this.setState({CustomListLoader: false});
      alert(error.message);
    }
  };
  GetMasterLeaveApproverDetails = async () => {
    //const url = `https://insproplus.com/palgeoapi/api/Leave/GetMasterLeaveApproverDetails/${this.state.institute_id}/${this.state.StaffCode}/${this.state.SelectedLeaveId}`;
    const url = `https://insproplus.com/palgeoapi/api/Leave/GetMasterLeaveApproverDetailsByMainType/${this.state.institute_id}/${this.state.StaffCode}/${this.state.SelectedLeaveId}`;
    console.log(url);
    try {
      const response = await axios.get(url);
      console.log(
        'missed punch GetMasterLeaveApproverDetails = ',
        response.data,
      );
      this.setState(
        {
          Loader: false,
          ApprovalStages: response.data,
          ApproverLoading: false,
        },
        function () {
          this.state.ApprovalStages.length == 0 &&
            Alert.alert(
              'Alert',
              'No approver found.',
              [
                //{
                //  text: 'Stay Here',
                //  onPress: () => console.log('cancel pressed'),
                //  style: 'cancel',
                //},
                {
                  text: 'Go Back',
                  onPress: () => this.props.navigation.goBack(),
                },
              ],
              {
                cancelable: false,
              },
            );
        },
      );
    } catch (error) {
      this.setState({ApproverLoading: false});
      alert(error.message);
    }
  };
  AddUpdatePunch = async () => {
    //let date = new Date();
    const url =
      Const +
      'api/Leave/IdentifyMissedPunchTimestamps/' +
      this.state.institute_id +
      '/' +
      this.state.StaffCode;
    //let bodyData = {
    //  InstituteId: parseInt(this.state.institute_id),
    //  StaffCode: this.state.StaffCode,
    //  //FromDate: new Date(date.getFullYear(), date.getMonth(), 2).toISOString(),
    //  //ToDate: this.state.ToDate,

    //  //InstituteId: 2,
    //  //StaffCode: 'PW100',
    //  //FromDate: '2018-07-07T11:44:09.417Z',
    //  //ToDate: '2021-07-07T11:44:09.417Z',
    //};
    //console.log(url, bodyData);
    try {
      const response = await axios.get(url);
      console.log('missed_puches url =>>', url);
      console.log('missed_puches =>>', response.data);
      this.setState(
        {
          Loader: false,
          ActivityLoader: false,
          AddUpdatePunch: response.data,
        },
        () => {
          const NewArr = this.state.AddUpdatePunch.map((item2, index2) => {
            return {
              AccessDate: item2.accessDate,
              actualCheckinTime: item2.actualCheckinTime,
              actualCheckoutTime: item2.actualCheckoutTime,
              checkinDelayTime: item2.checkinDelayTime,
              CheckinRowId: item2.checkinRowId,
              checkoutEarlyTime: item2.checkoutEarlyTime,
              CheckoutRowId: item2.checkoutRowId,
              scheduledCheckinTime: item2.scheduledCheckinTime,
              scheduledCheckoutTime: item2.scheduledCheckoutTime,
              ApproveInTime: false,
              ApproveOutTime: false,
              isCheckIn: item2.isCheckIn,
              rowId: item2.rowId,
              Reason: this.state.Reason,
            };
          });
          this.setState({NewArr: NewArr || []}, () => {
            if (this.state.NewArr.length === 0) {
              alert('You do not have any missed punches.');
              return this.props.navigation.goBack();
            }
          });
        },
      );
    } catch (error) {
      this.setState({ActivityLoader: false});
      alert(error.message);
    }
  };

  AddLeave = async (data) => {
    const url = Const + 'api/Leave/RequestToModifyMissedPunchTimings';
    const days = data.map((e) => {
      return {accessDate: e.AccessDate};
    });
    const uniques = Object.values(
      days.reduce((a, c) => {
        a[c.accessDate] = c;
        return a;
      }, {}),
    );

    let bodyData = {
      Id: 0,
      InstituteId: parseInt(this.state.institute_id),
      RequestTypeId: parseInt(this.state.LeaveType),
      StaffCode: this.state.StaffCode,
      NoOfDays: uniques.length,
      PunchDetails: data,
      MissedPunchAttachment: this.state.Attachment,
      //Reason: this.state.Reason,
      //PunchDetails: [
      //  {
      //    Reason: this.state.Reason,
      //    AccessDate: this.state.SelectedData.accessDate,
      //    CheckinRowId: parseInt(this.state.SelectedData.checkinRowId),
      //    CheckoutRowId: parseInt(this.state.SelectedData.checkoutRowId),
      //    ApproveInTime: this.state.ApproveInTime,
      //    ApproveOutTime: this.state.ApproveOutTime,
      //  },
      //],
    };
    console.log(url);
    console.log(bodyData);

    try {
      const response = await axios.post(url, bodyData);
      console.log(JSON.stringify(response.data));
      if (response.data.status == true) {
        this.setState(
          {
            loader: false,
          },
          function () {
            this.props.navigation.navigate('Leaves');
            alert(response.data.message);
          },
        );
      } else {
        this.setState({loader: false});
        alert(response.data.message);
      }
    } catch (error) {
      this.setState({loader: false});
      alert(error.message);
    }
  };
  convertFrom24To12Format = (time24) => {
    const [sHours, minutes] = time24.match(/([0-9]{1,2}):([0-9]{2})/).slice(1);
    const period = +sHours < 12 ? 'AM' : 'PM';
    const hours = +sHours % 12 || 12;

    return `${hours}:${minutes} ${period}`;
  };
  render() {
    if (this.state.Loader) {
      return <Loader />;
    }
    return (
      <View style={styles.container}>
        <Container>
          <SubHeader
            title="Punch"
            showBack={true}
            backScreen="Leaves"
            showBell={false}
            navigation={this.props.navigation}
          />
          {this.state.loader && (
            <CustomLoader loaderText={'Trying to raise your request'} />
          )}
          <Content nestedScrollEnabled={true}>
            <View
              style={{
                width: '95%',
                alignSelf: 'center',
                marginTop: 20,
                backgroundColor: SubThemeColor,
                borderRadius: 10,
                minHeight: 200,
                maxHeight: 310,
              }}>
              <View
                style={[
                  styles.headerContainer,
                  {backgroundColor: ThemeColor, height: 60},
                ]}>
                <View
                  style={[
                    styles.headerTitleContainer,
                    {alignItems: 'flex-start'},
                  ]}>
                  <Text style={styles.text}>Missed</Text>
                  <Text style={[styles.text, {textAlign: 'left'}]}>Punch</Text>
                </View>
                <View style={[styles.headerTitleContainer, {}]}>
                  <Text style={styles.text}>
                    {/* {this.state.AddUpdatePunch.length
                      ? this.state.AddUpdatePunch[0].scheduledCheckinTime
                      : null} */}
                    Scheduled
                  </Text>
                  <Text style={styles.text}>Time</Text>
                </View>
                <View style={[styles.headerTitleContainer, {}]}>
                  <Text style={styles.text}>Actual</Text>
                  <Text style={styles.text}>Time</Text>
                </View>
                <View
                  style={[
                    styles.headerTitleContainer,
                    {alignItems: 'center', flex: 0.5, marginLeft: 10},
                  ]}>
                  <Text style={styles.text}>Type</Text>
                  {/* <Text style={styles.text}>Check out</Text> */}
                </View>
                {/*<View
                  style={[
                    styles.headerTitleContainer,
                    {alignItems: 'center', flex: 0.5, marginLeft: 10},
                  ]}>
                  <Text style={styles.text}>
                    {this.state.AddUpdatePunch.length
                      ? this.state.AddUpdatePunch[0].scheduledCheckoutTime
                      : null}
                  </Text>
                  <Text style={styles.text}>Select</Text>
                </View>*/}
              </View>
              <ScrollView nestedScrollEnabled={true}>
                {this.state.NewArr.length ? (
                  this.state.NewArr.map((item, index) => {
                    return (
                      <TouchableOpacity
                        onPress={() => {
                          let mod = this.state.NewArr;
                          if (item.isCheckIn) {
                            mod[index].ApproveInTime =
                              !mod[index].ApproveInTime;

                            this.setState({NewArr: [...mod]});
                            return;
                          }
                          mod[index].ApproveOutTime =
                            !mod[index].ApproveOutTime;

                          this.setState({NewArr: [...mod]});
                          ///console.log('NewArr==', this.state.NewArr),

                          return;
                        }}>
                        <View
                          key={index}
                          style={[
                            styles.headerContainer,
                            {
                              marginTop: 10,
                              paddingHorizontal: 0,
                              justifyContent: 'flex-end',
                              backgroundColor:
                                (this.state.NewArr.length &&
                                  this.state.NewArr[index].ApproveInTime) ||
                                this.state.NewArr[index].ApproveOutTime
                                  ? '#FA5F55'
                                  : 'transparent',
                            },
                          ]}>
                          <View
                            style={[
                              styles.headerTitleContainer,
                              {
                                alignItems: 'flex-start',
                                width: '20%',
                                //backgroundColor: 'yellow',
                              },
                            ]}>
                            <Text
                              style={[
                                styles.text,
                                {
                                  color:
                                    (this.state.NewArr.length &&
                                      this.state.NewArr[index].ApproveInTime) ||
                                    this.state.NewArr[index].ApproveOutTime
                                      ? 'white'
                                      : 'black',
                                },
                              ]}>
                              {moment(item.AccessDate).format('DD.MM.YYYY')}
                            </Text>
                          </View>
                          <View
                            style={[
                              styles.headerTitleContainer,
                              {
                                backgroundColor: 'transparent',
                                borderRadius: 10,
                              },
                            ]}>
                            <Text
                              style={[
                                styles.text,
                                {
                                  color:
                                    (this.state.NewArr.length &&
                                      this.state.NewArr[index].ApproveInTime) ||
                                    this.state.NewArr[index].ApproveOutTime
                                      ? 'white'
                                      : 'black',
                                },
                              ]}>
                              {item.isCheckIn
                                ? item.scheduledCheckinTime.slice(0, 5)
                                : item.scheduledCheckoutTime.slice(0, 5)}
                            </Text>
                          </View>

                          <View
                            style={[
                              styles.headerTitleContainer,
                              {
                                backgroundColor: 'transparent',
                                borderRadius: 10,
                              },
                            ]}>
                            <Text
                              style={[
                                styles.text,
                                {
                                  color:
                                    (this.state.NewArr.length &&
                                      this.state.NewArr[index].ApproveInTime) ||
                                    this.state.NewArr[index].ApproveOutTime
                                      ? 'white'
                                      : 'black',
                                  //marginHorizontal: 10,
                                },
                              ]}>
                              {item.isCheckIn
                                ? item.actualCheckinTime.slice(0, 5)
                                : item.actualCheckoutTime.slice(0, 5)}
                            </Text>
                          </View>
                          <View
                            style={[
                              styles.headerTitleContainer,
                              {
                                alignItems: 'center',
                                justifyContent: 'center',
                                borderRadius: 10,
                                backgroundColor: 'transparent',
                                marginLeft: 10,
                              },
                            ]}>
                            <Text
                              style={[
                                styles.text,
                                {
                                  marginHorizontal: 10,

                                  color:
                                    (this.state.NewArr.length &&
                                      this.state.NewArr[index].ApproveInTime) ||
                                    this.state.NewArr[index].ApproveOutTime
                                      ? 'white'
                                      : 'black',
                                },
                              ]}>
                              {item.isCheckIn ? 'Check In' : 'Check Out'}
                            </Text>
                          </View>
                        </View>
                      </TouchableOpacity>
                    );
                  })
                ) : (
                  <View
                    style={{
                      width: '100%',
                      height: 150,
                      alignItems: 'center',
                      justifyContent: 'center',
                    }}>
                    {this.state.ActivityLoader ? (
                      <ActivityIndicator color={ThemeColor} />
                    ) : (
                      <Text>No Record Found</Text>
                    )}
                  </View>
                )}
              </ScrollView>
            </View>
            <View style={{marginTop: 20}}>
              <CustomTextArea
                onPress2={() => {
                  this.setState({
                    Attachment: {
                      FileName: '',
                      FileType: '',
                      Attachment: '',
                    },
                  });
                }}
                SelectedImage={(Attachment) => {
                  this.setState({Attachment});
                }}
                text={this.state.Attachment.FileName || 'No files attached'}
                title="Reason"
                backgroundColor={SubThemeColor}
                placeholderTextColor={ThemeColor}
                width="95%"
                placeholder="Write reason here"
                value={this.state.Reason}
                onChangeText={(text) => {
                  this.setState({Reason: text});
                }}
                onPress={() => {
                  let data = this.state.NewArr;
                  data = data
                    .filter(
                      (item) =>
                        (item.ApproveInTime == true &&
                          item.ApproveOutTime == true) ||
                        (item.ApproveInTime == true &&
                          item.ApproveOutTime == false) ||
                        (item.ApproveInTime == false &&
                          item.ApproveOutTime == true),
                    )
                    .map(
                      ({
                        Reason,
                        AccessDate,
                        CheckinRowId,
                        CheckoutRowId,
                        ApproveInTime,
                        ApproveOutTime,
                        isCheckIn,
                        rowId,
                      }) => ({
                        Reason: this.state.Reason,
                        AccessDate,
                        CheckinRowId,
                        CheckoutRowId,
                        ApproveInTime,
                        ApproveOutTime,
                        isCheckIn,
                        rowId,
                      }),
                    );
                  // console.log('data = ', data);
                  this.setState({data});
                  // console.log('data length = ', data.length);
                  if (
                    this.state.LeaveType == '' ||
                    this.state.LeaveType == null
                  ) {
                    alert(
                      'You have not assigned any Missed Punch for this type ',
                    );
                  } else if (this.state.Reason == '') {
                    alert('Please add reason');
                  } else if (data.length == 0) {
                    alert(
                      'Please select missed punch check in / check out time',
                    );
                  } else {
                    this.setState({loader: true});
                    this.AddLeave(data);
                  }
                }}
              />
            </View>
            <View style={{marginVertical: 20}}>
              {this.state.ApprovalStages.length > 0 ||
              !this.state.ApproverLoading ? (
                <ApprovalStages
                  onPress={() => {}}
                  width="95%"
                  key={0}
                  title="Approval Stages"
                  color={SubThemeColor}
                  headerColor={ThemeColor}
                  Arr={this.state.ApprovalStages}
                />
              ) : (
                <View>
                  <ActivityIndicator />
                  <Text style={{alignSelf: 'center', marginTop: 10}}>
                    Fetching Approver List...
                  </Text>
                </View>
              )}
            </View>
          </Content>
        </Container>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fbfbfb',
  },
  text: {
    fontFamily: 'Poppins-Regular',
    fontSize: 15,
    textAlign: 'center',
  },
  item: {
    borderRadius: 15,
    margin: 10,
    alignSelf: 'center',
    backgroundColor: '#ffffff',
    borderColor: '#f1f1f1',
    borderWidth: 0.5,
    shadowColor: 'silver',
    shadowOffset: {width: 0, height: 0},
    shadowOpacity: 0.3,
    height: hp('5'),
  },
  label: {
    fontFamily: 'Poppins-SemiBold',
    fontSize: 15,
    color: '#000000',
  },
  labelContainer: {
    margin: '1.5%',
  },
  dateContainer: {
    backgroundColor: ThemeColor,
  },
  text: {
    fontFamily: 'Poppins-Regular',
    fontSize: 15,
    color: 'white',
  },
  headerContainer: {
    width: '100%',
    height: 40,
    flexDirection: 'row',
    borderRadius: 15,
    justifyContent: 'space-evenly',
    alignItems: 'center',
    paddingHorizontal: 10,
  },
  headerTitleContainer: {
    //flex: 1,
    width: '25%',
    height: '100%',
    justifyContent: 'center',
    alignItems: 'center',
  },
});
