import AsyncStorage from '@react-native-community/async-storage';
import axios from 'axios';
import moment from 'moment';
import {Toast, Card} from 'native-base';
import React, {Component} from 'react';
import {Text, View} from 'react-native';
import AwesomeAlert from 'react-native-awesome-alerts';
import {Agenda} from 'react-native-calendars';
import Timeline from 'react-native-timeline-flatlist';
import {Colors} from '../../utils/configs/Colors';
import Loader from '../common/Loader';
import Const from '../../components/common/Constants';
import CustomLabel from '../common/CustomLabel';
import {Badge} from 'react-native-elements';

export class NormalReport extends Component {
  state = {
    report: [],
    date: new Date(),
    loader: true,
    StaffNo: '',
    institute_id: '',
    bearer_token: '',
    showAlert: false,
    items: {},
    activeTab: 'Normal Check-in',
  };

  componentDidMount() {
    setTimeout(() => {
      this.setState({loader: false});
    }, 700);
    this.retreiveData();
  }

  retreiveData = async () => {
    try {
      const user_id = await AsyncStorage.getItem('user_id');
      const institute_id = await AsyncStorage.getItem('institute_id');
      const bearer_token = await AsyncStorage.getItem('bearer_token');

      this.setState({StaffNo: user_id, institute_id, bearer_token}, () => {
        this.getStaffReport(user_id, institute_id, bearer_token);
        this.getAppointments(user_id, institute_id, bearer_token);
        this.getTasks(user_id, institute_id, bearer_token);
      });
    } catch (e) {
      alert('Error retreiving data. Please try again.');
    }
  };

  getTasks = async (user_id, institute_id, bearer_token) => {
    const url = `${Const}api/Staff/StaffTasks`;
    const body = {
      staffCode: user_id,
      InstituteId: Number(institute_id),
      Date: this.state.date,
    };
    try {
      const response = await axios.post(url, body, {
        headers: {
          Authorization: 'Bearer ' + bearer_token,
        },
      });
      const {data} = response;
      console.log('tasks', data);
      if (data.length === 0) return;
      const tasks = data.map((task) => {
        return {
          title: task.task,
          startTime: task.start_Time,
          endTime: task.end_Date,
          startDateTime: moment(task.task_Date).format('YYYY-MM-DD'),
          status: task.isCompleted ? 'Completed' : 'Not Completed',
        };
      });
      const obj = this.groupByKey(tasks, 'startDateTime');
      console.log('tasksobject ==', obj);
      this.setState({items: {...this.state.items, ...obj}}, () =>
        console.log(this.state.items),
      );
    } catch (e) {
      alert('Error retreiving current tasks: ' + e);
    }
  };

  getAppointments = async (user_id, institute_id, bearer_token) => {
    const instituteId = institute_id;
    const StaffCode = user_id;
    const calendarView = 'month';
    const url = `${Const}api/GeoFencing/travelcheckin/Details/${instituteId}/${StaffCode}/${calendarView}`;

    try {
      const response = await axios.get(url);
      const marked = response.data;
      const modified = marked.map((each) => {
        return {
          title: each.title,
          startTime: each.startDateTime,
          endTime: each.endDateTime,
          startDateTime: moment(each.startDateTime).format('YYYY-MM-DD'),
          status: each.status || 'Not Completed',
        };
      });
      //console.log('marked', modified);
      const obj = this.groupByKey(modified, 'startDateTime');
      console.log('obj', obj);
      this.setState({items: {...this.state.items, ...obj}});
    } catch (e) {
      console.log('err', e);
    }
  };
  // getTravelReport = async (user_id, institute_id, bearer_token) => {
  //   const url = `${Const}api/Staff/StaffCummulativeTravelCheckInActivities`;
  //   try {
  //     const response = await axios.post();
  //   } catch (e) {}
  // };

  groupByKey = (array, key) => {
    return array.reduce((hash, obj) => {
      if (obj[key] === undefined) return hash;
      return Object.assign(hash, {
        [obj[key]]: (hash[obj[key]] || []).concat(obj),
      });
    }, {});
  };

  getStaffReport = async (user_id) => {
    const bearer_token = await AsyncStorage.getItem('bearer_token');
    const institute_id = await AsyncStorage.getItem('institute_id');
    console.log('date', this.state.date);
    const sub = moment(this.state.date)
      .subtract(24, 'hours')
      .format('YYYY-MM-DD');
    var from_date =
      this.state.activeTab === 'Normal Check-in'
        ? sub
        : moment(this.state.date).format('YYYY-MM-DD');
    var to_date =
      this.state.activeTab === 'Normal Check-in'
        ? sub
        : moment(this.state.date).format('YYYY-MM-DD');
    //console.log('from and to ==', from_date, to_date);
    this.setState({showAlert: true});
    //var url = `${Const}api/Staff/GetStaffEntryExitReport/${user_id}/${from_date}/${to_date}`;
    var url =
      this.state.activeTab === 'Normal Check-in'
        ? `${Const}api/Staff/StaffCummulativeActivities`
        : `${Const}api/Staff/StaffCummulativeTravelCheckInActivities`;
    fetch(url, {
      method: 'POST',
      body: JSON.stringify({
        FromDate: from_date,
        ToDate: to_date,
        InstituteId: institute_id,
        StaffCodes: [user_id],
        IsTravelReport: true,
        IsMobileApp: true,
      }),
      headers: {
        Authorization: 'Bearer ' + bearer_token,
        Accept: 'text/plain',
        'Content-Type': 'application/json-patch+json',
      },
    })
      .then((response) => response.json())
      .then((json) => {
        if (json?.staffReport?.length > 0) {
          const report = json.staffReport;
          const dateWiseReport = report.filter((e) => e.dateWiseActivities);
          //console.log('dateWiseReport', dateWiseReport);
          const find = dateWiseReport.find((e, i) => i === 0);
          //console.log('find', find);
          const dateWise = find.dateWiseActivities;
          //console.log('dateWise', dateWise);

          const currentDateReport = dateWise.find(
            (e) =>
              moment(e.captureDate).format('YYYY-MM-DD') ===
              moment(this.state.date).format('YYYY-MM-DD'),
          );
          const activities = currentDateReport?.activities;
          const reverse = activities?.reverse();
          console.log('activities', activities);
          if (this.state.activeTab === 'Travel Check-in') {
            const locations = activities.filter((each) => each.coordinates);
            if (locations?.length > 0) {
              const origin = locations[0];
              const destination = locations[locations.length - 1];
              const slicedArray = locations.slice(1, -1);
              const inn = slicedArray.map((e) =>
                e.coordinates ? e.coordinates : COORDINATES,
              );
              this.setState({locations, origin, destination, inn});
            }
          }
          //console.log('currentDateReport', activities);

          this.setState({report: reverse || [], showAlert: false});
        } else {
          Toast.show({
            text: 'No entry exit report found',
            duration: 3500,
            type: 'warning',
            textStyle: {
              fontFamily: 'Poppins-Regular',
              color: '#ffffff',
              textAlign: 'center',
              fontSize: 14,
            },
          });
          this.setState({report: [], showAlert: false});
        }
      })
      .catch((error) => {
        this.setState({showAlert: false});
        console.error(error);
      });
  };

  loadItems = (day) => {
    console.log('day', day);
  };

  renderItem(item) {
    return (
      <Card style={{padding: 10, marginTop: 10}}>
        <CustomLabel title={item.title} />

        <Text>
          {moment(item.startTime).format('h:mm a')} to{' '}
          {moment(item.endTime).format('h:mm a')}
        </Text>
        <Badge
          value={item.status}
          textStyle={{fontFamily: 'Poppins-Regular'}}
          badgeStyle={{
            paddingVertical: 10,
            backgroundColor: Colors.header,
          }}
          containerStyle={{marginVertical: 5, alignSelf: 'flex-end'}}
        />
      </Card>
    );
  }

  renderEmptyDate() {
    return (
      <View style={{alignItems: 'center', justifyContent: 'center'}}>
        <Text>No appointment.</Text>
      </View>
    );
  }

  rowHasChanged(r1, r2) {
    return r1.name !== r2.name;
  }

  render() {
    //console.log('report', this.state.report);
    if (this.state.loader) {
      return <Loader />;
    }
    return (
      <>
        <AwesomeAlert
          show={this.state.showAlert}
          showProgress={true}
          title={this.state.message || 'Loading'}
          closeOnTouchOutside={false}
          closeOnHardwareBackPress={false}
        />
        <Agenda
          items={this.state.items}
          loadItemsForMonth={() => {}}
          selected={this.state.date}
          onDayPress={(day) =>
            this.setState(
              {
                date: day.dateString,
                // origin: null,
                // destination: null,
                // inn: [],
                // locations: [],
                // distance: '0',
              },
              () => {
                this.getStaffReport(this.state.StaffNo);
                this.getTasks(
                  this.state.user_id,
                  this.state.institute_id,
                  this.state.bearer_token,
                );
                console.log('day', day);
                // this.getTotalWorkingDays(
                //   this.state.user_id,
                //   this.state.institute_id,
                //   this.state.bearer_token,
                //   day.dateString,
                // );
              },
            )
          }
          renderItem={this.renderItem}
          renderEmptyDate={this.renderEmptyDate}
          renderEmptyData={this.renderEmptyDate}
          rowHasChanged={this.rowHasChanged}
          showClosingKnob={true}
          futureScrollRange={100}
          style={{maxHeight: 300}}
          displayLoadingIndicator={false}
          theme={{
            agendaKnobColor: Colors.header,
            agendaDayNumColor: Colors.maroon,
            agendaTodayColor: Colors.header,
            selectedDayBackgroundColor: Colors.header,
            selectedDayTextColor: Colors.white,
            dotColor: Colors.maroon,
            todayTextColor: Colors.maroon,
          }}
        />
        {this.state.report.length > 0 && (
          <Timeline
            data={this.state.report.map((rr) => {
              return {
                time: moment(rr.capturedTime).format('h:mm:ss a'),
                title: rr.activity || 'Location',
                description: rr.geoLocatioName || rr.activity,
              };
            })}
            style={{padding: 10}}
            circleColor={Colors.header}
            timeStyle={{
              textAlign: 'center',
              backgroundColor: '#ff9797',
              color: 'white',
              padding: 5,
              borderRadius: 13,
              //color: Colors.header,
              fontFamily: 'Poppins-Regular',
            }}
            timeContainerStyle={{minWidth: 100}}
            innerCircle={'dot'}
            lineColor={Colors.header}
            options={{
              nestedScrollEnabled: true,
            }}
          />
        )}
      </>
    );
  }
}

export default NormalReport;
