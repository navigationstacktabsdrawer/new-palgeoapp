import AsyncStorage from '@react-native-community/async-storage';
import axios from 'axios';
import moment from 'moment';
import {Toast, Card, Picker} from 'native-base';
import React, {Component} from 'react';
import {Text, View, StyleSheet, Image} from 'react-native';
import AwesomeAlert from 'react-native-awesome-alerts';
import {Agenda} from 'react-native-calendars';
import Timeline from 'react-native-timeline-flatlist';
import {Colors} from '../../utils/configs/Colors';
import Loader from '../common/Loader';
import Const from '../../components/common/Constants';
import CustomLabel from '../common/CustomLabel';
import {Avatar, Badge} from 'react-native-elements';
import MapView, {Marker, Polyline} from 'react-native-maps';
import MapViewDirections from 'react-native-maps-directions';
import {GOOGLE_MAPS_APIKEY} from '../../utils/configs/Constants';
import haversine from 'haversine';
import {Dimensions} from 'react-native';
const LAT_DELTA = 0.01;
const LONG_DELTA = 0.01;
const COORDINATES = {
  latitude: 19.0339284,
  longitude: 72.9502294,
};
const {width, height} = Dimensions.get('window');
export class TravelReport extends Component {
  mapView = null;
  state = {
    report: [],
    date: new Date(),
    loader: true,
    StaffNo: '',
    institute_id: '',
    bearer_token: '',
    showAlert: false,
    items: {},
    activeTab: 'Travel Check-in',
    origin: null,
    destination: null,
    inn: [],
    locations: [],
    message: null,
    distance: '0',
    photo: '',
    tripsArray: [],
    pickerTrips: [],
    currentTrip: [],
    currentIndex: 0,
    currentTripPicker: 'Trip 1',
  };

  componentDidMount() {
    setTimeout(() => {
      this.setState({loader: false});
    }, 400);
    //this.retreiveData();
    this.retreiveDataNew();
  }

  retreiveDataNew = async () => {
    try {
      const user_id = await AsyncStorage.getItem('user_id');
      const institute_id = await AsyncStorage.getItem('institute_id');
      const bearer_token = await AsyncStorage.getItem('bearer_token');
      const profile_pic = await AsyncStorage.getItem('profile_pic');

      this.setState(
        {StaffNo: user_id, institute_id, bearer_token, photo: profile_pic},
        () => {
          this.getStaffReportNew(user_id, institute_id, bearer_token);
          this.getAppointments(user_id, institute_id, bearer_token);
          this.getTasks(user_id, institute_id, bearer_token);
        },
      );
    } catch (e) {
      alert('Error retreiving data. Please try again.');
    }
  };

  getActivities = (json) => {
    const report = json.staffReport;
    const dateWiseReport = report.filter((e) => e.dateWiseActivities);
    const find = dateWiseReport.find((e, i) => i === 0);
    const dateWise = find.dateWiseActivities;
    const currentDateReport = dateWise.find(
      (e) =>
        moment(e.captureDate).format('YYYY-MM-DD') ===
        moment(this.state.date).format('YYYY-MM-DD'),
    );
    //this.setState({distance: currentDateReport.distance});
    const activities = currentDateReport?.activities;
    return activities;
  };

  getAllIndexes = (arr, val) => {
    var indexes = [],
      i;
    for (i = 0; i < arr.length; i++)
      if (arr[i].travelCheckInType === val) indexes.push(i);
    return indexes;
  };

  getStaffReportNew = async (user_id, institute_id, bearer_token) => {
    const from_date = moment(this.state.date).format('YYYY-MM-DD');
    const to_date = from_date;
    this.setState({showAlert: true});
    const url = `${Const}api/Staff/StaffCummulativeTravelCheckInActivities`;
    const body = {
      FromDate: from_date,
      ToDate: to_date,
      InstituteId: institute_id,
      StaffCodes: [user_id],
      IsTravelReport: true,
      IsMobileApp: true,
    };
    const headers = {
      headers: {
        Authorization: 'Bearer ' + bearer_token,
      },
    };
    try {
      const response = await axios.post(url, body, headers);
      const json = response.data;
      if (json?.staffReport?.length > 0) {
        const activities = this.getActivities(json);

        if (activities.length > 0) {
          const tripsIndexes = this.getAllIndexes(activities, 4);
          //console.log('tripsIndexes', tripsIndexes);
          let tripsArray = [];
          let j = 0;
          //console.log('j', j);
          for (let i = 0; i < tripsIndexes.length; i++) {
            let u = tripsIndexes[i] + 1;
            const trip = activities.slice(j, u);
            tripsArray.push(trip);
            j = tripsIndexes[i] + 1;
          }
          const pickerTrips = tripsArray.map((e, i) => `Trip ${i + 1}`);
          //console.log('tripsArray', tripsArray);
          this.setState({
            tripsArray,
            pickerTrips,
            currentTrip: tripsArray[this.state.currentIndex],
          });
          const locations = tripsArray[this.state.currentIndex].filter(
            (each) => each.coordinates,
          );
          console.log('locations', locations);
          if (locations?.length > 0) {
            const origin = locations[0];
            const destination = locations[locations.length - 1];
            const slicedArray = locations.slice(1, -1);
            const inn = slicedArray.map((e) =>
              e.coordinates ? e.coordinates : COORDINATES,
            );
            this.setState({locations, origin, destination, inn});
          }
          const reverse = tripsArray[this.state.currentIndex].reverse();
          //console.log('reverse', reverse);
          this.setState({report: reverse || []});
        }
        this.setState({showAlert: false});
      } else {
        Toast.show({
          text: 'No trips found',
          duration: 3500,
          type: 'warning',
          textStyle: {
            fontFamily: 'Poppins-Regular',
            color: '#ffffff',
            textAlign: 'center',
            fontSize: 14,
          },
        });
        this.setState({report: [], showAlert: false});
      }
    } catch (error) {
      this.setState({showAlert: false});
    }
  };

  getTasks = async (user_id, institute_id, bearer_token) => {
    const url = `${Const}api/Staff/StaffTasks`;
    const body = {
      staffCode: user_id,
      InstituteId: Number(institute_id),
      Date: this.state.date,
    };
    try {
      const response = await axios.post(url, body, {
        headers: {
          Authorization: 'Bearer ' + bearer_token,
        },
      });
      const {data} = response;
      console.log('tasks', data);
      if (data.length === 0) return;
      const tasks = data.map((task) => {
        return {
          title: task.task,
          startTime: task.start_Time,
          endTime: task.end_Date,
          startDateTime: moment(task.task_Date).format('YYYY-MM-DD'),
          status: task.isCompleted ? 'Completed' : 'Not Completed',
        };
      });
      const obj = this.groupByKey(tasks, 'startDateTime');
      console.log('tasksobject ==', obj);
      this.setState({items: {...this.state.items, ...obj}}, () =>
        console.log(this.state.items),
      );
    } catch (e) {
      alert('Error retreiving current tasks: ' + e);
    }
  };

  getAppointments = async (user_id, institute_id, bearer_token) => {
    const instituteId = institute_id;
    const StaffCode = user_id;
    const calendarView = 'month';
    const url = `${Const}api/GeoFencing/travelcheckin/Details/${instituteId}/${StaffCode}/${calendarView}`;

    try {
      const response = await axios.get(url);
      const marked = response.data;
      const modified = marked.map((each) => {
        return {
          title: each.title,
          startTime: each.startDateTime,
          endTime: each.endDateTime,
          startDateTime: moment(each.startDateTime).format('YYYY-MM-DD'),
          status: each.status || 'Not Completed',
        };
      });
      //console.log('marked', modified);
      const obj = this.groupByKey(modified, 'startDateTime');
      console.log('obj', obj);
      this.setState({items: {...this.state.items, ...obj}});
    } catch (e) {
      console.log('err', e);
    }
  };
  // getTravelReport = async (user_id, institute_id, bearer_token) => {
  //   const url = `${Const}api/Staff/StaffCummulativeTravelCheckInActivities`;
  //   try {
  //     const response = await axios.post();
  //   } catch (e) {}
  // };

  groupByKey = (array, key) => {
    return array.reduce((hash, obj) => {
      if (obj[key] === undefined) return hash;
      return Object.assign(hash, {
        [obj[key]]: (hash[obj[key]] || []).concat(obj),
      });
    }, {});
  };

  calc = (prevLatLng, newLatLng) => {
    return haversine(prevLatLng, newLatLng) || 0;
  };

  loadItems = (day) => {
    console.log('day', day);
  };

  renderItem(item) {
    return (
      <Card style={{padding: 10, marginTop: 10}}>
        <CustomLabel title={item.title} />

        <Text>
          {moment(item.startTime).format('h:mm a')} to{' '}
          {moment(item.endTime).format('h:mm a')}
        </Text>
        <Badge
          value={item.status}
          textStyle={{fontFamily: 'Poppins-Regular'}}
          badgeStyle={{
            paddingVertical: 10,
            backgroundColor: Colors.header,
          }}
          containerStyle={{marginVertical: 5, alignSelf: 'flex-end'}}
        />
      </Card>
    );
  }

  renderEmptyDate() {
    return (
      <View style={{alignItems: 'center', justifyContent: 'center'}}>
        <Text>No appointment.</Text>
      </View>
    );
  }

  rowHasChanged(r1, r2) {
    return r1.name !== r2.name;
  }

  render() {
    const {origin, destination, locations, photo, inn} = this.state;
    console.log('wayPoints_length', this.state.inn);
    if (this.state.loader) {
      return <Loader />;
    }
    return (
      <>
        <AwesomeAlert
          show={this.state.showAlert}
          showProgress={true}
          title={this.state.message || 'Loading'}
          closeOnTouchOutside={false}
          closeOnHardwareBackPress={false}
        />
        <Agenda
          items={this.state.items}
          loadItemsForMonth={() => {}}
          selected={this.state.date}
          onDayPress={(day) =>
            this.setState(
              {
                date: day.dateString,
                origin: null,
                destination: null,
                inn: [],
                locations: [],
                distance: '0',
              },
              () => {
                this.getStaffReportNew(
                  this.state.StaffNo,
                  this.state.institute_id,
                  this.state.bearer_token,
                );
                this.getTasks(
                  this.state.user_id,
                  this.state.institute_id,
                  this.state.bearer_token,
                );
                console.log('day', day);
                // this.getTotalWorkingDays(
                //   this.state.user_id,
                //   this.state.institute_id,
                //   this.state.bearer_token,
                //   day.dateString,
                // );
              },
            )
          }
          renderItem={this.renderItem}
          renderEmptyDate={this.renderEmptyDate}
          renderEmptyData={this.renderEmptyDate}
          rowHasChanged={this.rowHasChanged}
          showClosingKnob={true}
          futureScrollRange={100}
          style={{maxHeight: 300}}
          displayLoadingIndicator={false}
          theme={{
            agendaKnobColor: Colors.header,
            agendaDayNumColor: Colors.maroon,
            agendaTodayColor: Colors.header,
            selectedDayBackgroundColor: Colors.header,
            selectedDayTextColor: Colors.white,
            dotColor: Colors.maroon,
            todayTextColor: Colors.maroon,
          }}
        />
        {this.state.activeTab === 'Travel Check-in' && origin && destination && (
          <View>
            {this.state.tripsArray?.length > 1 && (
              <Picker
                style={{flex: 0}}
                selectedValue={this.state.currentTripPicker}
                onValueChange={(value, i) => {
                  console.log('val', value);
                  this.setState(
                    {currentTripPicker: value, currentIndex: i},
                    () =>
                      this.getStaffReportNew(
                        this.state.StaffNo,
                        this.state.institute_id,
                        this.state.bearer_token,
                      ),
                  );
                }}>
                {this.state.pickerTrips.map((e, i) => (
                  <Picker.Item label={e} key={i} value={e} />
                ))}
              </Picker>
            )}
            <View style={styles.map}>
              <MapView
                style={{width: '100%', height: '100%'}}
                //mapType={'satellite'}
                ref={(c) => (this.mapView = c)}
                loadingEnabled
                followUserLocation={true}
                //cacheEnabled={true}
                initialRegion={{
                  latitude: parseFloat(
                    destination?.coordinates?.latitude || COORDINATES.latitude,
                  ),
                  longitude: parseFloat(
                    destination?.coordinates?.longitude ||
                      COORDINATES.longitude,
                  ),
                  latitudeDelta: LAT_DELTA,
                  longitudeDelta: LONG_DELTA,
                }}
                zoomEnabled={true}>
                {/* {inn.length > 0 &&
                inn.map((e, i) => <Marker key={i} coordinate={e} />)} */}
                {locations.length > 0 && (
                  <>
                    <Marker
                      key={origin.capturedTime}
                      coordinate={origin.coordinates}
                      opacity={0.7}
                      //description={'Origin'}
                      title={moment(origin.capturedTime)
                        .local()
                        .format('h:mm a')}>
                      <Image
                        source={{
                          uri: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ_8jXWxkA50-ColElavoSs1x8pLcz5m9fj2A&usqp=CAU',
                        }}
                        style={{
                          width: 60,
                          height: 60,
                          borderRadius: 60,
                          //backgroundColor: 'transparent',
                          resizeMode: 'contain',
                          //tintColor: 'green',
                        }}
                      />
                    </Marker>
                    <Marker
                      key={destination.capturedTime}
                      coordinate={destination.coordinates}
                      //opacity={0.7}
                      description={'Destination'}
                      title={moment(destination.capturedTime)
                        .local()
                        .format('h:mm a')}>
                      <>
                        <Avatar
                          rounded
                          size="medium"
                          avatarStyle={{
                            width: '100%',
                            height: '100%',
                            //borderRadius: 50,
                            resizeMode: 'stretch',
                          }}
                          // containerStyle={{
                          //   position: 'absolute',
                          //   top: 0,
                          //   right: 0,
                          //   zIndex: 1,
                          // }}
                          source={{
                            uri:
                              photo ||
                              'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQtOqCkEk1bHWlechHBJVOMBkfxoe9vXRO9SH0aTfy8mhNfXVH0DPk0Iu7LEYGg4YlIeAE&usqp=CAU',
                          }}
                        />
                      </>
                    </Marker>
                  </>
                )}

                {locations.length > 0 && (
                  // <Polyline
                  // coordinates = {[this.state.origin.coordinates, ...inn, this.state.destination.coordinates]}
                  // //   strokeWidth={8}
                  // //   strokeColor="red"
                  // />
                  <MapViewDirections
                    //optimizeWaypoints
                    onStart={(params) => {
                      this.setState({
                        showAlert: true,
                        message: `Fetching distance between initial and latest/last location...`,
                      });
                    }}
                    onReady={(result) => {
                      this.setState({
                        showAlert: false,
                        distance: result.distance,
                      });
                      this.mapView.fitToCoordinates(result.coordinates, {
                        edgePadding: {
                          right: width / 20,
                          bottom: height / 20,
                          left: width / 20,
                          top: height / 20,
                        },
                      });
                      console.log(`Distance: ${result.distance} km`);
                      // console.log(`Duration: ${result.duration} min.`);
                    }}
                    onError={(err) => alert('Error fetching path: ' + err)}
                    origin={origin.coordinates}
                    waypoints={inn}
                    splitWaypoints={inn.length > 25}
                    destination={destination.coordinates}
                    apikey={GOOGLE_MAPS_APIKEY}
                    strokeWidth={8}
                    strokeColor="red"
                  />
                )}
              </MapView>
              <View
                style={{
                  position: 'absolute',
                  top: 0,
                  right: 0,
                  zIndex: 1,
                  borderWidth: 2,
                  borderColor: Colors.header,
                  padding: 5,
                }}>
                <Text>{Number(this.state.distance).toFixed(2) || '0'} km</Text>
              </View>
            </View>
          </View>
        )}
        {this.state.report.length > 0 && (
          <Timeline
            data={this.state.report
              .filter((e) => e.activity !== null && e.geoLocatioName !== null)
              .map((rr) => {
                return {
                  time: moment(rr.capturedTime).format('h:mm:ss a'),
                  title: rr.activity || 'Location',
                  description: rr.geoLocatioName || rr.activity,
                };
              })}
            style={{padding: 10}}
            circleColor={Colors.header}
            timeStyle={{
              textAlign: 'center',
              backgroundColor: '#ff9797',
              color: 'white',
              padding: 5,
              borderRadius: 13,
              //color: Colors.header,
              fontFamily: 'Poppins-Regular',
            }}
            timeContainerStyle={{minWidth: 100}}
            innerCircle={'dot'}
            lineColor={Colors.header}
            options={{
              nestedScrollEnabled: true,
            }}
          />
        )}
      </>
    );
  }
}

const styles = StyleSheet.create({
  map: {
    height: 200,
    width: '100%',
    alignSelf: 'center',
    //marginTop: 30,
  },
});

export default TravelReport;
