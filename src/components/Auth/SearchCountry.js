import React, { Component } from 'react';
import { StyleSheet, Text, View, ScrollView, TouchableOpacity } from 'react-native';
import SearchInput, { createFilter } from 'react-native-search-filter';
import emails from './CountryCode.json';
const KEYS_TO_FILTERS = ['dial_code','name','code'];
export default class SearchCountry extends Component {
 constructor(props) {
    super(props);
    this.state = {
      searchTerm: ''
    }
  }
  searchUpdated(term) {
    this.setState({ searchTerm: term })
  }
  selectCountryCode = (countryCode) => {
    this.props.navigation.navigate('RegisterPin',{
        CountryCode : countryCode
    });
  }
  render() {
    const filteredEmails = emails.filter(createFilter(this.state.searchTerm, KEYS_TO_FILTERS))
    return (
      <View style={styles.container}>
        <SearchInput 
            onChangeText={(term) => { this.searchUpdated(term) }} 
            style={styles.searchInput}
            placeholder="Search by country . . ."
        />
        <ScrollView>
          {filteredEmails.map((item,index) => {
            return (
              <TouchableOpacity onPress={()=>this.selectCountryCode(item.dial_code)} key={index} style={styles.emailItem}>
                <View>
                  <Text style={styles.emailSubject}>({item.dial_code}) {item.name} ({item.code})</Text>
                </View>
              </TouchableOpacity>
            )
          })}
        </ScrollView>
      </View>
    );
  }
}
 
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    justifyContent: 'flex-start'
  },
  emailItem:{
    borderBottomWidth: 0.5,
    borderColor: 'rgba(0,0,0,0.3)',
    padding: 10
  },
  emailSubject: {
    fontFamily : 'Poppins-Regular'
  },
  searchInput:{
    padding: 10,
    borderColor: '#CCC',
    borderWidth: 1,
    fontFamily : 'Poppins-Regular'
  }
});