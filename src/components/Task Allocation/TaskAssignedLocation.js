import React, {Component} from 'react';
import {
  View,
  Text,
  StyleSheet,
  TouchableWithoutFeedback,
  Image,
  Dimensions,
} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {Container, Content, Item, Input, Icon, Textarea} from 'native-base';
import SubHeader from '../common/SubHeader';
import Loader from '../common/Loader';
import DateTimePickerModal from 'react-native-modal-datetime-picker';
import AwesomeAlert from 'react-native-awesome-alerts';
import Const from '../common/Constants';
import AsyncStorage from '@react-native-community/async-storage';
import CheckBox from 'react-native-check-box';
import ImagePicker from 'react-native-image-crop-picker';
import RNFetchBlob from 'rn-fetch-blob';
import DocumentPicker from 'react-native-document-picker';
import CustomPicker from '../common/CustomPicker';
import moment from 'moment';
import {TouchableOpacity} from 'react-native';
import {CustomCalendar} from '../common/CustomCalendar';
import CustomLabel from '../common/CustomLabel';
import {Colors} from '../../utils/configs/Colors';
const SubThemeColor = Colors.secondary;
const ThemeColor = Colors.header;
const screenWidth = Dimensions.get('window').width;
export default class TaskAssignedLocation extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loader: false,
      showAlert: false,
      isMustComplete: true,
      TaskFrequency: [
        {name: 'Not-Repeat', id: 1},
        {name: 'Daily', id: 2},
        {name: 'Weekly', id: 3},
        {name: 'Monthly', id: 4},
      ],
      SelectedInstitute: this.props.route.params
        ? this.props.route.params.SelectedInstitute
          ? this.props.route.params.SelectedInstitute
          : []
        : [],
      SelectedTaskType: this.props.route.params
        ? this.props.route.params.SelectedTaskType
          ? this.props.route.params.SelectedTaskType
          : []
        : [],
      SelectedBranch: this.props.route.params
        ? this.props.route.params.SelectedBranch
          ? this.props.route.params.SelectedBranch
          : []
        : [],
      SelectedStaffTypes: this.props.route.params
        ? this.props.route.params.SelectedStaffTypes
          ? this.props.route.params.SelectedStaffTypes
          : []
        : [],
      SelectedDepartment: this.props.route.params
        ? this.props.route.params.SelectedDepartment
          ? this.props.route.params.SelectedDepartment
          : []
        : [],
      SelectedDesignation: this.props.route.params
        ? this.props.route.params.SelectedDesignation
          ? this.props.route.params.SelectedDesignation
          : []
        : [],
      SelectedStaffMembers: this.props.route.params
        ? this.props.route.params.SelectedStaffMembers
          ? this.props.route.params.SelectedStaffMembers
          : []
        : [],
      subject: '',
      bearer_token: '',
      institute_id: '',
      showAlert1: false,
      error_message: '',
      org_id: '',
      SelectedTaskFrequency: '',
      time: new Date(),
      time2: new Date(),
      FromDate: new Date(),
      ToDate: new Date(),
      File: {
        FileName: '',
        FileType: '',
        Attachment: '',
      },
      IsMon: false,
      IsTue: false,
      IsWed: false,
      IsThu: false,
      IsFri: false,
      IsSat: false,
      IsSun: false,
    };
  }
  componentDidMount() {
    console.log('props == ', this.props.route?.params);
    AsyncStorage.getItem('bearer_token').then((bearer_token) => {
      AsyncStorage.getItem('institute_id').then((institute_id) => {
        AsyncStorage.getItem('org_id').then((org_id) => {
          this.setState({
            bearer_token: bearer_token,
            institute_id: institute_id,
            org_id: org_id,
          });
        });
      });
    });
  }

  handleConfirm1 = (time) => {
    console.log('time', time);
    this.setState({time}, () => this.hideDatePicker1());
  };
  handleConfirm2 = (time) => {
    this.setState({time2: time}, () => this.hideDatePicker2());
  };

  hideDatePicker1 = () => {
    if (this.state.time && this.state.time2) {
      //this.DateCheckerFunction()
    }
    this.setState({showFirstTime: false});
  };
  hideDatePicker2 = () => {
    if (this.state.time && this.state.time2) {
      // this.DateCheckerFunction();
    }
    this.setState({showSecondTime: false});
  };

  AddOrUpdateStaffTaskDetail = () => {
    console.log(
      'url = ',
      Const + 'StaffTaskAllotment/AddOrUpdateStaffTaskDetail',
    );

    this.setState({loader: true});
    let formData = new FormData();
    formData.append('id', 0);
    formData.append('instituteId', parseInt(this.state.SelectedInstitute));
    formData.append('departmentId', parseInt(this.state.SelectedDepartment));
    formData.append('designationId', parseInt(this.state.SelectedDesignation)),
      formData.append('isMon', false);
    formData.append('isTue', false);
    formData.append('isWed', false);
    formData.append('isThu', false);
    formData.append('isFri', false);
    formData.append('isSat', false);
    formData.append('isSun', false);
    formData.append('orgId', this.state.SelectedInstitute);
    formData.append('staffName', this.state.SelectedStaffMembers);
    //staffName: ["1000222"],
    formData.append('task_Description', this.state.Task);
    formData.append('task_subject', this.state.subject);
    formData.append('task_StartDate', this.state.FromDate);
    formData.append('task_EndDate', this.state.ToDate);
    formData.append('task_StartTime', this.state.time);
    formData.append('task_EndTime', this.state.time2);
    formData.append('task_Type', this.state.SelectedTaskFrequency);
    formData.append('isMust', this.state.isMustComplete);
    formData.append('dates', '');
    //formData.append('attachments', JSON.stringify([]));
    console.log('formdata == ', formData);
    fetch(Const + 'StaffTaskAllotment/AddOrUpdateStaffTaskDetail', {
      method: 'POST',
      headers: {
        Accept: 'text/plain',
        'Content-Type': 'multipart/form-data',
      },
      body: formData,
    })
      .then((response) => {
        console.log('res', JSON.stringify(response, null, 4));
        return response.json();
      })
      .then((json) => {
        console.log('AddOrUpdateStaffTaskDetail = ', json);
        this.setState({loader: false});
        if (json.length > 0) {
          alert(json);
        } else {
          alert(json);
        }
      })
      .catch((error) => {
        alert(error);
        this.setState({loader: false});
      });
  };
  handleCheckbox1 = () => {
    this.setState({isMustComplete: !this.state.isMustComplete});
  };
  SelectTaskImage = async () => {
    try {
      const res = await DocumentPicker.pick({
        includeBase64: true,
        type: [DocumentPicker.types.allFiles],
      });
      let typeIdentifier = res.type.split('/');
      console.log(typeIdentifier[1]);
      //alert(typeIdentifier[1]);
      RNFetchBlob.fs
        .readFile(res.uri, 'base64')
        .then((data) => {
          var Attachment = {
            FileName:
              'TaskImage' + new Date().getTime() + '.' + typeIdentifier[1],
            FileType: res.type,
            Attachment: data,
          };

          this.setState({File: Attachment});
        })
        .catch((err) => {});
    } catch (err) {
      console.log('Unknown Error: ' + JSON.stringify(err));
    }
  };

  render() {
    if (this.state.loader) {
      return <Loader />;
    }
    return (
      <View style={styles.container}>
        <AwesomeAlert
          show={this.state.showAlert}
          showProgress={true}
          title="Loading"
          closeOnTouchOutside={false}
          closeOnHardwareBackPress={false}
        />
        <AwesomeAlert
          show={this.state.showAlert1}
          showProgress={false}
          title="Attention"
          message={this.state.error_message}
          closeOnTouchOutside={true}
          closeOnHardwareBackPress={false}
          showCancelButton={true}
          cancelText="Okay"
          onCancelPressed={() => {
            this.setState({showAlert1: false});
          }}
          cancelButtonColor="#f05760"
          cancelButtonTextStyle={{fontFamily: 'Poppins-Regular', fontSize: 13}}
          messageStyle={{fontFamily: 'Poppins-Regular', fontSize: 13}}
          titleStyle={{fontFamily: 'Poppins-SemiBold', fontSize: 14}}
        />
        <Container>
          <SubHeader
            title="Task Allocation Step - 2"
            showBack={true}
            backScreen="TaskPreferences"
            navigation={this.props.navigation}
          />
          <Content>
            <View style={styles.cardContainer}>
              <View style={{marginLeft: '2%', marginRight: '2%'}}>
                <View style={styles.labelContainer}>
                  <CustomLabel title={'Subject'} />
                </View>
                <View>
                  <Item regular style={styles.item}>
                    <Input
                      placeholder=""
                      style={styles.input}
                      value={this.state.subject}
                      onChangeText={(subject) => {
                        this.setState({subject: subject});
                      }}
                    />
                  </Item>
                </View>
              </View>
              {
                <View
                  style={{
                    marginTop: '2%',
                    marginLeft: '2%',
                    marginRight: '2%',
                  }}>
                  <View style={styles.labelContainer}>
                    <View style={styles.row}>
                      <CustomLabel title={'Description'} />
                    </View>
                  </View>
                  <View style={styles.item1}>
                    <Textarea
                      rowSpan={4}
                      bordered
                      style={styles.textarea}
                      value={this.state.Task}
                      onChangeText={(Task) => {
                        this.setState({Task: Task});
                      }}
                    />
                  </View>
                </View>
              }
              <View style={{marginTop: '4%'}}>
                <CustomLabel title={'    Task Frequency'} />
                <CustomPicker
                  label="Task Frequency"
                  selectedValue={this.state.SelectedTaskFrequency}
                  options={this.state.TaskFrequency}
                  onValueChange={(value) => {
                    this.setState({SelectedTaskFrequency: value});
                  }}
                />
              </View>
              <View
                style={{
                  width: '90%',
                  justifyContent: 'space-between',
                  alignItems: 'center',
                  flexDirection: 'row',
                  alignSelf: 'center',
                  marginVertical: 15,
                  //marginTop: "4%",
                }}>
                <TouchableOpacity
                  onPress={() => this.setState({showFirstTime: true})}>
                  <View style={{width: '100%'}}>
                    <View style={styles.labelContainer}>
                      <CustomLabel title={'Start Time'} />
                      <View
                        style={{
                          width: screenWidth / 2.9,
                          backgroundColor: SubThemeColor,
                          height: 50,
                          borderRadius: 5,
                          justifyContent: 'center',
                          //alignItems: 'center',
                          paddingLeft: 10,
                        }}>
                        <Text style={[styles.label, {color: ThemeColor}]}>
                          {moment(this.state.time).format('h:mm a')}
                        </Text>
                      </View>
                    </View>
                    <DateTimePickerModal
                      isVisible={this.state.showFirstTime}
                      mode="time"
                      onConfirm={this.handleConfirm1}
                      onCancel={this.hideDatePicker1}
                    />
                  </View>
                </TouchableOpacity>
                <TouchableOpacity
                  onPress={() => this.setState({showSecondTime: true})}>
                  <View style={{width: '100%'}}>
                    <View style={styles.labelContainer}>
                      <CustomLabel title={'End Time'} />
                      <View
                        style={{
                          width: screenWidth / 2.9,
                          backgroundColor: SubThemeColor,
                          height: 50,
                          borderRadius: 5,
                          justifyContent: 'center',
                          paddingLeft: 10,
                          //alignItems: 'center',
                        }}>
                        <Text style={[styles.label, {color: ThemeColor}]}>
                          {moment(this.state.time2).format('h:mm a')}
                        </Text>
                      </View>
                    </View>
                    <DateTimePickerModal
                      isVisible={this.state.showSecondTime}
                      mode="time"
                      onConfirm={this.handleConfirm2}
                      onCancel={this.hideDatePicker2}
                    />
                  </View>
                </TouchableOpacity>
              </View>
              <View
                style={{
                  alignItems: 'center',
                  flexDirection: 'row',
                  width: wp('100'),
                  alignSelf: 'center',
                  justifyContent: 'space-around',
                }}>
                <View style={{width: wp('35')}}>
                  <CustomCalendar
                    title={'Start Date'}
                    AvailableLeaves={1000}
                    FromDate={this.state.FromDate}
                    date={this.state.FromDate}
                    onPress={() => this.setState({dateVisible: true})}
                    isVisible={this.state.dateVisible}
                    onConfirm={(date) => {
                      this.setState({
                        FromDate: date,
                        dateVisible: false,
                      });
                    }}
                    style={{backgroundColor: SubThemeColor}}
                    textStyle={{color: ThemeColor}}
                    onCancel={() => this.setState({dateVisible: false})}
                  />
                </View>
                <View style={{width: wp('35')}}>
                  <CustomCalendar
                    title={'End Date'}
                    date={this.state.ToDate}
                    AvailableLeaves={this.state.AvailableLeaves}
                    FromDate={this.state.FromDate}
                    onPress={() => this.setState({dateVisible1: true})}
                    isVisible={this.state.dateVisible1}
                    onConfirm={(date) => {
                      this.setState({
                        ToDate: date,
                        dateVisible1: false,
                        SelectedDate: true,
                      });
                    }}
                    style={{backgroundColor: SubThemeColor}}
                    textStyle={{color: ThemeColor}}
                    onCancel={() => this.setState({dateVisible1: false})}
                  />
                </View>
              </View>

              <View
                style={{marginTop: '4%', marginLeft: '2%', marginRight: '2%'}}>
                <TouchableWithoutFeedback onPress={this.SelectTaskImage}>
                  <View>
                    <Item regular disabled style={styles.item}>
                      <Input
                        placeholder="Choose File"
                        style={styles.input}
                        value={this.state.File.FileName}
                        disabled
                      />
                    </Item>
                  </View>
                </TouchableWithoutFeedback>
              </View>

              <View
                style={{marginTop: '4%', marginLeft: '2%', marginRight: '2%'}}>
                <View
                  style={{
                    flexDirection: 'row',
                    marginLeft: wp('3'),
                    alignItems: 'center',
                  }}>
                  <CheckBox
                    checkedCheckBoxColor="#f05760"
                    isChecked={this.state.isMustComplete}
                    onClick={this.handleCheckbox1}
                  />
                  <Text
                    style={{
                      fontFamily: 'Poppins-Regular',
                      fontSize: 13,
                      marginLeft: 10,
                    }}>
                    Is Must Complete
                  </Text>
                </View>
              </View>
              <View
                style={{
                  width: '100%',
                  flexDirection: 'row',
                  flexWrap: 'wrap',
                  justifyContent: 'flex-start',
                }}>
                <View
                  style={{
                    marginTop: '4%',
                    marginLeft: '2%',
                    marginRight: '2%',
                  }}>
                  <View
                    style={{
                      flexDirection: 'row',
                      marginLeft: wp('3'),
                      alignItems: 'center',
                    }}>
                    <CheckBox
                      checkedCheckBoxColor="#f05760"
                      isChecked={this.state.IsMon}
                      onClick={() => {
                        this.setState({IsMon: !this.state.IsMon});
                      }}
                    />
                    <Text
                      style={{
                        fontFamily: 'Poppins-Regular',
                        fontSize: 13,
                        marginLeft: 10,
                      }}>
                      Mon
                    </Text>
                  </View>
                </View>
                <View
                  style={{
                    marginTop: '4%',
                    marginLeft: '2%',
                    marginRight: '2%',
                  }}>
                  <View
                    style={{
                      flexDirection: 'row',
                      marginLeft: wp('3'),
                      alignItems: 'center',
                    }}>
                    <CheckBox
                      checkedCheckBoxColor="#f05760"
                      isChecked={this.state.IsTue}
                      onClick={() => {
                        this.setState({IsTue: !this.state.IsTue});
                      }}
                    />
                    <Text
                      style={{
                        fontFamily: 'Poppins-Regular',
                        fontSize: 13,
                        marginLeft: 10,
                      }}>
                      Tue
                    </Text>
                  </View>
                </View>
                <View
                  style={{
                    marginTop: '4%',
                    marginLeft: '2%',
                    marginRight: '2%',
                  }}>
                  <View
                    style={{
                      flexDirection: 'row',
                      marginLeft: wp('3'),
                      alignItems: 'center',
                    }}>
                    <CheckBox
                      checkedCheckBoxColor="#f05760"
                      isChecked={this.state.IsWed}
                      onClick={() => {
                        this.setState({IsWed: !this.state.IsWed});
                      }}
                    />
                    <Text
                      style={{
                        fontFamily: 'Poppins-Regular',
                        fontSize: 13,
                        marginLeft: 10,
                      }}>
                      Wed
                    </Text>
                  </View>
                </View>
                <View
                  style={{
                    marginTop: '4%',
                    marginLeft: '2%',
                    marginRight: '2%',
                  }}>
                  <View
                    style={{
                      flexDirection: 'row',
                      marginLeft: wp('3'),
                      alignItems: 'center',
                    }}>
                    <CheckBox
                      checkedCheckBoxColor="#f05760"
                      isChecked={this.state.IsThu}
                      onClick={() => {
                        this.setState({IsThu: !this.state.IsThu});
                      }}
                    />
                    <Text
                      style={{
                        fontFamily: 'Poppins-Regular',
                        fontSize: 13,
                        marginLeft: 10,
                      }}>
                      Thu
                    </Text>
                  </View>
                </View>
                <View
                  style={{
                    marginTop: '4%',
                    marginLeft: '2%',
                    marginRight: '2%',
                  }}>
                  <View
                    style={{
                      flexDirection: 'row',
                      marginLeft: wp('3'),
                      alignItems: 'center',
                    }}>
                    <CheckBox
                      checkedCheckBoxColor="#f05760"
                      isChecked={this.state.IsFri}
                      onClick={() => {
                        this.setState({IsFri: !this.state.IsFri});
                      }}
                    />
                    <Text
                      style={{
                        fontFamily: 'Poppins-Regular',
                        fontSize: 13,
                        marginLeft: 10,
                      }}>
                      Fri
                    </Text>
                  </View>
                </View>
                <View
                  style={{
                    marginTop: '4%',
                    marginLeft: '2%',
                    marginRight: '2%',
                  }}>
                  <View
                    style={{
                      flexDirection: 'row',
                      marginLeft: wp('3'),
                      alignItems: 'center',
                    }}>
                    <CheckBox
                      checkedCheckBoxColor="#f05760"
                      isChecked={this.state.IsSat}
                      onClick={() => {
                        this.setState({IsSat: !this.state.IsSat});
                      }}
                    />
                    <Text
                      style={{
                        fontFamily: 'Poppins-Regular',
                        fontSize: 13,
                        marginLeft: 10,
                      }}>
                      Sat
                    </Text>
                  </View>
                </View>
                <View
                  style={{
                    marginTop: '4%',
                    marginLeft: '2%',
                    marginRight: '2%',
                  }}>
                  <View
                    style={{
                      flexDirection: 'row',
                      marginLeft: wp('3'),
                      alignItems: 'center',
                    }}>
                    <CheckBox
                      checkedCheckBoxColor="#f05760"
                      isChecked={this.state.IsSun}
                      onClick={() => {
                        this.setState({IsSun: !this.state.IsSun});
                      }}
                    />
                    <Text
                      style={{
                        fontFamily: 'Poppins-Regular',
                        fontSize: 13,
                        marginLeft: 10,
                      }}>
                      Sun
                    </Text>
                  </View>
                </View>
              </View>
              <View
                style={{
                  justifyContent: 'center',
                  alignItems: 'center',
                  marginTop: hp('3'),
                }}>
                <TouchableWithoutFeedback
                  onPress={this.AddOrUpdateStaffTaskDetail}>
                  <View style={styles.buttonContainer}>
                    <Image
                      source={require('../../assets/ic_send.png')}
                      style={styles.btImage}
                    />
                    <Text style={styles.buttonText}>Save</Text>
                  </View>
                </TouchableWithoutFeedback>
              </View>
            </View>
          </Content>
        </Container>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fbfbfb',
  },
  header: {
    backgroundColor: '#089bf9',
  },
  title: {
    fontFamily: 'Poppins-SemiBold',
    fontSize: 17,
    color: '#ffffff',
  },
  cardContainer: {
    marginTop: '5%',
    marginLeft: '2%',
    marginRight: '2%',
    justifyContent: 'center',
  },
  label: {
    fontFamily: 'Poppins-Regular',
    fontSize: 14,
    color: '#000000',
  },
  label1: {
    fontFamily: 'Poppins-Regular',
    fontSize: 12,
    color: '#c9c3c5',
    paddingLeft: wp('3'),
  },
  labelContainer: {
    margin: '1.5%',
  },
  input: {
    fontFamily: 'Poppins-Regular',
    fontSize: 13,
    backgroundColor: '#ffffff',
    paddingLeft: '5%',
    borderRadius: 10,
    borderColor: '#f1f1f1',
    borderWidth: 1.5,
  },
  item: {
    borderRadius: 10,
    backgroundColor: '#ffffff',
    height: hp('7'),
    borderColor: '#f1f1f1',
    borderWidth: 1.5,
  },

  item1: {
    borderRadius: 8,
    backgroundColor: '#ffffff',
    borderColor: '#f1f1f1',
    borderWidth: 1.5,
  },
  textarea: {
    borderLeftWidth: 0,
    borderBottomWidth: 0,
    borderTopWidth: 0,
    borderRightWidth: 0,
    borderRadius: 8,
  },
  footerText: {
    fontFamily: 'Poppins-Regular',
    fontSize: 15,
    color: '#ffffff',
  },
  buttonContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
    backgroundColor: '#f05760',
    borderRadius: 20,
    width: wp('30'),
    paddingRight: wp('7'),
    marginTop: '4%',
  },
  buttonText: {
    fontFamily: 'Poppins-Regular',
    fontSize: 13,
    color: '#ffffff',
  },
  btImage: {
    width: 54,
    height: 39,
  },
  row: {
    flexDirection: 'row',
  },
  optional: {
    fontFamily: 'Poppins-Regular',
    color: 'red',
    fontSize: 12,
    margin: 2,
  },
});
