import React, {Component} from 'react';
import {View, StyleSheet} from 'react-native';
import {Container, Tabs, Tab} from 'native-base';
import SubHeader from '../common/SubHeader';
import {Colors} from '../../utils/configs/Colors';
import NormalReport from '../Reports/NormalReport';
import TravelReport from '../Reports/NewTravelReport';

//var p = current.setDate(current.getDate() - 1);
const Home = (props) => {
  return (
    <View style={styles.container}>
      <Container>
        <SubHeader
          title="Reports"
          showBack={true}
          backScreen="Home"
          navigation={props.navigation}
        />
        <Tabs tabContainerStyle={{backgroundColor: Colors.header}}>
          <Tab
            activeTabStyle={{backgroundColor: Colors.header}}
            tabStyle={{backgroundColor: Colors.header}}
            heading="Check-in">
            <NormalReport />
          </Tab>
          <Tab
            activeTabStyle={{backgroundColor: Colors.header}}
            tabStyle={{backgroundColor: Colors.header}}
            heading="Travel History">
            <TravelReport />
          </Tab>
        </Tabs>
      </Container>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fbfbfb',
  },
});

export default Home;
