//Check-in old logic:

getCurrentLocationWithAccuracy = (uri) => {
  let coordinatesTemp = {latitude: '', longitude: '', accuracy: ''};
  this.main(uri);
  this.watchID = Geolocation.watchPosition(
    async (position) => {
      this.setState({
        coordinates: {
          latitude: position.coords.latitude,
          longitude: position.coords.longitude,
          accuracy: position.coords.accuracy,
        },
      });
      coordinatesTemp.latitude = position.coords.latitude;
      coordinatesTemp.longitude = position.coords.longitude;
      coordinatesTemp.accuracy = position.coords.accuracy;
      if (position.coords.accuracy <= 10) {
        Geolocation.clearWatch(this.watchID);
        await this.CheckIn(uri, coordinatesTemp);
        return;
      }
    },
    (e) => {
      console.log(e.message);
    },
    {
      enableHighAccuracy: true,
      distanceFilter: 0,
      interval: 1000,
      fastestInterval: 1000,
    },
  );
};

wait = (ms) => new Promise((resolve, reject) => setTimeout(resolve, ms));

main = async (uri) => {
  await this.wait(10000);
  Geolocation.clearWatch(this.watchId);
  if (this.state.coordinates.accuracy > 10) {
    console.log('accuracy_0', this.state.coordinates.accuracy);
    this.setState({showAlert2: false, showAlert: true});
    await this.CheckIn(uri, this.state.coordinates);
    return;
  } else {
    if (this.state.coordinates.accuracy === '') {
      console.log('accuracy', this.state.coordinates.accuracy);
      this.setState({showAlert2: false, showAlert: true});
      Geolocation.getCurrentPosition(
        (pos) => this.CheckIn(uri, pos.coords),
        () => {},
        {enableHighAccuracy: true, maximumAge: 0, distanceFilter: 0},
      );
    }
    console.log('Accuracy achieved');
  }
};

checkDistance = (locations) => {
  let check = true;
  if (locations.length === 0) {
    check = false;
  } else {
    locations.forEach((item) => {
      if (parseInt(item.distance) > 40) {
        check = false;
      } else {
        check = true;
      }
    });
  }
  return check;
};

CheckIn = async (uri, coordinates) => {
  Geocoder.from(coordinates.latitude, coordinates.longitude)
    .then((json) =>
      this.setState(
        {travelCheckInAddress: json.results[0].formatted_address},
        async () => {
          if (this.state.subCount > 0) {
            // console.log('==> I ran again', this.state.subCount);
          }

          let uploadData = new FormData();
          //console.log('Process time', new Date());
          uploadData.append('file', {
            uri: uri,
            name: 'photo.png',
            type: 'image/png',
          });
          //uploadData.append('file', uri);
          uploadData.append('StaffNo', this.state.StaffNo);
          uploadData.append('Latitude', Number(coordinates.latitude));
          uploadData.append('Longitude', Number(coordinates.longitude));
          uploadData.append('InstituteId', this.state.institute_id);
          uploadData.append('Accuracy', 10);
          //uploadData.append('Accuracy', Number(coordinates.accuracy));
          uploadData.append('IsTravelCheckIn', this.state.travelCheckIn);
          uploadData.append(
            'TravelCheckInAddress',
            this.state.travelCheckIn ? this.state.travelCheckInAddress : '',
          );
          console.log('uploadData', uploadData);
          const CancelToken = axios.CancelToken;
          const source = CancelToken.source();
          setTimeout(() => {
            source.cancel('Request cancel after timeout');
          }, 15000);
          try {
            const response = await axios({
              method: 'post',
              url: Const + 'api/Staff/CheckInWithFormFile',
              data: uploadData,
              headers: {'Content-Type': 'multipart/form-data'},
              timeout: 15000,

              cancelToken: source.token,
            });
            console.log('End time', new Date());
            const json = response.data;
            this.setState({showAlert2: false, showAlert: true});
            clearInterval(this.interval);
            Geolocation.clearWatch(this.watchID);
            console.log('JSON', json);
            const totalBeacons = json.geofencingClients.filter(
              (item, index) => item.uniqueId,
            );
            console.log('total_beacons', totalBeacons);
            const beacon = json.geofencingClients.find(
              (item, index) =>
                item.uniqueId === this.state.beaconID &&
                Math.abs(this.state.beaconDetails.rssi) <= 65,
            );

            if (beacon) {
              AsyncStorage.setItem('beacon', JSON.stringify(beacon));
            }
            console.log(
              'check',
              beacon,
              'totalBeacons_count',
              totalBeacons.length,
            );
            if (json.response.status) {
              this.setState({success: true});
              BluetoothStateManager.disable().then((result) => {
                // do something...
                console.log('blettoth', result);
              });
              if (this.state.travelCheckIn) {
                AsyncStorage.setItem(
                  'currentTravelLocation',
                  JSON.stringify(coordinates),
                );
                AsyncStorage.setItem('current_travel_checkin', 'running');
                AsyncStorage.setItem('geo_location', 'Travel Check In');
              }
              if (!this.state.travelCheckIn) {
                const geofence = json.geofencingClients.find(
                  (item) => item.distance === 0,
                );
                console.log('Checked_in_geofence ==>', geofence);
                AsyncStorage.setItem('geo_id', geofence.id.toString());
                AsyncStorage.setItem('geo_location', geofence.accessLocation);
                if (geofence.type == 'Circle') {
                  AsyncStorage.setItem(
                    'coordinates',
                    JSON.stringify(geofence.coordinates),
                  );
                  AsyncStorage.setItem('radius', geofence.radius.toString());
                  AsyncStorage.setItem('type', geofence.type.toString());
                  AsyncStorage.setItem(
                    'checkin_time',
                    geofence.CheckInTime ? geofence.CheckInTime : '',
                  );
                } else {
                  AsyncStorage.setItem(
                    'coordinates',
                    JSON.stringify(geofence.coordinates),
                  );
                  AsyncStorage.setItem(
                    'checkin_time',
                    geofence.CheckInTime ? geofence.CheckInTime : '',
                  );
                  AsyncStorage.setItem('type', geofence.type.toString());
                }
              }
              AsyncStorage.setItem('checked_out', 'no');
              AsyncStorage.setItem('isInside', 'yes');
              AsyncStorage.setItem('count', '0');
              AsyncStorage.setItem('gpsCount', '0');

              this.setState({
                showAlert: false,
                showAlert1: true,
                error_message: json.response.message,
              });

              KeepAwake.activate();

              // return setTimeout(() => {
              //   this.props.navigation.navigate('StaffTasks', {disable: true});
              // }, 5200);
            } else if (!json.response.status) {
              //this.enableBluetooth()
              this.setState({success: false});
              let isWithinBoundary = this.checkDistance(json.geofencingClients);
              console.log('boundary', isWithinBoundary);
              if (json.geofencingClients.length === 0) {
                this.setState({
                  showAlert: false,
                  showAlert1: true,
                  error_message:
                    'No assigned locations. Please contact your administrator for further details.',
                });
                return;
              }
              if (totalBeacons.length > 0) {
                if (
                  !beacon &&
                  json.geofencingClients.length > 0 &&
                  !this.state.travelCheckIn
                ) {
                  this.setState({
                    showAlert1: true,
                    showAlert: false,
                    error_message: `Either you are not in your assigned location or you are not close enough to the assigned beacon. Please move closer to the beacon location for successful check-in.`,
                  });
                  return setTimeout(
                    () => this.props.navigation.navigate('Home'),
                    5000,
                  );
                }
                if (
                  beacon &&
                  json.geofencingClients.length > 0 &&
                  !this.state.travelCheckIn
                ) {
                  let uploadData1 = new FormData();
                  console.log('Process time', new Date());
                  uploadData1.append('file', {
                    uri: uri,
                    name: 'photo.png',
                    type: 'image/png',
                  });
                  //uploadData1.append('file', uri);
                  uploadData1.append('StaffNo', this.state.StaffNo);
                  uploadData1.append('Latitude', Number(coordinates.latitude));
                  uploadData1.append(
                    'Longitude',
                    Number(coordinates.longitude),
                  );
                  uploadData1.append('InstituteId', this.state.institute_id);
                  uploadData1.append('Accuracy', Number(coordinates.accuracy));
                  uploadData1.append(
                    'IsTravelCheckIn',
                    this.state.travelCheckIn,
                  );
                  uploadData1.append('IsBeaconVerified', true);
                  uploadData1.append('AccessLocationId', beacon.id);
                  fetch(Const + 'api/Staff/CheckInWithFormFile', {
                    method: 'POST',
                    body: uploadData1,
                  })
                    .then((response) => response.json())
                    .then((json) => {
                      clearInterval(this.interval);
                      this.setState({success: true});
                      BluetoothStateManager.disable().then((result) => {
                        // do something...
                        console.log('blettoth', result);
                      });
                      Geolocation.clearWatch(this.watchID);
                      console.log('beacon_json', json);
                      AsyncStorage.setItem('checked_out', 'no');
                      AsyncStorage.setItem('isInside', 'yes');
                      if (this.state.travelCheckIn) {
                        AsyncStorage.setItem(
                          'current_travel_checkin',
                          'running',
                        );
                      }
                      this.setState({
                        showAlert: false,
                        showAlert1: true,
                        error_message: json.response.message,
                      });
                      // try {
                      if (beacon.type == 'Circle') {
                        AsyncStorage.setItem(
                          'coordinates',
                          JSON.stringify(beacon.coordinates),
                        );
                        AsyncStorage.setItem(
                          'radius',
                          beacon.radius.toString(),
                        );
                        AsyncStorage.setItem('type', beacon.type.toString());
                        AsyncStorage.setItem(
                          'checkin_time',
                          beacon.CheckInTime ? beacon.CheckInTime : '',
                        );
                      } else {
                        AsyncStorage.setItem(
                          'coordinates',
                          JSON.stringify(beacon.coordinates),
                        );
                        AsyncStorage.setItem(
                          'checkin_time',
                          beacon.CheckInTime ? beacon.CheckInTime : '',
                        );
                        AsyncStorage.setItem('type', beacon.type.toString());
                      }

                      return setTimeout(() => {
                        this.props.navigation.navigate('StaffTasks', {
                          disable: true,
                        });
                      }, 5200);
                      // } catch (e) {
                      //   console.log(e);
                      // }
                    })
                    .catch((error) => {
                      console.log('beacon_error', error);
                      this.setState({success: false});
                    });
                  return;
                }
              }
              if (
                this.state.coordinates.accuracy > 10 &&
                json.geofencingClients.length > 0 &&
                !this.state.travelCheckIn &&
                isWithinBoundary
              ) {
                this.setState({
                  showAlert: false,
                  modal: true,
                  count: 0,
                });
              } else if (
                this.state.coordinates.accuracy <= 10 &&
                json.geofencingClients.length > 0 &&
                isWithinBoundary &&
                !this.state.travelCheckIn
              ) {
                Geolocation.getCurrentPosition(
                  (position) => {
                    let coordinates = {
                      latitude: position.coords.latitude,
                      longitude: position.coords.longitude,
                      accuracy: position.coords.accuracy,
                    };
                    this.setState({count: 0});
                    if (this.state.subCount < 2) {
                      this.CheckIn(uri, coordinates);
                      console.log('==> I ran');
                    } else {
                      // this.setState({
                      //   showAlert: false,
                      //   showAlert1: true,
                      //   error_message:
                      //     'Please check-in from assigned location or retry again.',
                      // });
                      alert(
                        // 'Please check-in from assigned location or retry again.',
                        json.response.message,
                      );
                      setTimeout(
                        () => this.props.navigation.navigate('Home'),
                        1200,
                      );
                    }
                    setTimeout(
                      () => this.setState({subCount: this.state.subCount + 1}),
                      1200,
                    );
                  },
                  (e) => {
                    console.log(e);
                  },
                  {
                    enableHighAccuracy: true,
                    maximumAge: 0,
                  },
                );
              } else {
                this.setState({
                  showAlert1: true,
                  error_message: json.response.message,
                  showAlert: false,
                  count: 0,
                  success: false,
                });
              }
            }
          } catch (error) {
            clearInterval(this.interval);
            Geolocation.clearWatch(this.watchID);
            if (axios.isCancel(error)) {
              console.log('Request canceled', error.message);
              this.setState({
                showAlert: false,
                showAlert2: false,
                showAlert1: true,
                error_message:
                  'Request taking too long to respond. Refresh internet connection and retry.',
                count: 0,
                success: false,
              });
              return;
            }
            console.log(error);
            this.setState({
              showAlert: false,
              showAlert2: false,
              showAlert1: true,
              error_message:
                'Sorry! Unhandled exception occured while checking-in.',

              count: 0,
              success: false,
            });
          }
        },
      ),
    )
    .catch((e) => {
      console.log(e);
      alert('Unable to find location');
    });
  // console.log('uri==>', uri, coordinates, this.state.StaffNo);
};

//for check out old logic

getCurrentLocationWithAccuracy = (uri) => {
  // this.interval = setInterval(
  //   () => this.setState((prev) => ({count: prev.count + 1})),
  //   1000,
  // );
  let coordinatesTemp = {latitude: '', longitude: '', accuracy: ''};
  this.main(uri);
  // setTimeout(async () => {
  //   Geolocation.clearWatch(this.watchId);
  //   if (this.state.coordinates.accuracy > 10) {
  //     this.setState({showAlert2: false, showAlert: true});
  //     await this.checkout(uri, this.state.coordinates);
  //     return;
  //   } else {
  //     if (this.state.coordinates.accuracy === '') {
  //       this.setState({showAlert2: false, showAlert: true});
  //       Geolocation.getCurrentPosition(
  //         (pos) => this.checkout(uri, pos.coords),
  //         () => {},
  //         {enableHighAccuracy: true, maximumAge: 0, distanceFilter: 0},
  //       );
  //     }
  //     console.log('Accuracy achieved');
  //   }
  // }, 10000);
  this.watchId = Geolocation.watchPosition(
    async (position) => {
      console.log('[Matched]', position);
      //this.setState((prev) => ({count: prev.count + 1}));
      this.setState({
        coordinates: {
          latitude: position.coords.latitude,
          longitude: position.coords.longitude,
          accuracy: position.coords.accuracy,
        },
      });
      coordinatesTemp.latitude = position.coords.latitude;
      coordinatesTemp.longitude = position.coords.longitude;
      coordinatesTemp.accuracy = position.coords.accuracy;
      if (position.coords.accuracy <= 10) {
        Geolocation.clearWatch(this.watchId);
        await this.checkout(uri, coordinatesTemp);
        return;
      }
    },
    (e) => {
      //alert('no current location found');
      console.log(e.message);
    },

    {
      enableHighAccuracy: true,
      interval: 1000,
      fastestInterval: 1000,
      distanceFilter: 0,
    },
  );
};

wait = (ms) => new Promise((resolve, reject) => setTimeout(resolve, ms));

main = async (uri) => {
  await this.wait(10000);
  Geolocation.clearWatch(this.watchId);
  if (this.state.coordinates.accuracy > 10) {
    this.setState({showAlert2: false, showAlert: true});
    await this.checkout(uri, this.state.coordinates);
    return;
  } else {
    if (this.state.coordinates.accuracy === '') {
      this.setState({showAlert2: false, showAlert: true});
      Geolocation.getCurrentPosition(
        (pos) => this.checkout(uri, pos.coords),
        () => {},
        {enableHighAccuracy: true, maximumAge: 0, distanceFilter: 0},
      );
    }
    console.log('Accuracy achieved');
  }
};

checkout = async (uri, coordinates) => {
  let status = this.state.travelCheckOut;
  Geocoder.from(coordinates.latitude, coordinates.longitude)
    .then((json) =>
      this.setState(
        {travelCheckInAddress: json.results[0].formatted_address},
        async () => {
          console.log('travel_status', status);

          let uploadData = new FormData();
          uploadData.append('file', {
            uri: uri,
            name: 'photo.png',
            type: 'image/png',
          });
          uploadData.append('StaffNo', this.state.StaffNo);
          uploadData.append('Latitude', Number(coordinates.latitude));
          uploadData.append('Longitude', Number(coordinates.longitude));
          uploadData.append('InstituteId', this.state.institute_id);
          uploadData.append('Accuracy', Number(coordinates.accuracy));
          uploadData.append('IsTravelCheckIn', status);
          uploadData.append(
            'TravelCheckInAddress',
            status ? this.state.travelCheckInAddress : '',
          );
          const CancelToken = axios.CancelToken;
          const source = CancelToken.source();
          const config = {
            method: 'post',
            url: Const + 'api/Staff/CheckOutWithFormFile',
            headers: {
              'Content-Type': 'multipart/form-data',
            },
            data: uploadData,
            timeout: 15000,
            cancelToken: source.token,
          };
          setTimeout(() => {
            source.cancel('Request cancel after timeout');
          }, 15000);
          axios(config)
            .then((response) => response.data)

            // fetch(Const + 'api/Staff/CheckOutWithFormFile', {
            //   method: 'POST',
            //   body: uploadData,
            //   // isBeaconVerified: false,
            //   // AccessLocationId: null,
            // })
            //.then((response) => response.json())
            .then((json) => {
              this.setState({showAlert2: false, showAlert: true});
              clearInterval(this.interval);
              Geolocation.clearWatch(this.watchId);
              console.log('[JSON]==>', json);
              if (json.status) {
                this.setState({
                  showAlert: false,
                  showAlert1: true,
                  error_message: json.message,
                  count: 0,
                });
                if (this.state.isBluetoothOn) {
                  BluetoothStateManager.disable().then((result) => {
                    // do something...
                    console.log('blettoth', result);
                  });
                }
                AsyncStorage.setItem('checked_out', 'yes');
                if (status) {
                  AsyncStorage.setItem('current_travel_checkin', 'stopped');
                }
                try {
                  //AsyncStorage.setItem('coordinates', '');
                  //AsyncStorage.setItem('radius', '');
                  //AsyncStorage.setItem('type', '');
                  AsyncStorage.setItem('checkin_time', '');
                  AsyncStorage.setItem('beacon', '');
                  // AsyncStorage.setItem('geo_id', '');
                  // if (ReactNativeForegroundService.is_task_running('taskid')) {
                  //   ReactNativeForegroundService.remove_task('taskid');
                  // }
                  // Stoping Foreground service.
                  KeepAwake.deactivate();
                  //ReactNativeForegroundService.stop();

                  // return setTimeout(() => {
                  //   this.props.navigation.navigate('Home');
                  // }, 5200);
                } catch (e) {
                  console.log(e);
                }
              } else if (
                this.state.beaconDetails &&
                this.state.beaconDetails.uniqueId === this.state.beaconID &&
                this.state.rssi <= 65
              ) {
                let uploadData1 = new FormData();
                uploadData1.append('file', {
                  uri: uri,
                  name: 'photo.png',
                  type: 'image/png',
                });
                uploadData1.append('StaffNo', this.state.StaffNo);
                uploadData1.append('Latitude', Number(coordinates.latitude));
                uploadData1.append('Longitude', Number(coordinates.longitude));
                uploadData1.append('InstituteId', this.state.institute_id);
                uploadData1.append('Accuracy', Number(coordinates.accuracy));
                uploadData1.append('IsTravelCheckIn', status);
                uploadData1.append('IsBeaconVerified', true);
                uploadData1.append(
                  'AccessLocationId',
                  this.state.beaconDetails.id,
                );
                axios({
                  method: 'post',
                  url: Const + 'api/Staff/CheckOutWithFormFile',
                  data: uploadData1,
                  headers: {'Content-Type': 'multipart/form-data'},
                })
                  .then((response) => response.data)
                  .then((json) => {
                    clearInterval(this.interval);
                    Geolocation.clearWatch(this.watchId);
                    console.log('[JSON_BEACON]==>', json);
                    if (!json.status) {
                      this.setState({
                        showAlert1: true,
                        error_message: json.message,
                        showAlert: false,
                        count: 0,
                      });
                      return;
                    }
                    if (json.status) {
                      setTimeout(() => {
                        this.setState({
                          showAlert1: true,
                          error_message: json.message,
                          showAlert: false,
                          count: 0,
                        });
                        if (this.state.isBluetoothOn) {
                          BluetoothStateManager.disable().then((result) => {
                            // do something...
                            console.log('blettoth', result);
                          });
                        }
                        AsyncStorage.setItem('checked_out', 'yes');
                        if (status) {
                          AsyncStorage.setItem(
                            'current_travel_checkin',
                            'stopped',
                          );
                        }
                        try {
                          AsyncStorage.setItem('coordinates', '');
                          AsyncStorage.setItem('radius', '');
                          AsyncStorage.setItem('type', '');
                          AsyncStorage.setItem('checkin_time', '');
                          AsyncStorage.setItem('beacon', '');
                          AsyncStorage.setItem('geo_id', '');
                          return setTimeout(() => {
                            this.props.navigation.navigate('Home');
                          }, 5200);
                        } catch (e) {
                          console.log(e);
                        }
                      }, 1200);
                      return;
                    }
                  })
                  .catch((error) => {
                    return this.setState({
                      showAlert1: true,
                      error_message: `${error.message}`,
                      showAlert: false,
                      count: 0,
                    });
                  });
                return;
              }
              if (this.state.coordinates.accuracy > 10 && json.isOutSide) {
                this.setState({
                  showAlert: false,
                  modal: true,
                  count: 0,
                });
              } else {
                this.setState({
                  showAlert1: true,
                  error_message: json.message,
                  showAlert: false,
                  count: 0,
                });
                setTimeout(() => this.props.navigation.navigate('Home'), 1200);
              }
            })
            .catch((error) => {
              clearInterval(this.interval);
              Geolocation.clearWatch(this.watchId);
              console.log('error', error);
              if (axios.isCancel(error)) {
                console.log('Request canceled', error.message);
                this.setState({
                  showAlert: false,
                  showAlert2: false,
                  showAlert1: true,
                  error_message:
                    'Request taking too long to respond. Refresh internet connection and retry.',
                  count: 0,
                  success: false,
                });
                return;
              }
              this.setState({
                showAlert1: true,
                error_message:
                  'Sorry! Unhandled exception occured while checking-out.',
                showAlert: false,
                count: 0,
                showAlert2: false,
              });
            });
        },
      ),
    )
    .catch((e) => {
      console.log(e);
      this.setState({showAlert2: false, showAlert: false});
      alert('Unable to find location');
    });
};

//manager report location tracking

{
  /* {locations.length > 0 &&
                locations.length > 0 && (
                  <>
                    <Marker
                      key={locations[this.state.tripNoChosen].createdTime}
                      coordinate={locations[this.state.tripNoChosen].coordinates}
                      opacity={0.7}
                      //description={'Origin'}
                      title={moment
                        .utc(locations[this.state.tripNoChosen].createdTime)
                        .local()
                        .format('h:mm a')}>
                      <Icon
                        name={'map-marker'}
                        type={'material-community'}
                        size={30}
                        color={Colors.header}
                      />
                    </Marker>
                    <Marker
                      key={locations[locations.length - 1].createdTime}
                      coordinate={locations[locations.length - 1].coordinates}
                      opacity={0.7}
                      description={'Destination'}
                      title={moment
                        .utc(locations[locations.length - 1].createdTime)
                        .local()
                        .format('h:mm a')}>
                      <Icon
                        name={'map-marker'}
                        type={'material-community'}
                        size={30}
                        color={Colors.header}
                      />
                    </Marker>
                  </>
                )
                // data[this.state.tripNoChosen].map((loc, i, lloc) => {
                locations.map((loc, i, lloc) => {
                  return (
                    <Marker
                      key={loc.createdTime}
                      coordinate={loc.coordinates}
                      //opacity={0.7}
                      //description={'Location ' + (i + 1).toString()}
                      description={
                        i === 0
                          ? 'Starting point'
                          : i === lloc.length - 1
                          ? 'Latest/Last point'
                          : `Location ${i}`
                      }
                      title={moment(loc.capturedTime).format('h:mm a')}>
                      <Icon
                        name={
                          i === 0
                            ? ''
                            : i !== lloc.length - 1
                            ? 'map-marker'
                            : ''
                        }
                        type={'material-community'}
                        size={30}
                        color={Colors.header}
                      />
                      {i === 0 && (
                        <Image
                          source={{
                            uri: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ_8jXWxkA50-ColElavoSs1x8pLcz5m9fj2A&usqp=CAU',
                          }}
                          style={{
                            width: 60,
                            height: 60,
                            borderRadius: 60,
                            backgroundColor: 'transparent',
                            resizeMode: 'contain',
                          }}
                        />
                      )}
                      {i === lloc.length - 1 && (
                        <>
                          <Text
                            style={{
                              textTransform: 'capitalize',
                              fontWeight: 'bold',
                              color: Colors.maroon,
                            }}>
                            {name}
                          </Text>
                          <Avatar
                            rounded
                            size="medium"
                            avatarStyle={{
                              width: '100%',
                              height: '100%',
                              //borderRadius: 50,
                              resizeMode: 'stretch',
                            }}
                            // containerStyle={{
                            //   position: 'absolute',
                            //   top: 0,
                            //   right: 0,
                            //   zIndex: 1,
                            // }}
                            source={{
                              uri:
                                photo ||
                                'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQtOqCkEk1bHWlechHBJVOMBkfxoe9vXRO9SH0aTfy8mhNfXVH0DPk0Iu7LEYGg4YlIeAE&usqp=CAU',
                            }}
                          />
                        </>
                      )}
                    </Marker>
                  );
                })} */
}
