import React, {Component} from 'react';
import {
  View,
  Text,
  StyleSheet,
  Alert,
  NativeEventEmitter,
  NativeModules,
  ActivityIndicator,
} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {Container, Content} from 'native-base';

import Loader from '../common/Loader';
import Geolocation from 'react-native-geolocation-service';
import Const from '../common/Constants';
import AwesomeAlert from 'react-native-awesome-alerts';
import AsyncStorage from '@react-native-community/async-storage';

import CameraComp from '../common/CameraComp';
import BleManager from 'react-native-ble-manager';
import axios from 'axios';

import KeepAwake from 'react-native-keep-awake';
import ImageResizer from 'react-native-image-resizer';

import Geocoder from 'react-native-geocoding';
import Location from '../common/Location';
import {GOOGLE_MAPS_APIKEY} from '../../utils/configs/Constants';
import CustomModal from '../common/CustomModal';
import OTPInputView from '@twotalltotems/react-native-otp-input';
import {
  getAssignedLocations,
  staffCurrentLocation,
  validatePointWithinCircle,
} from '../../utils/helperFunctions';
import {CustomButton} from '../common/CustomButton';
import CustomLabel from '../common/CustomLabel';
import {Colors} from '../../utils/configs/Colors';
import LocationModal from '../common/LocationModal';
import FaceMismatchModal from '../common/FaceMismatchModal';
import GPSState from 'react-native-gps-state';
import moment from 'moment';
const BleManagerModule = NativeModules.BleManager;
const bleManagerEmitter = new NativeEventEmitter(BleManagerModule);
export default class Checkin extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loader: false,
      coordinates: {
        latitude: '',
        longitude: '',
        accuracy: '',
      },
      showAlert: false,
      StaffNo: '',
      bearer_token: '',
      institute_id: '',
      showAlert1: false,
      error_message: '',
      travel_check_in: this.props.route?.params?.travel || 'no',
      qrCheckin: this.props.route?.params?.qrCheckin || 'false',
      uri: '',
      travelCheckIn: this.props.route?.params?.travelCheckIn || false,
      selected: false,
      notInFence: false,
      modal: false,
      show: this.props.route?.params?.show || false,
      success: false,
      //show: false,
      showTimer: false,
      count: 0,
      subCount: 0,
      btnCount: 1,
      pressed: false,
      base64Data: null,
      isScanning: false,
      beaconID: '',
      beaconDetails: {},
      show1: false,
      show2: this.props.route?.params?.show2 || false,
      travelCheckInAddress: '',
      showAlert2: false,
      showMPINAlert: this.props.route?.params?.showMpinAlert || false,
      isTravelCheckinWithMpin:
        this.props.route?.params?.isTravelCheckinWithMpin || 'no',
      mpin: '',
      assignedLocations: [],
      geofence: {},
      faceMismatchModal: false,
      faceMismatchMessage: '',
      locationModal: false,
      profile_pic: '',
      capturedPhoto: {},
      progress: 0,
    };
    this.interval = 0;
    this.watchID = 0;
  }

  startScan = () => {
    if (!this.state.isScanning) {
      BleManager.scan([], 5, true)
        .then((results) => {
          console.log('Scanning...');
          this.setState({isScanning: true});
        })
        .catch((err) => {
          console.error(err);
        });
    }
  };

  openCamera1 = async (camera) => {
    const options1 = {quality: 1, base64: false};
    const body = new FormData();

    try {
      const image1 = await camera.takePictureAsync(options1);
      ImageResizer.createResizedImage(
        image1.uri,
        300,
        300,
        'PNG',
        0,
        0,
        undefined,
      )
        .then(async (response) => {
          console.log('size of image ===>', response.size);
          this.setState({showAlert: true});
          if (response.uri) {
            body.append('StaffNo', this.state.StaffNo);
            body.append('InstituteId', this.state.institute_id);
            body.append('File', {
              uri: response.uri,
              name: 'photo.png',
              type: 'image/png',
            });
            body.append('AccessLocationId', Number(this.state.data.locationId));
            body.append('QrCodeOwnerStaffCode', this.state.data.StaffNo);
            const response = await axios({
              method: 'POST',
              url: Const + 'api/staff/QrCheckIn',
              data: body,
              headers: {'Content-Type': 'multipart/form-data'},
            });
            this.setState({show1: false, showAlert: false});
            console.log('JSON ==>', response.data);
            const json = response.data;
            if (!json.response.status) {
              return alert(response.data.response.message);
            }
            //alert(response.data.response.message)
            const geofence = json.geofencingClients.find(
              (item) => item.distance === 0,
            );
            console.log('Checked_in_geofence ==>', geofence);

            AsyncStorage.setItem('checked_out', 'no');
            await AsyncStorage.setItem(
              'actualCheckInTime',
              moment().format('DD.MM.YYYY h:mm a'),
            );
            if (this.state.travelCheckIn) {
              AsyncStorage.setItem('current_travel_checkin', 'running');
            }

            if (Platform.OS === 'android') {
              this.setState({
                showAlert: false,
                showAlert1: true,
                error_message: json.response.message,
                count: 0,
              });
            } else {
              alert(json.response.message);
              this.setState({showAlert: false, count: 0});
            }
            AsyncStorage.setItem('geo_id', geofence.id.toString());
            if (geofence.type == 'Circle') {
              AsyncStorage.setItem(
                'coordinates',
                JSON.stringify(geofence.coordinates),
              );
              AsyncStorage.setItem('radius', geofence.radius.toString());
              AsyncStorage.setItem('type', geofence.type.toString());
              AsyncStorage.setItem(
                'checkin_time',
                geofence.CheckInTime ? geofence.CheckInTime : '',
              );
            } else {
              AsyncStorage.setItem(
                'coordinates',
                JSON.stringify(geofence.coordinates),
              );
              AsyncStorage.setItem(
                'checkin_time',
                geofence.CheckInTime ? geofence.CheckInTime : '',
              );
              AsyncStorage.setItem('type', geofence.type.toString());
            }
            // ReactNativeForegroundService.start({
            //   id: 144,
            //   title: 'Background Location Tracking',
            //   message: 'You are being tracked!',
            // });

            KeepAwake.activate();
            return setTimeout(() => {
              this.props.navigation.navigate('StaffTasks', {disable: true});
            }, 4000);
          }
          // response.uri is the URI of the new image that can now be displayed, uploaded...
          // response.path is the path of the new image
          // response.name is the name of the new image with the extension
          // response.size is the size of the new image
        })
        .catch((err) => {
          console.log('error in resizing', err);
          // Oops, something went wrong. Check that the filename is correct and
          // inspect err to get more details.
        });
    } catch (e) {
      console.log(e);
      alert(e.toString());
      this.setState({show1: false, showAlert: false});
    }
  };

  handleStopScan = () => {
    console.log('Scan is stopped');
    this.setState({isScanning: false});
  };

  handleDiscoverPeripheral = async (peripheral) => {
    console.log('peripheral', peripheral);
    if (!peripheral.name) {
      peripheral.name = 'unknown';
    }

    if (this.state.beaconDetails.uniqueId === peripheral.id) {
      this.setState({isScanning: false});
      alert(
        `Got beacon ${
          peripheral.id
        } with signal strength ${peripheral.rssi.toString()}`,
      );
      this.setState({beaconID: peripheral.id, beaconDetails: peripheral});
    }
  };

  async componentDidMount() {
    Geocoder.init(GOOGLE_MAPS_APIKEY);
    this.checkGPSStatus();
    const user_id = await AsyncStorage.getItem('user_id');
    const institute_id = await AsyncStorage.getItem('institute_id');
    const profile_pic = await AsyncStorage.getItem('profile_pic');
    const isTravelCheckinWithMpin = await AsyncStorage.getItem(
      'isTravelCheckinWithMpin',
    );

    const checkBeacon = await this.checkBeacon(user_id, institute_id);
    if (checkBeacon) {
      this.enableBluetooth();
    }
    bleManagerEmitter.addListener(
      'BleManagerDiscoverPeripheral',
      this.handleDiscoverPeripheral,
    );
    bleManagerEmitter.addListener('BleManagerStopScan', this.handleStopScan);

    const bearer_token = await AsyncStorage.getItem('bearer_token');
    const travel_check_in = await AsyncStorage.getItem('travel_check_in');
    const locations = JSON.parse(await AsyncStorage.getItem('locations'));
    //console.log('ttt', travel_check_in);
    this.setState({
      bearer_token,
      StaffNo: user_id,
      institute_id,
      travel_check_in,
      isTravelCheckinWithMpin,
      assignedLocations: locations,
      profile_pic,
    });
  }

  checkGPSStatus = async () => {
    const status = await GPSState.getStatus();
    return status;
  };

  checkBeacon = async (user_id, institute_id) => {
    //this.setState({showAlert: false});
    return fetch(
      Const + 'api/Staff/information/' + institute_id + '/' + user_id,
      {
        method: 'GET',
        headers: {
          Accept: 'text/plain',
          'content-type': 'application/json-patch+json',
        },
      },
    )
      .then((response) => response.json())
      .then((json) => {
        if (json.status) {
          const beacon = json.assignedLocations.find(
            (item) => item.uniqueId !== null && item.uniqueId !== '', //this is accepting travel check in also
          );
          if (beacon) {
            this.setState({beaconDetails: beacon});
            return true;
          }
          return false;
        }
        return false;
      })
      .catch((error) => {
        this.setState({showAlert: false});
        return false;
      });
  };

  componentWillUnmount() {
    clearInterval(this.interval);
    //clearInterval(this.interval2);
    Geolocation.clearWatch(this.watchID);
    bleManagerEmitter.removeListener(
      'BleManagerDiscoverPeripheral',
      this.handleDiscoverPeripheral,
    );
    bleManagerEmitter.removeListener('BleManagerStopScan', this.handleStopScan);
  }

  enableBluetooth = () => {
    BleManager.enableBluetooth()
      .then(() => {
        // Success code
        console.log('The bluetooth is already enabled or the user confirm');
        //this.startConnect();
        BleManager.start({showAlert: false});
        this.startScan();
      })
      .catch((error) => {
        //Failure code
        Alert.alert('Attention', 'Please turn on your bluetooth', [
          {
            text: 'OK',
            onPress: () => {
              BleManager.enableBluetooth();
            },
          },
        ]);
        return;
      });
  };

  openCamera = async (camera) => {
    const {geofence} = this.state;
    const options1 = {quality: 1, base64: false, width: 800};
    try {
      const image1 = await camera.takePictureAsync(options1);
      console.log(image1);
      if (!image1.uri) {
        return this.setState({
          showAlert1: true,
          error_message: 'Please re authenticate your face',
          showAlert: false,
          showTimer: false,
        });
      }

      ImageResizer.createResizedImage(
        image1.uri,
        300,
        300,
        'PNG',
        0,
        0,
        undefined,
      )
        .then(async (response) => {
          this.setState({
            show: false,
            //showAlert2: true,
            capturedPhoto: response,
            //error_message: 'Fetching location...',
            showAlert: false,
          });
          if (this.state.travelCheckIn) {
            this.setState({showAlert2: true});
            const options = {
              enableHighAccuracy: true,
              distanceFilter: 0,
              maximumAge: 0,
            };
            Geolocation.getCurrentPosition(
              (pos) => this.travelCheckInAPIHandler(response.uri, pos.coords),
              this.error,
              options,
            );

            return;
          }
          this.checkLocationHandler();
          // call new face validation API:
        })
        .catch((error) => console.log(error));
    } catch (e) {
      this.setState({
        showAlert: false,
        showAlert2: false,
        showAlert1: false,
      });
      alert('Error validating face:' + e + '. Please try again');
    }
  };

  checkInHandler = async () => {
    this.setState({showAlert2: true});
    const {geofence} = this.state;
    const url = `${Const}api/Staff/StaffFaceCheckIn`;
    let uploadData = new FormData();
    uploadData.append('StaffNo', this.state.StaffNo);
    uploadData.append('InstituteId', this.state.institute_id);
    uploadData.append('File', {
      uri: this.state.capturedPhoto.uri,
      name: 'photo.png',
      type: 'image/png',
    });
    uploadData.append('AccessLocationId', this.state.geofence.id);
    const body = uploadData;
    const apiResponse = await axios.post(url, body, {
      headers: {
        'Content-Type': 'multipart/form-data',
      },
    });
    const {data} = apiResponse;
    console.log('body ==>', uploadData);
    console.log('facerespinse ==>', data);
    if (!data.status && !data.message.includes('Unhandled')) {
      return this.setState({
        showAlert2: false,
        faceMismatchModal: true,
        faceMismatchMessage: data.message,
      });
    }

    if (data.status) {
      const json = await staffCurrentLocation(
        this.state.bearer_token,
        this.state.institute_id,
        this.state.StaffNo,
        geofence.coordinates[0],
      );
      await AsyncStorage.setItem('checked_out', 'no');
      await AsyncStorage.setItem(
        'actualCheckInTime',
        moment().format('DD.MM.YYYY h:mm a'),
      );
      await AsyncStorage.setItem('count', '0');
      await AsyncStorage.setItem('gpsCount', '0');
      await AsyncStorage.setItem('geo_id', geofence.id.toString());
      await AsyncStorage.setItem('geo_location', geofence.accessLocation);
      if (geofence.type == 'Circle') {
        await AsyncStorage.setItem(
          'coordinates',
          JSON.stringify(geofence.coordinates),
        );
        await AsyncStorage.setItem('radius', geofence.radius.toString());
        await AsyncStorage.setItem('type', geofence.type.toString());
        await AsyncStorage.setItem(
          'checkin_time',
          geofence.CheckInTime ? geofence.CheckInTime : '',
        );
      } else {
        await AsyncStorage.setItem(
          'coordinates',
          JSON.stringify(geofence.coordinates),
        );
        await AsyncStorage.setItem(
          'checkin_time',
          geofence.CheckInTime ? geofence.CheckInTime : '',
        );
        await AsyncStorage.setItem('type', geofence.type.toString());
      }
    }

    this.setState({
      showAlert: false,
      showAlert2: false,
      showAlert1: true,
      error_message: data.message,
    });
  };

  travelCheckInAPIHandler = async (uri, coordinates) => {
    const assignedLocations = await getAssignedLocations();

    const findTravelGeoFence = assignedLocations.find(
      (each) => each.accessLocation === 'Travel Check In',
    );
    const AccessLocationId = findTravelGeoFence.id;
    // const AccessLocationId = await AsyncStorage.getItem(
    //   'travelCheckinLocationId',
    // );
    console.log('travelCheckinLocationId', AccessLocationId);

    this.setState({coordinates});
    const {latitude, longitude, accuracy} = coordinates;
    Geocoder.from(latitude, longitude).then((json) => {
      const {results} = json;
      const filteredResults = results.filter(
        (e) => !e.formatted_address.includes('+'),
      );
      const filteredLocations = assignedLocations.filter(
        (e) => e.type !== 'Anonymous',
      );

      let count = 0;
      if (filteredLocations.length > 0) {
        filteredLocations.forEach((e) => {
          const check = validatePointWithinCircle(e, coordinates);
          console.log('check', check);
          if (check) {
            return this.setState(
              {travelCheckInAddress: e.accessLocation},
              //  () =>
              // this.checkIn(
              //   uri,
              //   latitude,
              //   longitude,
              //   accuracy,
              //   AccessLocationId,
              //   coordinates,
              // ),
            );
          }
          count += 1;
        });
      }
      if (count === filteredLocations.length) {
        this.setState(
          {travelCheckInAddress: filteredResults[0].formatted_address},
          () =>
            this.checkIn(
              uri,
              latitude,
              longitude,
              accuracy,
              AccessLocationId,
              coordinates,
            ),
        );
      } else {
        this.checkIn(
          uri,
          latitude,
          longitude,
          accuracy,
          AccessLocationId,
          coordinates,
        );
      }
      // if (!check) {
      //   this.setState(
      //     {travelCheckInAddress: filteredResults[0].formatted_address},
      //     () =>
      //       this.checkIn(
      //         uri,
      //         latitude,
      //         longitude,
      //         accuracy,
      //         AccessLocationId,
      //         coordinates,
      //       ),
      //   );
      // }
    });
  };

  checkIn = async (
    uri,
    latitude,
    longitude,
    accuracy,
    AccessLocationId,
    coordinates,
  ) => {
    let uploadData = new FormData();
    //console.log('Process time', new Date());
    uploadData.append('file', {
      uri: uri,
      name: 'photo.png',
      type: 'image/png',
    });
    //uploadData.append('file', uri);
    uploadData.append('StaffNo', this.state.StaffNo);
    uploadData.append('Latitude', Number(latitude));
    uploadData.append('Longitude', Number(longitude));
    uploadData.append('InstituteId', this.state.institute_id);
    //uploadData.append('Accuracy', 10);
    uploadData.append('Accuracy', Number(accuracy));
    uploadData.append('IsTravelCheckIn', true);
    uploadData.append('AccessLocationId', Number(AccessLocationId));
    uploadData.append('TravelCheckInAddress', this.state.travelCheckInAddress);
    console.log('uploadData', uploadData);
    const CancelToken = axios.CancelToken;
    const source = CancelToken.source();
    setTimeout(() => {
      source.cancel('Request cancel after timeout');
    }, 30000);
    try {
      const response = await axios({
        method: 'post',
        url: Const + 'api/Staff/CheckInWithFormFile',
        data: uploadData,
        headers: {'Content-Type': 'multipart/form-data'},
        timeout: 30000,

        cancelToken: source.token,
      });
      console.log('End time', new Date());

      const {data} = response;
      console.log('data==', data);
      if (!data.response.status) {
        this.setState({
          showAlert: false,
          showAlert1: true,
          showAlert2: false,
          progress: 0,
          error_message: data.response.message,
        });
        return;
      }

      const json = await staffCurrentLocation(
        this.state.bearer_token,
        this.state.institute_id,
        this.state.StaffNo,
        coordinates,
      );
      await AsyncStorage.setItem('current_travel_checkin', 'running');
      await AsyncStorage.setItem('checked_out', 'no');

      await AsyncStorage.setItem(
        'actualCheckInTime',
        moment().format('DD.MM.YYYY h:mm a'),
      );
      await AsyncStorage.setItem('geo_location', 'Travel Check In');
      await AsyncStorage.setItem(
        'currentTravelLocation',
        JSON.stringify(coordinates),
      );
      this.setState({
        showAlert: false,
        showAlert1: true,
        showAlert2: false,
        progress: 0,
        error_message: data.response.message,
      });
    } catch (error) {
      if (axios.isCancel(error)) {
        console.log('Request canceled', error.message);
        this.setState({
          showAlert: false,
          showAlert2: false,
          showAlert1: true,
          error_message:
            'Request taking too long to respond. Refresh internet connection and retry.',
          progress: 0,
          success: false,
        });
        return;
      }
      console.log(error);
      this.setState({
        showAlert: false,
        showAlert2: false,
        showAlert1: true,
        error_message: 'Sorry! Unhandled exception occured while checking-in.',

        progress: 0,
        success: false,
      });

      //alert('Error occured while doing travel check-in: ' + e);
    }
  };

  CheckinWithBarCode = async ({data}) => {
    console.log('data', data);
    let parsedDate = JSON.parse(data);
    let now1 = new Date().valueOf();
    let then1 = new Date(parsedDate.timeStamp).valueOf();
    const difference = (now1 - then1) / 1000;
    console.log({
      now: now1,
      then1,
      difference,
    });
    if (difference > 20) {
      this.setState({show2: false}, () => {
        return alert('Time exceeded for check in');
      });
      return;
    }

    this.setState({
      data: parsedDate,
      show2: false,
      show1: true,
    });
  };

  goToHome = () => {
    this.props.navigation.navigate('Home');
  };

  goToStaffTasks = () => {
    this.props.navigation.navigate('StaffTasks', {disable: true});
  };

  launchTipsModal = () => {
    this.setState({modal: true, notInFence: false});
  };

  goTo = () => {
    if (this.state.success) {
      this.props.navigation.navigate('StaffTasks', {disable: true});
    } else {
      this.props.navigation.navigate('Home');
    }
  };

  pinHandler = async () => {
    const url = `${Const}api/Staff/TravelCheckInWithStaffCode`;
    this.setState({showAlert: true});
    Geolocation.getCurrentPosition(
      async (pos) => {
        try {
          const assignedLocations = await getAssignedLocations();

          const findTravelGeoFence = assignedLocations.find(
            (each) => each.accessLocation === 'Travel Check IN with Staff Code',
          );
          if (!findTravelGeoFence) {
            alert(
              'You are not assigned travel with mpin check in. Please logout and login again.',
            );
            return this.goToHome();
          }

          const geoFenceId = findTravelGeoFence?.id || 0;
          const shiftId = findTravelGeoFence?.shiftId || 0;
          const body = {
            geoFenceId,
            staffCode: this.state.StaffNo,
            instituteId: this.state.institute_id,
            checkinCoordinates: `${pos.coords.latitude},${pos.coords.longitude}`,
            shiftId,
            mpin: this.state.mpin,
          };
          console.log('body', body);
          const response = await axios.post(url, body);
          const {data} = response;

          if (data.status) {
            await AsyncStorage.setItem('checked_out', 'no');
            await AsyncStorage.setItem(
              'actualCheckInTime',
              moment().format('DD.MM.YYYY h:mm a'),
            );
            await AsyncStorage.setItem('current_travel_checkin', 'running');
          }

          this.setState({
            showAlert: false,
            showMPINAlert: false,
            showAlert1: true,
            error_message: data.message,
          });
        } catch (e) {
          this.setState({
            showAlert: false,
            showMPINAlert: false,
          });
          alert('Error checking in with mpin: ' + e);
          this.goToHome();
        }
      },
      this.error,
      {
        enableHighAccuracy: true,
        distanceFilter: 0,
        maximumAge: 0,
      },
    );
  };

  success = (position) => this.setState({coordinates: position.coords});

  error = (error) =>
    alert(
      'Error retreiving location. Check your location services.' +
        JSON.stringify(error.message),
    );

  checkLocationHandler = async () => {
    try {
      const assignedLocationsAll = await getAssignedLocations();
      const assignedLocations = assignedLocationsAll.filter(
        (each) => each.type === 'Circle',
      );
      this.setState({loaderModal: true});
      const options = {
        enableHighAccuracy: true,
        distanceFilter: 0,
        interval: 1000,
        fastestInterval: 1000,
      };
      this.watchID = Geolocation.watchPosition(
        this.success,
        this.error,
        options,
      );
      this.main(assignedLocations);
    } catch (e) {
      alert('Error fetching assigned locations: ' + e);
    }
  };

  wait = (ms) => new Promise((resolve, reject) => setTimeout(resolve, ms));

  main = async (assignedLocations) => {
    await this.wait(10000);
    Geolocation.clearWatch(this.watchID);
    clearInterval(this.interval);
    this.setState({loaderModal: false, progress: 0});
    if (assignedLocations?.length === 0) {
      alert(
        'No locations assigned. Please contact your administrator for further details.',
      );
      return this.goToHome();
    }
    let count = 0;
    assignedLocations.forEach((location) => {
      const point = validatePointWithinCircle(location, this.state.coordinates);
      if (point) {
        return this.setState({
          // show: true,
          geofence: location,
        });
      }
      count += 1;
    });
    console.log(count);
    if (count === assignedLocations.length) {
      return this.setState({locationModal: true});
    }
    // return this.setState({show: true});
    this.checkInHandler();
  };
  render() {
    if (this.state.loader) {
      return <Loader />;
    }

    return (
      <View style={styles.container}>
        <AwesomeAlert
          show={this.state.showAlert}
          showProgress={true}
          title="Loading"
          closeOnTouchOutside={false}
          closeOnHardwareBackPress={false}
        />
        <CustomModal isVisible={this.state.loaderModal} doNotShowDeleteIcon>
          <View style={{justifyContent: 'center', alignItems: 'center'}}>
            <ActivityIndicator color={Colors.header} size={'large'} />
            {/* <ProgressCircle
              percent={Number(this.state.progress) * 10}
              radius={30}
              borderWidth={5}
              color="#f05760"
              shadowColor="#999"
              bgColor="#fff">
              <Text>{(this.state.progress * 10).toString()}</Text>
            </ProgressCircle> */}
          </View>

          <CustomLabel
            title={
              'Please wait... Checking whether you are in your assigned location or not...'
            }
            labelStyle={{textAlign: 'center'}}
          />
        </CustomModal>
        {this.state.showAlert2 && (
          <Location
            accuracy={Number(this.state.coordinates.accuracy).toFixed(2)}
            latitude={Number(this.state.coordinates.latitude)}
            longitude={Number(this.state.coordinates.longitude)}
            isVisible={this.state.showAlert2}
            text={'Please wait... Trying to check-in...'}
          />
        )}

        {this.state.locationModal && (
          <LocationModal
            isVisible={this.state.locationModal}
            deleteIconPress={() =>
              this.setState({locationModal: false}, () => this.goToHome())
            }
            title={
              'Your GPS accuracy: ' +
              Number(this.state.coordinates.accuracy).toFixed()
            }
            subTitle={'The recommended GPS accuracy is 10 m or less.'}
            latitude={this.state.coordinates.latitude || 12.3788}
            longitude={this.state.coordinates.longitude || 74.444}
            photo={this.state.profile_pic}
            // onPressRetry={() => {
            //   this.setState({locationModal: false});
            //   this.checkLocationHandler();
            // }}
            //radius={this.state.coordinates.accuracy || 25}
            assignedLocations={this.state.assignedLocations || []}
          />
        )}
        {this.state.faceMismatchMessage !== '' &&
          !this.state.faceMismatchMessage.includes('Unhandled') && (
            <FaceMismatchModal
              isVisible={this.state.faceMismatchModal}
              title={'Face Mismatch'}
              profilePic={
                this.state.profile_pic ||
                'https://upload.wikimedia.org/wikipedia/commons/9/99/Sample_User_Icon.png'
              }
              capturedPhoto={
                this.state.capturedPhoto?.uri ||
                'https://upload.wikimedia.org/wikipedia/commons/9/99/Sample_User_Icon.png'
              }
              errorMsg={
                this.state.faceMismatchMessage.includes('register')
                  ? this.state.faceMismatchMessage +
                    '. Re-register your face again'
                  : this.state.faceMismatchMessage +
                      '. Adjust your camera showing the face clearly or re-register your face again.' ||
                    'No error'
              }
              registerScreen={() => {
                this.setState({faceMismatchModal: false}, () =>
                  this.props.navigation.navigate('FaceRegister', {show: true}),
                );
              }}
              deleteIconPress={() => this.setState({faceMismatchModal: false})}
            />
          )}

        <AwesomeAlert
          show={this.state.showAlert1}
          showProgress={
            this.state.error_message === 'Fetching location...' ? true : false
          }
          title={
            this.state.error_message === 'Fetching location...'
              ? this.state.count.toString()
              : 'Attention'
          }
          //title={'Attention'}
          message={this.state.error_message}
          closeOnTouchOutside={false}
          closeOnHardwareBackPress={false}
          showCancelButton={
            this.state.error_message === 'Fetching location...' ? false : true
          }
          cancelText="Okay"
          onCancelPressed={() => {
            this.setState({showAlert1: false, count: 0}, () => this.goTo());
            if (
              this.state.error_message ===
              'Either you are not in your assigned location or you are not close enough to the assigned beacon. Please move closer to the beacon location for successful check-in.'
            ) {
              return this.props.navigation.navigate('Home');
            }
          }}
          cancelButtonColor="#f05760"
          cancelButtonTextStyle={{fontFamily: 'Poppins-Regular', fontSize: 13}}
          messageStyle={{fontFamily: 'Poppins-Regular', fontSize: 13}}
          titleStyle={{fontFamily: 'Poppins-SemiBold', fontSize: 14}}
        />
        <Container>
          {/* <SubHeader
            title="Check In"
            showBack={true}
            backScreen="Home"
            showBell={false}
            navigation={this.props.navigation}
          /> */}

          <Content>
            {/* {this.state.travel_check_in == 'yes' ? ( */}
            {/* <View style={{marginTop: 20, marginLeft: 15, marginRight: 15}}>
              <Text style={styles.heading}>
                Please select your Check IN type
              </Text>
              <View style={styles.center}>
                <TouchableWithoutFeedback
                  onPress={() =>
                    this.checkGPSStatus().then((status) => {
                      console.log('SSTTTS', status);
                      if (
                        status === GPSState.DENIED ||
                        status === GPSState.NOT_DETERMINED ||
                        status === GPSState.RESTRICTED
                      ) {
                        return Alert.alert(
                          'Refresh GPS services',
                          'It is recommended that you refresh your GPS services for proper check-in',
                          [
                            {
                              text: 'Open Settings',
                              onPress: () => GPSState.openLocationSettings(),
                            },
                          ],
                          {cancelable: false},
                        );
                      }
                      this.setState({show: true});
                      //this.checkLocationHandler();
                    })
                  }>
                  <Card style={styles.mainCardStyle}>
                    <Text style={styles.checkinText}>Check IN</Text>
                  </Card>
                </TouchableWithoutFeedback>
                {this.state.travel_check_in == 'yes' && (
                  <TouchableWithoutFeedback
                    onPress={() =>
                      this.setState({travelCheckIn: true, show: true})
                    }>
                    <Card style={styles.mainCardStyle}>
                      <Text style={styles.checkinText}>Travel Check IN</Text>
                    </Card>
                  </TouchableWithoutFeedback>
                )}
                {this.state.isTravelCheckinWithMpin == 'yes' && (
                  <TouchableWithoutFeedback
                    onPress={() => this.setState({showMPINAlert: true})}>
                    <Card style={styles.mainCardStyle}>
                      <Text style={styles.checkinText}>
                        MPIN Travel Check IN
                      </Text>
                    </Card>
                  </TouchableWithoutFeedback>
                )}
                {this.state.qrCheckin === 'true' && (
                  <TouchableWithoutFeedback
                    onPress={() =>
                      this.setState({qrCodeScanner: true, show2: true})
                    }>
                    <Card style={styles.mainCardStyle}>
                      <Text style={styles.checkinText}>QR Code Check IN</Text>
                    </Card>
                  </TouchableWithoutFeedback>
                )}
              </View>
            </View> */}
            {this.state.showMPINAlert && (
              <CustomModal
                isVisible={this.state.showMPINAlert}
                deleteIconPress={() =>
                  this.setState({showMPINAlert: false}, () => this.goToHome())
                }>
                <Text>Enter MPIN to verify</Text>
                <OTPInputView
                  style={{width: wp('80'), height: 80}}
                  pinCount={4}
                  autoFocusOnLoad={true}
                  secureTextEntry={true}
                  codeInputFieldStyle={styles.inputFeilds}
                  codeInputHighlightStyle={styles.inputFeildsFocus}
                  onCodeFilled={(text) => this.setState({mpin: text})}
                />

                <CustomButton
                  title={'Verify'}
                  color={Colors.header}
                  onPress={this.pinHandler}
                />
              </CustomModal>
            )}

            {this.state.show && (
              <CameraComp
                visible={this.state.show}
                onClose={() =>
                  this.setState({show: false}, () => this.goToHome())
                }
                onPress={(camera) => this.openCamera(camera)}
                btnText={
                  !this.state.travelCheckIn ? 'check-in' : 'travel check-in'
                }
              />
            )}
            {this.state.show2 && (
              <CameraComp
                visible={this.state.show2}
                onClose={() =>
                  this.setState({show2: false}, () => this.goToHome())
                }
                pressed
                onPress={(camera) => {}}
                btnText={'QR Code Scanner'}
                onBarCodeRead={this.CheckinWithBarCode}
                //type = {RNCamera.Constants.Type.back}
              />
            )}

            {this.state.show1 && (
              <CameraComp
                visible={this.state.show1}
                onClose={() =>
                  this.setState({show1: false}, () => this.goToHome())
                }
                onPress={(camera) => this.openCamera1(camera)}
                btnText={'qr check-in'}
              />
            )}

            {/* {this.state.modal && (
              <ModalForAccuracy
                visible={this.state.modal}
                onClose={() => this.setState({modal: false})}
                onPress={() => this.props.navigation.goBack()}
                accuracy={Math.round(
                  Number(this.state.coordinates.accuracy),
                ).toString()}
                checkin
              />
            )} */}

            {/* <Button title="Get ccurrent location" onPress={this.CheckIn} /> */}
          </Content>
        </Container>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fbfbfb',
  },
  header: {
    backgroundColor: '#089bf9',
  },
  title: {
    fontFamily: 'Poppins-SemiBold',
    fontSize: 17,
    color: '#ffffff',
  },
  cardContainer: {
    marginTop: '10%',
    marginLeft: '6%',
    marginRight: '6%',
    justifyContent: 'center',
  },
  label: {
    fontFamily: 'Poppins-Regular',
    fontSize: 14,
    color: '#c9c3c5',
  },
  label1: {
    fontFamily: 'Poppins-Regular',
    fontSize: 12,
    color: '#c9c3c5',
    paddingLeft: wp('3'),
  },
  labelContainer: {
    margin: '1.5%',
  },
  input: {
    fontFamily: 'Poppins-Regular',
    fontSize: 13,
    backgroundColor: '#f1f1f1',
    paddingLeft: '5%',
    borderRadius: 10,
    height: hp('7'),
  },
  item: {
    borderRadius: 10,
    backgroundColor: '#f1f1f1',
    borderLeftWidth: 0,
    borderRightWidth: 0,
    borderTopWidth: 0,
    borderBottomWidth: 0,
    height: hp('7'),
  },
  card: {
    borderRadius: 10,
    elevation: 4,
    paddingTop: '3%',
    paddingBottom: '3%',
    height: hp('65'),
  },
  preview: {
    flex: 1,
  },
  buttonContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
    backgroundColor: '#f05760',
    borderRadius: 20,
    width: wp('35'),
    paddingRight: wp('7'),
    marginTop: '4%',
  },
  buttonContainer1: {
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
    backgroundColor: '#f05760',
    borderRadius: 20,
    width: wp('40'),
    marginTop: '4%',
  },
  buttonText: {
    fontFamily: 'Poppins-Regular',
    fontSize: 12,
    color: '#ffffff',
  },
  buttonText: {
    fontFamily: 'Poppins-Regular',
    fontSize: 12,
    color: '#ffffff',
    paddingRight: '3%',
  },
  btImage: {
    width: 50,
    height: 39,
    borderRadius: 20,
  },
  userImage: {
    width: 120,
    height: 120,
  },
  center: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  note: {
    fontFamily: 'Poppins-SemiBold',
    fontSize: 13,
    marginBottom: 10,
  },
  mainCardStyle: {
    borderRadius: 10,
    elevation: 4,
    padding: 20,
    width: wp('85'),
  },
  checkinText: {
    fontFamily: 'Poppins-Regular',
    fontSize: 13,
  },
  heading: {
    fontFamily: 'Poppins-SemiBold',
    margin: 12,
  },
  inputFeilds: {
    backgroundColor: '#ffffff',
    borderWidth: 1,
    color: '#000000',
    borderRadius: 10,
  },
  inputFeildsFocus: {
    backgroundColor: '#ffffff',
    borderWidth: 2,
    borderColor: '#f05760',
    color: '#000000',
    borderRadius: 10,
  },
});
