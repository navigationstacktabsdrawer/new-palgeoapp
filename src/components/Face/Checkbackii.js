let uploadData = new FormData();
            uploadData.append('file', {uri: uri,name: "photo.png",type: 'image/png'});
            uploadData.append('StaffNo',this.state.StaffNo);
            uploadData.append('Latitude',coordinates.latitude);
            uploadData.append('Longitude',coordinates.longitude);
            uploadData.append('InstituteId',this.state.institute_id);
            uploadData.append('Accuracy',parseInt(position.coords.accuracy));
            uploadData.append('IsTravelCheckIn',status);
fetch(Const+'api/Staff/CheckInWithFormFile', { 
    method: 'POST',
    body: uploadData
    }).then((response) => response.json())
    .then((json) => {
        if(json.response.status){
            AsyncStorage.setItem('checked_out','no');
            if(status){
                AsyncStorage.setItem('current_travel_checkin','running');
            }
            this.setState({showAlert1:true,error_message:'Checked in successfully',showAlert : false});
            try{
                if(json.geofencingClients[0].type == "Circle"){
                    AsyncStorage.setItem('coordinates', JSON.stringify(json.geofencingClients[0].coordinates));
                    AsyncStorage.setItem('radius',json.geofencingClients[0].radius.toString());
                    AsyncStorage.setItem('type',json.geofencingClients[0].type.toString());
                    AsyncStorage.setItem('checkin_time',json.geofencingClients[0].CheckInTime?json.geofencingClients[0].CheckInTime:'');
                }else{
                    AsyncStorage.setItem('coordinates',JSON.stringify(json.geofencingClients[0].coordinates));
                    AsyncStorage.setItem('checkin_time',json.geofencingClients[0].CheckInTime?json.geofencingClients[0].CheckInTime:'');
                    AsyncStorage.setItem('type',json.geofencingClients[0].type.toString());
                }
                setTimeout(()=>{this.props.navigation.navigate('StaffTasks',{disable:true})},1200);
            }catch(e){
                console.log(e)
            } 
        }else if(!json.response.status){
            this.setState({showAlert1:true,error_message:json.response.message,showAlert : false});
        }
    }).catch((error) => {
        this.setState({showAlert1:true,error_message:'Unknown error occured',showAlert : false});
    });