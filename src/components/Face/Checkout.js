import React, {Component} from 'react';
import {
  View,
  Text,
  StyleSheet,
  TouchableWithoutFeedback,
  Image,
  Alert,
  NativeModules,
  NativeEventEmitter,
  ActivityIndicator,
} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {Container, Card, Content} from 'native-base';
import SubHeader from '../common/SubHeader';
import Loader from '../common/Loader';
import Geolocation from 'react-native-geolocation-service';
import Const from '../common/Constants';
import AwesomeAlert from 'react-native-awesome-alerts';
import AsyncStorage from '@react-native-community/async-storage';
import ImagePicker from 'react-native-image-crop-picker';
import ModalForAccuracy from '../common/Modal';
import CameraComp from '../common/CameraComp';
import BleManager from 'react-native-ble-manager';
import axios from 'axios';
//import Boundary, {Events} from 'react-native-boundary';
const BleManagerModule = NativeModules.BleManager;
const bleManagerEmitter = new NativeEventEmitter(BleManagerModule);
import BluetoothStateManager from 'react-native-bluetooth-state-manager';
import ReactNativeForegroundService from '@supersami/rn-foreground-service';
import KeepAwake from 'react-native-keep-awake';
import ImageResizer from 'react-native-image-resizer';
import Geocoder from 'react-native-geocoding';
import Location from '../common/Location';
import {GOOGLE_MAPS_APIKEY} from '../../utils/configs/Constants';
import {
  getAssignedLocations,
  staffCurrentLocation,
  validatePointWithinCircle,
} from '../../utils/helperFunctions';
import CustomModal from '../common/CustomModal';
import OTPInputView from '@twotalltotems/react-native-otp-input';
import {CustomButton} from '../common/CustomButton';
import {Colors} from '../../utils/configs/Colors';
import CustomLabel from '../common/CustomLabel';
import ProgressCircle from 'react-native-progress-circle';
import LocationModal from '../common/LocationModal';
import FaceMismatchModal from '../common/FaceMismatchModal';
import GPSState from 'react-native-gps-state';

export default class Checkout extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loader: false,
      coordinates: {
        latitude: '',
        longitude: '',
        accuracy: '',
      },
      showAlert: false,
      StaffNo: '',
      bearer_token: '',
      institute_id: '',
      showAlert1: false,
      error_message: '',
      travel_check_in: this.props.route?.params?.travel_check_in || 'no',
      qrCheckout: this.props.route?.params?.qrCheckin || 'false',
      notInFence: false,
      modal: false,
      //show: false,
      show: this.props.route?.params?.show || false,
      count: 0,
      btnCount: 1,
      pressed: false,
      base64Data: null,
      isScanning: false,
      beaconID: '',
      beaconDetails: {},
      rssi: null,
      isBluetoothOn: false,
      travelCheckOut: this.props.route?.params?.travelCheckOut || false,
      current_travel_check_in: '',
      showAlert2: false,
      showMPINAlert: this.props.route?.params?.showMpinAlert || false,
      isTravelCheckinWithMpin:
        this.props.route?.params?.isTravelCheckinWithMpin || 'no',
      mpin: '',
      assignedLocations: [],
      geofence: {},
      faceMismatchModal: false,
      faceMismatchMessage: '',
      locationModal: false,
      profile_pic: '',
      capturedPhoto: {},
      progress: 0,
      show2: this.props.route?.params?.show2 || false,
    };
    this.interval = 0;
    this.watchID = 0;
  }
  async componentDidMount() {
    Geocoder.init(GOOGLE_MAPS_APIKEY);
    this.checkGPSStatus();
    const beacon = await AsyncStorage.getItem('beacon');
    const current_travel_check_in = await AsyncStorage.getItem(
      'current_travel_checkin',
    );
    const isTravelCheckinWithMpin = await AsyncStorage.getItem(
      'isTravelCheckinWithMpin',
    );
    const locations = JSON.parse(await AsyncStorage.getItem('locations'));
    // this.setState({
    //   current_travel_check_in,
    //   isTravelCheckinWithMpin,
    //   assignedLocations: locations,
    // });
    console.log('f', beacon);
    if (beacon) {
      this.setState({beaconDetails: JSON.parse(beacon), isBluetoothOn: true});
      this.enableBluetooth();
    }

    bleManagerEmitter.addListener(
      'BleManagerDiscoverPeripheral',
      this.handleDiscoverPeripheral,
    );
    bleManagerEmitter.addListener('BleManagerStopScan', this.handleStopScan);

    AsyncStorage.getItem('user_id').then((user_id) => {
      AsyncStorage.getItem('profile_pic').then((profile_pic) => {
        AsyncStorage.getItem('bearer_token').then((bearer_token) => {
          AsyncStorage.getItem('institute_id').then((institute_id) => {
            this.setState({
              StaffNo: user_id,
              bearer_token: bearer_token,
              institute_id: institute_id,
              //travel_check_in: travel_check_in,
              profile_pic,
              current_travel_check_in,
              isTravelCheckinWithMpin,
              assignedLocations: locations,
            });
          });
        });
      });
    });
  }

  checkGPSStatus = async () => {
    const status = await GPSState.getStatus();
    // console.log('GPS_STATUS ==>', status);
    // if (
    //   status === GPSState.DENIED ||
    //   status === GPSState.NOT_DETERMINED ||
    //   status === GPSState.RESTRICTED
    // ) {
    //   Alert.alert(
    //     'Refresh GPS services',
    //     'It is recommended that you refresh your GPS services. Do you want to open location settings?',
    //     [
    //       {
    //         text: 'Open Settings',
    //         onPress: () => GPSState.openLocationSettings(),
    //       },
    //     ],
    //     {cancelable: false},
    //   );
    // }
    return status;
  };
  startScan = () => {
    if (!this.state.isScanning) {
      BleManager.scan([], 5, true)
        .then((results) => {
          console.log('Scanning...');
          this.setState({isScanning: true});
        })
        .catch((err) => {
          console.error(err);
        });
    }
  };

  enableBluetooth = () => {
    BleManager.enableBluetooth()
      .then(() => {
        // Success code
        console.log('The bluetooth is already enabled or the user confirm');
        //this.startConnect();
        BleManager.start({showAlert: false});
        this.startScan();
      })
      .catch((error) => {
        //Failure code
        Alert.alert('Attention', 'Please turn on your bluetooth', [
          {
            text: 'OK',
            onPress: () => {
              BleManager.enableBluetooth();
            },
          },
        ]);
        return;
      });
  };

  handleStopScan = () => {
    console.log('Scan is stopped');
    this.setState({isScanning: false});
  };

  handleDiscoverPeripheral = async (peripheral) => {
    console.log('peripheral', peripheral);
    if (!peripheral.name) {
      peripheral.name = 'unknown';
    }

    if (
      this.state.beaconDetails &&
      this.state.beaconDetails.uniqueId === peripheral.id &&
      Math.abs(peripheral.rssi) <= 65
    ) {
      return alert(
        `Got beacon ${
          peripheral.id
        } with signal strength ${peripheral.rssi.toString()}`,
      );
      this.setState({beaconID: peripheral.id, rssi: Math.abs(peripheral.rssi)});
    }
  };
  componentWillUnmount() {
    clearInterval(this.interval);
    Geolocation.clearWatch(this.watchId);
    bleManagerEmitter.removeListener(
      'BleManagerDiscoverPeripheral',
      this.handleDiscoverPeripheral,
    );
    bleManagerEmitter.removeListener('BleManagerStopScan', this.handleStopScan);
  }

  travelCheckInAPIHandler = async (uri, coordinates) => {
    const assignedLocations = await getAssignedLocations();

    const findTravelGeoFence = assignedLocations.find(
      (each) => each.accessLocation === 'Travel Check In',
    );
    const AccessLocationId = findTravelGeoFence.id;
    // const AccessLocationId = await AsyncStorage.getItem(
    //   'travelCheckinLocationId',
    // );
    console.log('AccessLocationId', AccessLocationId);
    const {latitude, longitude, accuracy} = coordinates;
    Geocoder.from(latitude, longitude).then((json) => {
      const {results} = json;
      const filteredResults = results.filter(
        (e) => !e.formatted_address.includes('+'),
      );

      const filteredLocations = assignedLocations.filter(
        (e) => e.type !== 'Anonymous',
      );
      let count = 0;
      if (filteredLocations.length > 0) {
        filteredLocations.forEach((e) => {
          const check = validatePointWithinCircle(e, coordinates);
          console.log('check', check);
          if (check) {
            return this.setState({travelCheckInAddress: e.accessLocation});
          }
          count += 1;
        });
      }
      if (count === filteredLocations.length) {
        this.setState(
          {travelCheckInAddress: filteredResults[0].formatted_address},
          () =>
            this.checkOUT(uri, latitude, longitude, accuracy, AccessLocationId),
        );
      } else {
        this.checkOUT(uri, latitude, longitude, accuracy, AccessLocationId);
      }
    });
  };

  checkOUT = async (uri, latitude, longitude, accuracy, AccessLocationId) => {
    let uploadData = new FormData();
    //console.log('Process time', new Date());
    uploadData.append('file', {
      uri: uri,
      name: 'photo.png',
      type: 'image/png',
    });
    //uploadData.append('file', uri);
    uploadData.append('StaffNo', this.state.StaffNo);
    uploadData.append('Latitude', Number(latitude));
    uploadData.append('Longitude', Number(longitude));
    uploadData.append('InstituteId', this.state.institute_id);
    uploadData.append('Accuracy', Number(accuracy));
    uploadData.append('IsTravelCheckIn', true);
    uploadData.append('AccessLocationId', Number(AccessLocationId));
    uploadData.append('TravelCheckInAddress', this.state.travelCheckInAddress);
    console.log('uploadData', this.state.travelCheckInAddress);
    const CancelToken = axios.CancelToken;
    const source = CancelToken.source();
    setTimeout(() => {
      source.cancel('Request cancel after timeout');
    }, 30000);
    try {
      const response = await axios({
        method: 'post',
        url: Const + 'api/Staff/CheckOutWithFormFile',
        data: uploadData,
        headers: {'Content-Type': 'multipart/form-data'},
        timeout: 30000,

        cancelToken: source.token,
      });
      console.log('End time', new Date());

      const {data} = response;
      if (!data.status) return alert(data.message);
      const coordinates = {
        latitude,
        longitude,
      };
      const json = await staffCurrentLocation(
        this.state.bearer_token,
        this.state.institute_id,
        this.state.StaffNo,
        coordinates,
      );
      await AsyncStorage.setItem('current_travel_checkin', 'stopped');
      await AsyncStorage.setItem('checked_out', 'yes');
      await AsyncStorage.setItem('coordinates', '');
      await AsyncStorage.setItem('radius', '');
      await AsyncStorage.setItem('type', '');
      await AsyncStorage.setItem('checkin_time', '');
      await AsyncStorage.setItem('beacon', '');
      await AsyncStorage.setItem('geo_id', '');
      await AsyncStorage.setItem('actualCheckInTime', '');
      this.setState({
        showAlert: false,
        showAlert2: false,
        showAlert1: true,
        progress: 0,
        error_message: data.message,
      });
    } catch (error) {
      if (axios.isCancel(error)) {
        console.log('Request canceled', error.message);
        this.setState({
          showAlert: false,
          showAlert2: false,
          showAlert1: true,
          error_message:
            'Request taking too long to respond. Refresh internet connection and retry.',
          progress: 0,
          success: false,
        });
        return;
      }
      this.setState({
        showAlert1: true,
        error_message: 'Sorry! Unhandled exception occured while checking-out.',
        showAlert: false,
        progress: 0,

        showAlert2: false,
      });

      // this.setState({
      //   showAlert: false,
      //   showAlert2: false,
      //   showAlert1: false,
      //   progress: 0,
      // });
      //alert('Error validating face:' + e + '. Please try again');
    }
  };

  openCamera = async (camera) => {
    const options1 = {quality: 1, base64: false, width: 800};
    try {
      const image1 = await camera.takePictureAsync(options1);
      console.log(image1);
      if (!image1.uri) {
        return this.setState({
          showAlert1: true,
          error_message: 'Please re authenticate your face',
          showAlert: false,
          showTimer: false,
        });
      }

      // this.setState({
      //   show: false,
      //   showAlert2: true,
      //   capturedPhoto: image1,
      //   //error_message: 'Fetching location...',
      //   showAlert: false,
      // });
      ImageResizer.createResizedImage(
        image1.uri,
        300,
        300,
        'PNG',
        0,
        0,
        undefined,
      )
        .then(async (response) => {
          this.setState({
            show: false,
            //showAlert2: true,
            capturedPhoto: response,
            //error_message: 'Fetching location...',
            showAlert: false,
          });
          if (this.state.travelCheckOut) {
            this.setState({showAlert2: true});
            const options = {
              enableHighAccuracy: true,
              distanceFilter: 0,
              maximumAge: 0,
            };
            Geolocation.getCurrentPosition(
              (pos) => this.travelCheckInAPIHandler(response.uri, pos.coords),
              this.error,
              options,
            );

            return;
          }
          this.checkLocationHandler();
          // call new face validation API:
        })
        .catch((error) => console.log(error));
      //Travel Check-in
    } catch (error) {
      if (axios.isCancel(error)) {
        console.log('Request canceled', error.message);
        this.setState({
          showAlert: false,
          showAlert2: false,
          showAlert1: true,
          error_message:
            'Request taking too long to respond. Refresh internet connection and retry.',
          progress: 0,
          success: false,
        });
        return;
      }
      this.setState({
        showAlert1: true,
        error_message: 'Sorry! Unhandled exception occured while checking-out.',
        showAlert: false,
        progress: 0,

        showAlert2: false,
      });

      // this.setState({
      //   showAlert: false,
      //   showAlert2: false,
      //   showAlert1: false,
      //   progress: 0,
      // });
      //alert('Error validating face:' + e + '. Please try again');
    }
  };

  checkOutHandler = async () => {
    this.setState({showAlert2: true});
    const url = `${Const}api/Staff/StaffFaceCheckOut`;
    let uploadData = new FormData();
    uploadData.append('StaffNo', this.state.StaffNo);
    uploadData.append('InstituteId', this.state.institute_id);
    uploadData.append('File', {
      uri: this.state.capturedPhoto.uri,
      name: 'photo.png',
      type: 'image/png',
    });
    uploadData.append('AccessLocationId', this.state.geofence.id);
    const body = uploadData;
    const apiResponse = await axios.post(url, body, {
      headers: {
        'Content-Type': 'multipart/form-data',
      },
    });
    const {data} = apiResponse;
    console.log('facerespinse ==>', data);
    console.log('photo ==>', this.state.capturedPhoto.uri);
    if (!data.status && !data.message.includes('Unhandled')) {
      return this.setState({
        showAlert2: false,
        progress: 0,
        faceMismatchModal: true,
        faceMismatchMessage: data.message,
      });
    }
    if (data.status) {
      const json = await staffCurrentLocation(
        this.state.bearer_token,
        this.state.institute_id,
        this.state.StaffNo,
        this.state.geofence.coordinates[0],
      );
      await AsyncStorage.setItem('checked_out', 'yes');
      await AsyncStorage.setItem('coordinates', '');
      await AsyncStorage.setItem('radius', '');
      await AsyncStorage.setItem('type', '');
      await AsyncStorage.setItem('checkin_time', '');
      await AsyncStorage.setItem('beacon', '');
      await AsyncStorage.setItem('geo_id', '');
      await AsyncStorage.setItem('actualCheckInTime', '');
    }
    this.setState({
      showAlert: false,
      showAlert2: false,
      showAlert1: true,
      progress: 0,
      error_message: data.message,
    });
  };

  openCamera1 = async (camera) => {
    const options1 = {quality: 1, base64: false};
    const body = new FormData();

    try {
      const image1 = await camera.takePictureAsync(options1);
      ImageResizer.createResizedImage(
        image1.uri,
        300,
        300,
        'PNG',
        0,
        0,
        undefined,
      ).then(async (response) => {
        console.log('size of image ===>', response.size);
        this.setState({showAlert: true});
        if (response.uri) {
          body.append('StaffNo', this.state.StaffNo);
          body.append('InstituteId', this.state.institute_id);
          body.append('File', {
            uri: response.uri,
            name: 'photo.png',
            type: 'image/png',
          });
          body.append('AccessLocationId', Number(this.state.data.locationId));
          body.append('QrCodeOwnerStaffCode', this.state.data.StaffNo);
          console.log(body);
          const response = await axios({
            method: 'POST',
            url: Const + 'api/staff/QrCheckOut',
            data: body,
            headers: {'Content-Type': 'multipart/form-data'},
          });
          this.setState({show1: false, showAlert: false});
          console.log('JSON ==>', response.data);
          const json = response.data;
          if (!json.status) {
            return alert(response.data.message);
          }

          if (json.status) {
            this.setState({
              showAlert1: true,
              error_message: json.message,
              showAlert: false,
              count: 0,
            });
            if (this.state.isBluetoothOn) {
              BluetoothStateManager.disable().then((result) => {
                // do something...
                console.log('blettoth', result);
              });
            }
            AsyncStorage.setItem('checked_out', 'yes');
            try {
              //AsyncStorage.setItem('coordinates', '');
              //AsyncStorage.setItem('radius', '');
              //AsyncStorage.setItem('type', '');
              AsyncStorage.setItem('checkin_time', '');
              AsyncStorage.setItem('beacon', '');
              // AsyncStorage.setItem('geo_id', '');
              // if (ReactNativeForegroundService.is_task_running('taskid')) {
              //   ReactNativeForegroundService.remove_task('taskid');
              // }
              // Stoping Foreground service.
              KeepAwake.deactivate();
              //ReactNativeForegroundService.stop();

              return setTimeout(() => {
                this.props.navigation.navigate('Home');
              }, 5200);
            } catch (e) {
              console.log(e);
            }
          }
          //alert(response.data.response.message)
        }
      });
    } catch (e) {
      console.log(e);
      alert(e.toString());
      this.setState({show1: false, showAlert: false});
    }
  };

  CheckOutWithBarCode = async ({data}) => {
    let parsedDate = JSON.parse(data);
    let now1 = new Date().valueOf();
    let then1 = new Date(parsedDate.timeStamp).valueOf();
    const difference = (now1 - then1) / 1000;
    if (difference > 10) {
      this.setState({show2: false}, () => {
        return alert('Time exceeded for check out');
      });
      return;
    }
    this.setState({
      data: JSON.parse(data),
      show2: false,
      show1: true,
    });
  };

  launchTipsModal = () => {
    this.setState({modal: true, notInFence: false});
  };
  goToHome = () => {
    this.props.navigation.navigate('Home');
  };

  pinHandler = async () => {
    const url = `${Const}api/Staff/TravelCheckOutWithStaffCode`;
    this.setState({showAlert: true});
    Geolocation.getCurrentPosition(
      async (pos) => {
        try {
          const assignedLocations = await getAssignedLocations();
          const findTravelGeoFence = assignedLocations.find(
            (each) => each.accessLocation === 'Travel Check IN with Staff Code',
          );
          if (!findTravelGeoFence) {
            alert(
              'You are not assigned travel with mpin check in. Please logout and login again.',
            );
            return this.goToHome();
          }
          const geoFenceId = findTravelGeoFence?.id || 0;
          const shiftId = findTravelGeoFence?.shiftId || 0;
          const body = {
            geoFenceId,
            staffCode: this.state.StaffNo,
            instituteId: this.state.institute_id,
            checkoutCoordinates: `${pos.coords.latitude},${pos.coords.longitude}`,
            shiftId,
            mpin: this.state.mpin,
          };
          const response = await axios.post(url, body);
          const {data} = response;
          if (data.status) {
            await AsyncStorage.setItem('checked_out', 'yes');
            await AsyncStorage.setItem('current_travel_checkin', 'stopped');
            await AsyncStorage.setItem('actualCheckInTime', '');
          }

          this.setState({
            showAlert: false,
            showMPINAlert: false,
            showAlert1: true,
            error_message: data.message,
          });
        } catch (e) {
          this.setState({showAlert: false, showMPINAlert: false});
          alert('Error checking out with mpin: ' + e);
        }
      },
      this.error,
      {
        enableHighAccuracy: true,
        distanceFilter: 0,
        maximumAge: 0,
      },
    );
  };

  success = (position) => this.setState({coordinates: position.coords});

  error = (error) =>
    alert(
      'Error retreiving location. Check your location services. ' +
        JSON.stringify(error.message),
    );

  checkLocationHandler = async () => {
    try {
      const assignedLocationsAll = await getAssignedLocations();
      const assignedLocations = assignedLocationsAll.filter(
        (each) => each.type === 'Circle',
      );
      this.setState({loaderModal: true});

      const options = {
        enableHighAccuracy: true,
        distanceFilter: 0,
        interval: 1000,
        fastestInterval: 1000,
      };

      this.watchID = Geolocation.watchPosition(
        this.success,
        this.error,
        options,
      );
      this.main(assignedLocations);
    } catch (e) {
      alert('Error fetching assigned locations: ' + e);
    }
  };

  wait = (ms) => new Promise((resolve, reject) => setTimeout(resolve, ms));

  main = async (assignedLocations) => {
    await this.wait(10000);
    Geolocation.clearWatch(this.watchID);
    clearInterval(this.interval);
    this.setState({loaderModal: false, progress: 0});
    if (assignedLocations?.length === 0) {
      return alert(
        'No locations assigned. Please contact your administrator for further details.',
      );
    }
    let count = 0;
    assignedLocations.forEach((location) => {
      const point = validatePointWithinCircle(location, this.state.coordinates);
      if (point) {
        return this.setState({
          // show: true,
          geofence: location,
        });
      }
      count += 1;
    });
    console.log(count);
    if (count === assignedLocations.length) {
      return this.setState({locationModal: true});
    }
    // return this.setState({show: true});
    this.checkOutHandler();
  };

  render() {
    var cardStyle1 = {
      borderRadius: 10,
      elevation: 4,
      padding: 20,
    };
    var cardStyle2 = {
      borderRadius: 10,
      elevation: 4,
      padding: 20,
    };
    let {current_travel_check_in} = this.state;
    const mainCardStyle =
      this.state.travel_check_in == 'yes' ? cardStyle1 : cardStyle2;
    if (this.state.loader) {
      return <Loader />;
    }

    return (
      <View style={styles.container}>
        <AwesomeAlert
          show={this.state.showAlert}
          showProgress={true}
          title="Loading"
          closeOnTouchOutside={false}
          closeOnHardwareBackPress={false}
        />
        <CustomModal isVisible={this.state.loaderModal} doNotShowDeleteIcon>
          <View style={{justifyContent: 'center', alignItems: 'center'}}>
            <ActivityIndicator color={Colors.header} size={'large'} />
          </View>

          <CustomLabel
            title={
              'Please wait... Checking whether you are in your assigned location or not...'
            }
            labelStyle={{textAlign: 'center'}}
          />
        </CustomModal>

        {this.state.locationModal && (
          <LocationModal
            isVisible={this.state.locationModal}
            deleteIconPress={() =>
              this.setState({locationModal: false}, () => this.goToHome())
            }
            title={
              'Your GPS accuracy: ' +
              Number(this.state.coordinates.accuracy).toFixed(2)
            }
            subTitle={'The recommended GPS accuracy is 10 m or less.'}
            latitude={this.state.coordinates.latitude || 12.3788}
            longitude={this.state.coordinates.longitude || 74.444}
            // onPressRetry={() => {
            //   this.setState({locationModal: false});
            //   this.checkLocationHandler();
            // }}
            //radius={this.state.coordinates.accuracy || 25}
            assignedLocations={this.state.assignedLocations || []}
          />
        )}
        {this.state.faceMismatchMessage !== '' &&
          !this.state.faceMismatchMessage.includes('Unhandled') && (
            <FaceMismatchModal
              isVisible={this.state.faceMismatchModal}
              title={'Face Mismatch'}
              profilePic={
                this.state.profile_pic ||
                'https://upload.wikimedia.org/wikipedia/commons/9/99/Sample_User_Icon.png'
              }
              capturedPhoto={
                this.state.capturedPhoto?.uri ||
                'https://upload.wikimedia.org/wikipedia/commons/9/99/Sample_User_Icon.png'
              }
              errorMsg={
                this.state.faceMismatchMessage.includes('register')
                  ? this.state.faceMismatchMessage +
                    '. Re-register your face again'
                  : this.state.faceMismatchMessage +
                      '. Adjust your camera showing the face clearly or re-register your face again.' ||
                    'No error'
              }
              registerScreen={() => {
                this.setState({faceMismatchModal: false}, () =>
                  this.props.navigation.navigate('FaceRegister'),
                );
              }}
              deleteIconPress={() => this.setState({faceMismatchModal: false})}
            />
          )}
        {this.state.showAlert2 && (
          <Location
            accuracy={Number(this.state.coordinates.accuracy).toFixed(2)}
            latitude={Number(this.state.coordinates.latitude)}
            longitude={Number(this.state.coordinates.longitude)}
            isVisible={this.state.showAlert2}
            text={'Please wait... Trying to check-out...'}
            //count={this.state.count}
          />
        )}
        <AwesomeAlert
          show={this.state.showAlert1}
          showProgress={
            this.state.error_message === 'Fetching location...' ? true : false
          }
          //title={'Attention'}
          title={
            this.state.error_message === 'Fetching location...'
              ? this.state.count.toString()
              : 'Attention'
          }
          message={this.state.error_message}
          closeOnTouchOutside={false}
          closeOnHardwareBackPress={true}
          showCancelButton={
            this.state.error_message === 'Fetching location...' ? false : true
          }
          cancelText="Okay"
          onCancelPressed={() => {
            this.setState({showAlert1: false}, () => this.goToHome());
          }}
          cancelButtonColor="#f05760"
          cancelButtonTextStyle={{fontFamily: 'Poppins-Regular', fontSize: 13}}
          messageStyle={{fontFamily: 'Poppins-Regular', fontSize: 13}}
          titleStyle={{fontFamily: 'Poppins-SemiBold', fontSize: 14}}
        />
        <Container>
          {/* <SubHeader
            title="Check Out"
            showBack={true}
            backScreen="Home"
            showBell={false}
            navigation={this.props.navigation}
          /> */}
          <Content>
            {this.state.show && (
              <CameraComp
                visible={this.state.show}
                onClose={() =>
                  this.setState({show: false}, () => this.goToHome())
                }
                onPress={(camera) => this.openCamera(camera)}
                //onBarCodeRead = {this.CheckOutWithBarCode}
                //btnText={'check-out'}
                btnText={
                  this.state.travelCheckOut ? 'Travel Check-Out' : 'Check-Out'
                }
                onRecordingStart={(event) => {
                  //console.log('event', event);
                  this.interval2 = setInterval(() => {
                    this.setState((prev) => ({btnCount: prev.btnCount + 1}));
                  }, 1000);
                }}
                onRecordingEnd={() => {
                  clearInterval(this.interval2);
                  this.setState({
                    show: false,
                    btnCount: 0,
                    pressed: false,
                    showAlert: true,
                  });
                }}
              />
            )}
            {/* <View style = {{padding: 10, flex: 1, justifyContent: 'center', alignItems: 'center'}}>
              <TouchableWithoutFeedback
                    onPress={() =>
                      this.setState({qrCodeScanner: true, show2: true})
                    }>
                    <Card 
                      style={{
                      borderRadius: 10,
                      elevation: 4,
                      padding: 20,
                      width: wp('85'),
                    }}>
                      <Text style={styles.checkinText}>QR Code Check Out</Text>
                    </Card>
                  </TouchableWithoutFeedback>
              </View> */}

            {/* <View style={{marginTop: 20, marginLeft: 15, marginRight: 15}}>
              <Text style={styles.heading}>
                Please select your Check OUT type
              </Text>
              <View style={styles.center}>
                <TouchableWithoutFeedback
                  onPress={() => {
                    if (current_travel_check_in === 'running') {
                      return alert(
                        'You have checked in via travel check in. Opt for Travel Check Out.',
                      );
                    }
                    // this.setState({travelCheckOut: false, show: true});
                    this.checkGPSStatus().then((status) => {
                      console.log('SSTTTS', status);
                      if (
                        status === GPSState.DENIED ||
                        status === GPSState.NOT_DETERMINED ||
                        status === GPSState.RESTRICTED
                      ) {
                        return Alert.alert(
                          'Refresh GPS services',
                          'It is recommended that you refresh your GPS services for proper check-in',
                          [
                            {
                              text: 'Open Settings',
                              onPress: () => GPSState.openLocationSettings(),
                            },
                          ],
                          {cancelable: false},
                        );
                      }
                      this.setState({show: true});
                      //this.checkLocationHandler();
                    });
                  }}>
                  <Card style={styles.mainCardStyle}>
                    <Text style={styles.checkinText}>Check OUT</Text>
                  </Card>
                </TouchableWithoutFeedback>
                {this.state.travel_check_in == 'yes' && (
                  <TouchableWithoutFeedback
                    onPress={() => {
                      if (current_travel_check_in === 'running') {
                        this.setState({travelCheckOut: true, show: true});
                      } else {
                        alert('You have not checked in via travel check in');
                      }
                    }}>
                    <Card style={styles.mainCardStyle}>
                      <Text style={styles.checkinText}>Travel Check OUT</Text>
                    </Card>
                  </TouchableWithoutFeedback>
                )}
                {this.state.isTravelCheckinWithMpin == 'yes' && (
                  <TouchableWithoutFeedback
                    onPress={() => this.setState({showMPINAlert: true})}>
                    <Card style={styles.mainCardStyle}>
                      <Text style={styles.checkinText}>
                        MPIN Travel Check OUT
                      </Text>
                    </Card>
                  </TouchableWithoutFeedback>
                )}
                {this.state.qrCheckout === 'true' && (
                  <TouchableWithoutFeedback
                    onPress={() =>
                      this.setState({qrCodeScanner: true, show2: true})
                    }>
                    <Card style={styles.mainCardStyle}>
                      <Text style={styles.checkinText}>QR Code Check OUT</Text>
                    </Card>
                  </TouchableWithoutFeedback>
                )}
              </View>
            </View> */}

            {this.state.showMPINAlert && (
              <CustomModal
                isVisible={this.state.showMPINAlert}
                deleteIconPress={() =>
                  this.setState({showMPINAlert: false}, () => this.goToHome())
                }>
                <Text>Enter MPIN to verify</Text>
                <OTPInputView
                  style={{width: wp('80'), height: 80}}
                  pinCount={4}
                  autoFocusOnLoad={true}
                  secureTextEntry={true}
                  codeInputFieldStyle={styles.inputFeilds}
                  codeInputHighlightStyle={styles.inputFeildsFocus}
                  onCodeFilled={(text) => this.setState({mpin: text})}
                />

                <CustomButton
                  title={'Verify'}
                  color={Colors.header}
                  onPress={this.pinHandler}
                />
              </CustomModal>
            )}

            {this.state.show1 && (
              <CameraComp
                visible={this.state.show1}
                onClose={() =>
                  this.setState({show1: false}, () => this.goToHome())
                }
                onPress={(camera) => this.openCamera1(camera)}
                btnText={'qr check-out'}
              />
            )}
            {this.state.show2 && (
              <CameraComp
                visible={this.state.show2}
                onClose={() =>
                  this.setState({show2: false}, () => this.goToHome())
                }
                onPress={(camera) => {}}
                btnText={'QR Code Scanner'}
                onBarCodeRead={this.CheckOutWithBarCode}
                //type = {RNCamera.Constants.Type.back}
              />
            )}
            {/* {this.state.modal && (
              <ModalForAccuracy
                visible={this.state.modal}
                onClose={() => this.setState({modal: false})}
                onPress={() => this.props.navigation.goBack()}
                //accuracy={parseInt(this.state.coordinates.accuracy).toString()}
                accuracy={Math.round(
                  Number(this.state.coordinates.accuracy),
                ).toString()}
                checkout
              />
            )} */}
          </Content>
        </Container>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fbfbfb',
  },
  header: {
    backgroundColor: '#089bf9',
  },
  title: {
    fontFamily: 'Poppins-SemiBold',
    fontSize: 17,
    color: '#ffffff',
  },
  cardContainer: {
    marginTop: '10%',
    marginLeft: '6%',
    marginRight: '6%',
    justifyContent: 'center',
  },
  card: {
    borderRadius: 10,
    elevation: 4,
    paddingTop: '3%',
    paddingBottom: '3%',
    height: hp('60'),
  },
  buttonContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
    backgroundColor: '#f05760',
    borderRadius: 20,
    width: wp('35'),
    paddingRight: wp('7'),
    marginTop: '4%',
  },
  buttonText: {
    fontFamily: 'Poppins-Regular',
    fontSize: 13,
    color: '#ffffff',
  },
  btImage: {
    width: 50,
    height: 39,
    borderRadius: 20,
  },
  preview: {
    flex: 1,
  },
  heading: {
    fontFamily: 'Poppins-SemiBold',
    margin: 12,
  },
  buttonContainer1: {
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
    backgroundColor: '#f05760',
    borderRadius: 20,
    width: wp('41'),
    marginTop: '4%',
  },
  mainCardStyle: {
    borderRadius: 10,
    elevation: 4,
    padding: 20,
    width: wp('85'),
  },
  checkinText: {
    fontFamily: 'Poppins-Regular',
    fontSize: 13,
  },
  buttonText: {
    fontFamily: 'Poppins-Regular',
    fontSize: 12,
    color: '#ffffff',
    paddingRight: '3%',
  },
  userImage: {
    width: 120,
    height: 120,
  },
  center: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  note: {
    fontFamily: 'Poppins-SemiBold',
    fontSize: 13,
    marginBottom: 10,
  },
  inputFeilds: {
    backgroundColor: '#ffffff',
    borderWidth: 1,
    color: '#000000',
    borderRadius: 10,
  },
  inputFeildsFocus: {
    backgroundColor: '#ffffff',
    borderWidth: 2,
    borderColor: '#f05760',
    color: '#000000',
    borderRadius: 10,
  },
});
