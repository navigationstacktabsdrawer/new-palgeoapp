import moment from 'moment';
import {Icon, ScrollableTab, Tabs, Tab, Card} from 'native-base';
import React, {Component} from 'react';
import {View, Text, StyleSheet} from 'react-native';
import {Calendar} from 'react-native-calendars';
import {Colors} from '../../utils/configs/Colors';
import {createMaterialTopTabNavigator} from '@react-navigation/material-top-tabs';
import {Image} from 'react-native';
import {ScrollView} from 'react-native';
import Const from '../common/Constants';
import {
  heightPercentageToDP,
  widthPercentageToDP,
} from 'react-native-responsive-screen';
import {TouchableOpacity} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import axios from 'axios';
import {Platform} from 'react-native';
import {ActivityIndicator} from 'react-native';
import {Dimensions} from 'react-native';
import {TouchableWithoutFeedback} from 'react-native';
import CustomMap from '../common/CustomMap';
import {Overlay} from 'react-native-elements';
import Layout from '../common/Layout';
import {
  arrayWithoutDuplicates,
  groupByKey,
  showErrorMsg,
} from '../../utils/helperFunctions';

export default class Holidays extends Component {
  constructor(props) {
    super(props);
    this.state = {
      SelectedDay: moment().format('YYYY-MM-DD'),
      Holidays: [],
      WeekOff: [],
      Shifts: [],
      HolidayLoader: true,
      WeekOffLoader: true,
      ShiftLoader: true,
      CurrentMonth: moment(new Date()).format('MM'),
    };
  }
  componentDidMount() {
    this.retrieveData();
  }
  retrieveData = async () => {
    try {
      const value = await AsyncStorage.getItem('bearer_token');
      if (value !== null) {
        //alert(value);
        this.setState({Token: value}, function () {});
      }
    } catch (error) {
      alert('Error retrieving data');
    }
    try {
      const value = await AsyncStorage.getItem('institute_id');
      if (value !== null) {
        this.setState({institute_id: value}, function () {});
      }
    } catch (error) {
      alert('Error retrieving data');
    }
    try {
      const value = await AsyncStorage.getItem('user_id');
      if (value !== null) {
        //alert(value);
        this.setState({StaffCode: value}, () => {
          this.Holidays();
          // this.WeekOff();
          // this.Shifts();
        });
      }
    } catch (error) {
      alert('Error retrieving data');
    }
  };
  Holidays = async () => {
    const url =
      'https://insproplus.com/palgeoapi/api/Master/GetStaffHolidayMondayDetails';
    //console.log('url = ', url);
    let bodyData = {};
    bodyData = {
      StaffCode: this.state.StaffCode.toString(),
      InstituteId: parseInt(this.state.institute_id),
      Month:
        this.state.CurrentMonth == 1
          ? 'January'
          : this.state.CurrentMonth == 2
          ? 'February'
          : this.state.CurrentMonth == 3
          ? 'March'
          : this.state.CurrentMonth == 4
          ? 'April'
          : this.state.CurrentMonth == 5
          ? 'May'
          : this.state.CurrentMonth == 6
          ? 'June'
          : this.state.CurrentMonth == 7
          ? 'July'
          : this.state.CurrentMonth == 8
          ? 'August'
          : this.state.CurrentMonth == 9
          ? 'September'
          : this.state.CurrentMonth == 10
          ? 'October'
          : this.state.CurrentMonth == 11
          ? 'November'
          : this.state.CurrentMonth == 12
          ? 'December'
          : 'December',
      //Month: this.state.CurrentMonth.toString(),
    };
    //console.log('bodyData = ', bodyData);
    try {
      const response = await axios.post(url, bodyData);
      let data = [];
      if (response?.data?.length > 0) {
        data = response.data.sort((e1, e2) => e1.holidayDate >= e2.holidayDate);
      }
      //console.log('GetStaffHolidayMondayDetails response - ', response.data);
      this.setState({Holidays: data, HolidayLoader: false}, () => {
        //alert(response.data);
      });
    } catch (e) {
      this.setState({HolidayLoader: false}, () => {
        //alert(response.data);
        showErrorMsg(e);
      });
      //alert('GetStaffHolidayMondayDetails = ', e);
      //console.log('GetStaffHolidayMondayDetails = ', e);
    }
  };
  WeekOff = async () => {
    const url =
      'https://insproplus.com/palgeoapi/api/Master/GetAllMandatoryHolidayMonthDetails';
    //const url = Const + 'api​/Master​/GetAllMandatoryHolidayMonthDetails';
    //console.log('url = ', url);
    let bodyData = {};
    bodyData = {
      StaffCode: this.state.StaffCode.toString(),
      InstituteId: parseInt(this.state.institute_id),
      Month:
        this.state.CurrentMonth == 1
          ? 'January'
          : this.state.CurrentMonth == 2
          ? 'February'
          : this.state.CurrentMonth == 3
          ? 'March'
          : this.state.CurrentMonth == 4
          ? 'April'
          : this.state.CurrentMonth == 5
          ? 'May'
          : this.state.CurrentMonth == 6
          ? 'June'
          : this.state.CurrentMonth == 7
          ? 'July'
          : this.state.CurrentMonth == 8
          ? 'August'
          : this.state.CurrentMonth == 9
          ? 'September'
          : this.state.CurrentMonth == 10
          ? 'October'
          : this.state.CurrentMonth == 11
          ? 'November'
          : this.state.CurrentMonth == 12
          ? 'December'
          : 'December',
      //Month: this.state.CurrentMonth.toString(),
    };
    //console.log('bodyData = ', bodyData);
    try {
      const response = await axios.post(url, bodyData);
      let data = [];
      if (response?.data?.length > 0) {
        data = response.data.sort((e1, e2) => e1.date >= e2.date);
      }
      // console.log(
      //   'GetAllMandatoryHolidayMonthDetails response - ',
      //   response.data,
      // );
      this.setState({WeekOff: data, WeekOffLoader: false}, () => {
        // console.log(JSON.stringify(response.data, null, 4));
      });
    } catch (e) {
      this.setState({WeekOffLoader: false}, () => {
        //alert(response.data);
        showErrorMsg(e);
      });
      //alert('GetAllMandatoryHolidayMonthDetails = ', e);
      // console.log('GetAllMandatoryHolidayMonthDetails = ', e);
    }
  };
  Shifts = async () => {
    const url =
      'https://insproplus.com/palgeoapi/api/GeoFencing/shift/MonthDetails';
    //console.log('url = ', url);
    let bodyData = {};
    bodyData = {
      StaffCode: this.state.StaffCode.toString(),
      InstituteId: parseInt(this.state.institute_id),
      Month:
        this.state.CurrentMonth == 1
          ? 'January'
          : this.state.CurrentMonth == 2
          ? 'February'
          : this.state.CurrentMonth == 3
          ? 'March'
          : this.state.CurrentMonth == 4
          ? 'April'
          : this.state.CurrentMonth == 5
          ? 'May'
          : this.state.CurrentMonth == 6
          ? 'June'
          : this.state.CurrentMonth == 7
          ? 'July'
          : this.state.CurrentMonth == 8
          ? 'August'
          : this.state.CurrentMonth == 9
          ? 'September'
          : this.state.CurrentMonth == 10
          ? 'October'
          : this.state.CurrentMonth == 11
          ? 'November'
          : this.state.CurrentMonth == 12
          ? 'December'
          : 'December',
      //Month: this.state.CurrentMonth.toString(),
    };
    // console.log('bodyData = ', bodyData);
    try {
      const response = await axios.post(url, bodyData);
      let data = [];

      let uniques = [];
      if (response?.data?.length > 0) {
        data = response.data.sort((e1, e2) => e1.startDate >= e2.startDate);
        const newData = data.map((e) => {
          return {
            date: e.startDate,
            data: data.filter((ee) => ee.startDate === e.startDate),
          };
        });
        uniques = arrayWithoutDuplicates(newData, 'date');
      }

      this.setState({Shifts: uniques, ShiftLoader: false}, () => {
        // console.log(JSON.stringify(uniques, null, 2));
      });
    } catch (e) {
      this.setState({ShiftLoader: false}, () => {
        showErrorMsg(e);
        //alert(response.data);
      });

      console.log('MonthDetails = ', JSON.stringify(e));
    }
  };
  renderHolidays = () => {
    return (
      <View style={{width: '100%', minHeight: 200}}>
        {!this.state.HolidayLoader ? (
          this.state.Holidays.length ? (
            this.state.Holidays.map((item, index) => {
              return (
                <Card
                  key={index}
                  style={{
                    width: '90%',
                    justifyContent: 'space-between',
                    //alignItems: 'center',
                    alignSelf: 'center',
                    flexDirection: 'row',
                    padding: 10,
                    // borderBottomColor: Colors.overlay,
                    // borderBottomWidth: 2,
                    // marginBottom: 5,
                  }}>
                  {/* <View style={[styles.ListSubView, {width: '10%'}]}>
                    <Text style={styles.ListText}>{index + 1}.</Text>
                  </View> */}
                  <View style={[styles.ListSubView, {width: '90%'}]}>
                    <Text style={styles.ListText}>
                      {' '}
                      {moment(item.holidayDate).format('DD')}
                      {' - '}
                      {item.isFullDay || item.isEvening || item.isMorning ? (
                        <Text
                          style={
                            ([styles.ListText], {textTransform: 'capitalize'})
                          }>
                          {item.description}
                          {' ('}
                          {item.isFullDay
                            ? 'Full Day'
                            : item.isEvening
                            ? 'Half Day : Evening'
                            : item.isMorning
                            ? 'Half Day : Morning'
                            : null}
                          {' )'}
                        </Text>
                      ) : null}
                    </Text>
                    {/*<Text style={styles.ListText}>{item.description}</Text>*/}
                  </View>
                </Card>
              );
            })
          ) : (
            <Text style={[styles.ListText, {padding: 10}]}>
              No Holiday Available
            </Text>
          )
        ) : (
          <View
            style={{
              justifyContent: 'space-evenly',
              alignSelf: 'center',
              alignItems: 'center',
              width: '100%',
              marginTop: 10,
            }}>
            <ActivityIndicator size="large" color={Colors.header} />
            <Text
              style={{
                fontFamily: 'Poppins-Regular',
                fontSize: 14,
              }}>
              Fetching Holidays Data...
            </Text>
          </View>
        )}
      </View>
    );
  };
  renderWeekOff = () => {
    return (
      <View style={{width: '100%', minHeight: 200}}>
        {!this.state.WeekOffLoader ? (
          this.state.WeekOff.length ? (
            this.state.WeekOff.map((item, index) => {
              return (
                <View
                  key={index}
                  style={{
                    width: '100%',
                    justifyContent: 'space-between',
                    //alignItems: 'center',
                    flexDirection: 'row',
                    padding: 10,
                  }}>
                  <View style={[styles.ListSubView, {width: '10%'}]}>
                    <Text style={styles.ListText}>{index + 1}.</Text>
                  </View>
                  <View style={styles.ListSubView}>
                    <Text style={styles.ListText}>
                      {moment(item.date, 'DD-MM-YYYY').format('DD MMM')}
                    </Text>
                  </View>
                  <View style={styles.ListSubView}>
                    <Text style={styles.ListText}>{item.holidays}</Text>
                  </View>
                </View>
              );
            })
          ) : (
            <Text style={[styles.ListText, {padding: 10}]}>
              No Week Off Available
            </Text>
          )
        ) : (
          <View
            style={{
              justifyContent: 'space-evenly',
              alignSelf: 'center',
              alignItems: 'center',
              width: '100%',
              marginTop: 10,
            }}>
            <ActivityIndicator size="large" color={Colors.header} />
            <Text
              style={{
                fontFamily: 'Poppins-Regular',
                fontSize: 14,
              }}>
              Fetching WeekOff Data...
            </Text>
          </View>
        )}
      </View>
    );
  };

  goToMap = (item) => {
    const {coordinates, radius} = item;
    const parsedCoo = JSON.parse(coordinates);
    const newCoo = parsedCoo.map((e) => {
      return {
        latitude: e.Latitude,
        longitude: e.Longitude,
      };
    });
    console.log('neww', newCoo);

    if (parsedCoo.length > 1) {
      this.props.navigation.navigate('PolygonMapView', {
        coordinates: newCoo,
        back: true,
      });
      return;
    }
    // const newCoo = [
    //   {
    //     latitude: parsedCoo[0].Latitude,
    //     longitude: parsedCoo[0].Longitude,
    //   },
    // ];
    this.props.navigation.navigate('CircleMapView', {
      coordinates: newCoo,
      radius,
      back: true,
    });
  };

  renderShifts = () => {
    return (
      <View style={{width: '100%', minHeight: 200}}>
        {!this.state.ShiftLoader ? (
          this.state.Shifts.length ? (
            this.state.Shifts.map((itemm, index) => {
              return (
                <Card key={index}>
                  <View
                    style={{
                      marginVertical: 15,
                      backgroundColor: Colors.maroon,
                      paddingVertical: widthPercentageToDP('2%'),
                    }}>
                    <Text
                      style={{
                        color: 'white',
                        fontSize: 14,
                        fontFamily: 'Poppins-SemiBold',
                        textAlign: 'center',
                      }}>
                      {' '}
                      {moment(itemm.date).format('DD MMM, YYYY')}{' '}
                    </Text>
                  </View>
                  {itemm.data.length > 0 &&
                    itemm.data.map((item, i) => {
                      return (
                        <View key={i}>
                          <View
                            style={{
                              width: '100%',
                              justifyContent: 'space-between',
                              //alignItems: 'center',
                              flexDirection: 'row',
                              padding: 10,
                            }}>
                            <View style={[styles.ListSubView, {width: '10%'}]}>
                              <Text style={styles.ListText}>{i + 1}.</Text>
                            </View>
                            <View style={[styles.ListSubView, {width: '90%'}]}>
                              <Text style={[styles.ListText, {color: 'black'}]}>
                                {item.shiftName}
                                {'( ' +
                                  moment
                                    .utc(item.checkInTime, 'HH:mm')
                                    .local()
                                    .format('HH:mm') +
                                  ' - ' +
                                  moment
                                    .utc(item.checkOutTime, 'HH:mm')
                                    .local()
                                    .format('HH:mm') +
                                  ' )'}
                              </Text>
                              <TouchableWithoutFeedback
                                onPress={() => this.goToMap(item)}>
                                <Text
                                  style={[
                                    styles.ListText,
                                    {
                                      color: Colors.header,
                                      textDecorationLine: 'underline',
                                    },
                                  ]}>
                                  {item.geoFencingName}
                                </Text>
                              </TouchableWithoutFeedback>
                            </View>
                          </View>
                        </View>
                      );
                    })}
                </Card>
              );
            })
          ) : (
            <Text style={[styles.ListText, {padding: 10}]}>
              No Shift Available
            </Text>
          )
        ) : (
          <View
            style={{
              justifyContent: 'space-evenly',
              alignSelf: 'center',
              alignItems: 'center',
              width: '100%',
              marginTop: 10,
            }}>
            <ActivityIndicator size="large" color={Colors.header} />
            <Text
              style={{
                fontFamily: 'Poppins-Regular',
                fontSize: 14,
              }}>
              Fetching Shifts Data...
            </Text>
          </View>
        )}
      </View>
    );
  };
  render() {
    return (
      <Layout headerTitle="My Calendar" navigation={this.props.navigation}>
        <View>
          <Calendar
            theme={{
              arrowColor: Colors.header,
              monthTextColor: Colors.header,
              textMonthFontFamily: 'Poppins-Regular',
              textMonthFontWeight: 'bold',
              textMonthFontSize: 20,
              'stylesheet.calendar.header': {
                week: {
                  marginTop: 5,
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                },
              },
            }}
            markingType={'multi-dot'}
            dayComponent={(d) => {
              const holidays = this.state.Holidays.map((e) =>
                moment(e.holidayDate).format('D'),
              );
              const weekoffs = this.state.WeekOff.map((e) =>
                moment(e.date, 'DD-MM-YYYY').format('D'),
              );

              //console.log('d== ', d);
              return (
                <TouchableOpacity
                  disabled={this.state.CurrentMonth != d.date.month}
                  onPress={() => {
                    this.setState({SelectedDay: d.date.dateString});
                  }}
                  style={{
                    backgroundColor:
                      this.state.CurrentMonth == d.date.month &&
                      this.state.SelectedDay == d.date.dateString
                        ? '#f05760'
                        : 'transparent',
                    width: 50,
                    height: 50,
                    borderRadius: 50,
                    padding: 5,
                    // alignItems: 'center',
                    // justifyContent: 'center',
                  }}>
                  <Text
                    style={{
                      fontSize: 20,
                      alignSelf: 'center',
                      fontFamily: 'Poppins-Regular',
                      fontWeight: 'bold',
                      //color: 'black',
                      color:
                        this.state.CurrentMonth == d.date.month
                          ? this.state.SelectedDay == d.date.dateString
                            ? 'white'
                            : '#f05760'
                          : 'silver',
                    }}>
                    {d.date.day}
                  </Text>
                  <View
                    style={{flexDirection: 'row', justifyContent: 'center'}}>
                    {holidays.includes(JSON.stringify(d.date.day)) &&
                      parseInt(this.state.CurrentMonth) ==
                        parseInt(d.date.month) && (
                        <Image
                          source={require('../../assets/ic_privacy.png')}
                          style={{
                            width: 15,
                            height: 15,
                            resizeMode: 'contain',
                            marginHorizontal: 5,
                            tintColor: '#4000ff',
                          }}
                        />
                      )}
                    {weekoffs.includes(JSON.stringify(d.date.day)) &&
                      parseInt(this.state.CurrentMonth) ==
                        parseInt(d.date.month) && (
                        <Image
                          source={require('../../assets/ic_privacy.png')}
                          style={{
                            width: 15,
                            height: 15,
                            resizeMode: 'contain',
                            //marginHorizontal: 5,
                            tintColor: 'green',
                          }}
                        />
                      )}
                  </View>
                </TouchableOpacity>
              );
            }}
            onDayPress={(day) => {
              //console.log('selected day', day);
              //console.log('selected day', day.dateString);
              this.setState({SelectedDay: day.dateString});
            }}
            onMonthChange={(month) => {
              //console.log('month changed', month);
              this.setState(
                {
                  CurrentMonth: month.month,
                  HolidayLoader: true,
                  WeekOffLoader: true,
                  ShiftLoader: true,
                },
                () => {
                  this.Holidays();
                  this.WeekOff();
                  this.Shifts();
                },
              );
            }}
          />
          <View
            style={{
              width: '100%',
              minHeight: 500,
            }}>
            <Tabs
              onChangeTab={(data) => {
                if (data.i == 2) {
                  this.Shifts();
                }
                if (data.i == 1) {
                  this.WeekOff();
                }
              }}
              renderTabBar={() => (
                <ScrollableTab
                  tabStyle={{
                    backgroundColor:
                      Platform.OS == 'android' ? 'white' : 'transparent',
                  }}
                  tabsContainerStyle={{backgroundColor: 'white'}}
                  underlineStyle={{
                    backgroundColor: Colors.header,
                    height: 2,
                  }}
                />
              )}>
              <Tab
                tabStyle={{
                  backgroundColor:
                    Platform.OS == 'android' ? 'white' : 'transparent',
                }}
                activeTabStyle={{
                  backgroundColor:
                    Platform.OS == 'android' ? 'white' : 'transparent',
                }}
                activeTextStyle={{color: Colors.header}}
                textStyle={{color: 'black'}}
                heading="Holidays">
                {this.renderHolidays()}
              </Tab>
              <Tab
                tabStyle={{
                  backgroundColor:
                    Platform.OS == 'android' ? 'white' : 'transparent',
                }}
                activeTabStyle={{
                  backgroundColor:
                    Platform.OS == 'android' ? 'white' : 'transparent',
                }}
                activeTextStyle={{color: Colors.header}}
                textStyle={{color: 'black'}}
                heading="WeekOff">
                {this.renderWeekOff()}
              </Tab>
              <Tab
                tabStyle={{
                  backgroundColor:
                    Platform.OS == 'android' ? 'white' : 'transparent',
                }}
                activeTabStyle={{
                  backgroundColor:
                    Platform.OS == 'android' ? 'white' : 'transparent',
                }}
                activeTextStyle={{color: Colors.header}}
                textStyle={{color: 'black'}}
                heading="Shift Time">
                {this.renderShifts()}
              </Tab>
            </Tabs>
          </View>
        </View>
      </Layout>
    );
  }
}

const styles = StyleSheet.create({
  ListSubView: {width: '45%'},
  ListText: {
    fontFamily: 'Poppins-SemiBold',
    fontSize: 16,
  },
});
