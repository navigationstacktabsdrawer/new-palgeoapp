import {Textarea, Thumbnail} from 'native-base';
import React, {Component} from 'react';
import {Text, Alert, View, StyleSheet, TouchableOpacity} from 'react-native';
import {CustomButton} from './CustomButton';
import DocumentPicker from 'react-native-document-picker';
import RNFetchBlob from 'rn-fetch-blob';
import CameraComp from './CameraComp';
import ImageCropPicker from 'react-native-image-crop-picker';
export class CustomTextArea extends Component {
  constructor(props) {
    super(props);
    this.state = {Attachment: {}};
  }
  SelectImage = async () => {
    const {SelectedImage} = this.props;
    try {
      const res = await DocumentPicker.pick({
        type: [DocumentPicker.types.allFiles],
      });
      let typeIdentifier = res.type.split('/');
      //console.log(typeIdentifier[1]);
      //console.log('res = ', res);

      RNFetchBlob.fs
        .readFile(res.uri, 'base64')
        .then((data) => {
          //console.log('data =', data);
          var Attachment = {
            FileName:
              'Attachment_' + new Date().getTime() + '.' + typeIdentifier[1],
            //FileType: res.type,
            //Attachment: res.uri,
            Attachment: data,
          };

          this.setState(
            {Attachment: Attachment},
            () => SelectedImage(Attachment),
            //console.log('Attachment = ', Attachment),
          );
        })
        .catch((err) => {});
    } catch (err) {
      console.log('Unknown Error: ' + JSON.stringify(err));
    }
  };
  openCamera = async (camera) => {
    const {SelectedImage} = this.props;

    const options1 = {quality: 0, base64: true};
    try {
      const image1 = await camera.takePictureAsync(options1);

      // const image = await ImageCropPicker.openCamera({
      //   width: 300,
      //   height: 400,
      //   includeBase64: true,
      //   cropping: true,
      // });
      // //console.log(image);
      let fileExtension = image1.uri.slice(-3);
      console.log(fileExtension, image1.uri);
      var Attachment = {
        FileName: 'Attachment_' + new Date().getTime() + '.' + fileExtension,
        //FileType: image.mime,
        Attachment: image1.base64,
        //Attachment: image.path.replace('file:///', 'file://'),
      };

      this.setState(
        {Attachment: Attachment},
        () => {
          SelectedImage(Attachment);
          this.setState({showCamera: false});
        },
        //console.log('Attachment = ', Attachment),
      );
    } catch (e) {
      console.log();
    }
  };
  render() {
    let {
      text,
      onChangeText,
      value,
      placeholder,
      width,
      backgroundColor,
      placeholderTextColor,
      title,
      onPress,
      onPress2,
    } = this.props;
    return (
      <View style={styles.item1}>
        <Text style={[styles.label, {margin: 10}]}>{title}</Text>
        <Textarea
          blurOnSubmit={true}
          rowSpan={5}
          bordered
          placeholderTextColor={placeholderTextColor}
          placeholder={placeholder}
          style={[
            styles.textarea,
            {
              width: width,
              backgroundColor: backgroundColor,
              alignSelf: 'center',
            },
          ]}
          value={value}
          onChangeText={(message) => {
            onChangeText(message);
          }}
        />
        <View style={{marginTop: 20}}>
          <Text style={[styles.label, {margin: 10}]}>Attachment</Text>
          {this.state.showCamera && (
            <CameraComp
              back
              btnText={'Capture'}
              visible={this.state.showCamera}
              onClose={() => this.setState({showCamera: false})}
              onPress={(camera) => this.openCamera(camera)}
            />
          )}
          <TouchableOpacity
            //onPress={this.SelectImage}
            onPress={() =>
              Alert.alert('Choose one option', '', [
                {
                  text: 'CAMERA',
                  onPress: () => this.setState({showCamera: true}),
                },
                {text: 'GALLERY', onPress: () => this.SelectImage()},
              ])
            }
            style={{
              backgroundColor: backgroundColor,
              borderRadius: 10,
              height: 40,
              width: '95%',
              alignSelf: 'center',
              flexDirection: 'row',
              alignItems: 'center',
            }}>
            <Thumbnail
              square
              style={{
                width: 15,
                height: 15,
                resizeMode: 'contain',
                marginLeft: 10,
              }}
              source={require('../../assets/attachment.png')}
            />
            <Text
              style={[
                styles.label,
                {color: placeholderTextColor, padding: 10, fontWeight: '400'},
              ]}>
              Attach document proof
            </Text>
          </TouchableOpacity>
          {text != 'No files attached' ? (
            <View
              style={{
                backgroundColor: backgroundColor,
                borderRadius: 10,
                margin: 10,
                minHeight: 40,
                width: '95%',
                paddingHorizontal: 10,
                flexDirection: 'row',
                alignItems: 'center',
              }}>
              <View
                style={{
                  width: '85%',
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <Text
                  style={[
                    styles.label,
                    {
                      color: placeholderTextColor,
                      paddingHorizontal: 10,
                      fontSize: 14,
                    },
                  ]}>
                  {text}
                </Text>
              </View>
              <TouchableOpacity
                onPress={() => {
                  console.log('delete button pressed');
                  onPress2();
                }}
                style={{width: '15%', alignItems: 'center'}}>
                <Thumbnail
                  square
                  style={{
                    width: 12,
                    height: 12,
                    resizeMode: 'contain',
                  }}
                  source={require('../../assets/cross.png')}
                />
              </TouchableOpacity>
            </View>
          ) : null}
          <View style={{marginVertical: 20}}>
            <CustomButton
              title="Apply"
              width="40%"
              color={placeholderTextColor}
              onPress={() => {
                onPress();
              }}
            />
          </View>
        </View>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  item1: {
    width: '95%',
    alignSelf: 'center',
    borderRadius: 10,
    backgroundColor: 'white',
    elevation: 3,
    shadowColor: 'silver',
    shadowOffset: {width: 0, height: 0.5},
    shadowOpacity: 0.6,
  },
  textarea: {
    alignSelf: 'center',
    padding: 10,
    borderLeftWidth: 0,
    borderBottomWidth: 0,
    borderTopWidth: 0,
    borderRightWidth: 0,
    borderRadius: 8,
    width: '100%',
  },
  label: {
    fontFamily: 'Poppins-SemiBold',
    fontSize: 15,
    color: '#000000',
  },
});
