import React from 'react';
import {
  View,
  Text,
  StyleSheet,
  Image,
  StatusBar,
  Linking,
  TouchableWithoutFeedback,
} from 'react-native';
import VersionNumber from 'react-native-version-number';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
export default class AskForPermissions extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    const {onPress, onPressCancel} = this.props;
    return (
      <View style={styles.container}>
        <StatusBar hidden />
        <Image
          source={require('../../assets/update.jpg')}
          style={styles.updateImage}
          resizeMode="contain"
        />
        <Text style={styles.updateText}>
          This app collects location data to generate entry and exit reports of
          employees and hence monitor a employee's movement during login hours.
          During login hours, the app will track your movement and will collect
          the location data even if the app runs in background or if it is
          closed.
        </Text>
        <View style={{marginTop: 10}}>
          <View style={styles.center}>
            <TouchableWithoutFeedback onPress={onPress}>
              <View style={styles.buttonContainer}>
                <Text style={styles.footerText}>Agree</Text>
              </View>
            </TouchableWithoutFeedback>
            <TouchableWithoutFeedback onPress={onPressCancel}>
              <View style={styles.buttonContainer}>
                <Text style={styles.footerText}>No, thanks</Text>
              </View>
            </TouchableWithoutFeedback>
          </View>
        </View>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#ffffff',
  },
  updateImage: {
    width: 350,
    height: 350,
  },
  updateText: {
    fontFamily: 'Poppins-SemiBold',
    color: '#ff4f5a',
    textAlign: 'center',
  },
  center: {
    justifyContent: 'space-around',
    alignItems: 'center',
    flexDirection: 'row',
  },
  buttonContainer: {
    width: wp('40'),
    backgroundColor: '#f05760',
    justifyContent: 'center',
    alignItems: 'center',
    padding: 12,
    borderRadius: 30,
    marginTop: hp('1'),
  },
  footerText: {
    fontFamily: 'Poppins-Regular',
    fontSize: 14,
    color: '#ffffff',
  },
});
