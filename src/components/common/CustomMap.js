import React, {Children} from 'react';
import {StyleSheet, Text, View} from 'react-native';

import MapView, {Marker} from 'react-native-maps';
import {Icon} from 'native-base';
import {Avatar} from 'react-native-elements';
import CustomLabel from './CustomLabel';
import {Colors} from '../../utils/configs/Colors';
const LAT_DELTA = 0.05;
const LONG_DELTA = 0.05;
const CustomMap = (props) => {
  const styles = StyleSheet.create({
    map: {
      height: props.height || 300,
      width: props.width || '100%',
    },
  });
  return (
    <View style={styles.map}>
      <MapView
        region={{
          latitude: parseFloat(props.latitude),
          longitude: parseFloat(props.longitude),
          latitudeDelta: LAT_DELTA,
          longitudeDelta: LONG_DELTA,
        }}
        //onRegionChange={this.onRegionChange}
        style={{width: '100%', height: '100%'}}
        //mapType={'satellite'}
        followUserLocation={true}
        cacheEnabled={true}
        initialRegion={{
          latitude: parseFloat(props.latitude),
          longitude: parseFloat(props.longitude),
          latitudeDelta: LAT_DELTA,
          longitudeDelta: LONG_DELTA,
        }}
        scrollEnabled={props.scrollEnabled || false}
        zoomEnabled={true}>
        <Marker
          //draggable
          coordinate={{
            latitude: parseFloat(props.latitude),
            longitude: parseFloat(props.longitude),
            latitudeDelta: LAT_DELTA,
            longitudeDelta: LONG_DELTA,
          }}
          title={props.markerTitle || 'Customer location'}>
          {!props.appointment && (
            <View style={{alignItems: 'center', justifyContent: 'center'}}>
              <CustomLabel
                containerStyle={{
                  borderWidth: 2,
                  borderColor: Colors.header,
                  padding: 2,
                }}
                title={'You are here'}
              />
              <Avatar
                rounded
                source={{uri: props.photo || ''}}
                size={'small'}
              />
            </View>
          )}
        </Marker>
        {props.radius && (
          <MapView.Circle
            key={(props.latitude + props.longitude).toString()}
            center={{
              latitude: parseFloat(props.latitude),
              longitude: parseFloat(props.longitude),
              latitudeDelta: LAT_DELTA,
              longitudeDelta: LONG_DELTA,
            }}
            radius={parseFloat(props.radius) || 25}
            strokeWidth={2}
            strokeColor={'red'}
            fillColor={'lightpink'}
          />
        )}
        {props.children}
      </MapView>
    </View>
  );
};

export default CustomMap;
