import React from 'react';
import {StyleSheet, Text, View} from 'react-native';
import CalendarStrip from 'react-native-calendar-strip';
const CalenderStrip = () => {
  return (
    <View style={{flex: 1}}>
      <CalendarStrip
        scrollable
        calendarAnimation={{type: 'sequence', duration: 30}}
        daySelectionAnimation={{
          type: 'background',
          duration: 300,
          highlightColor: Colors.header,
        }}
        style={{
          height: 100,

          paddingBottom: 10,
        }}
        calendarHeaderStyle={{
          color: 'grey',
          fontSize: 20,
        }}
        calendarColor={Colors.secondary}
        dateNumberStyle={{
          color: 'black',
        }}
        iconLeft={require('../../assets/left.png')}
        dateNameStyle={{
          color: 'grey',
        }}
        //maxDayComponentSize={100}
        dayComponentHeight={50}
        iconContainer={{flex: 0.1}}
        highlightDateNameStyle={{color: 'white'}}
        highlightDateNumberStyle={{color: 'white'}}
        highlightDateContainerStyle={{
          backgroundColor: Colors.header,
        }}
        iconRight={require('../../assets/right.png')}
        selectedDate={moment()}
        maxDate={moment()}
        onDateSelected={(selectedDate) => {
          this.setState({selectedDate});
          this.setState({
            formattedDate: selectedDate.format('YYYY-MM-DD'),
          });
        }}
        useIsoWeekday={false}
      />
    </View>
  );
};

export default CalenderStrip;

const styles = StyleSheet.create({});
