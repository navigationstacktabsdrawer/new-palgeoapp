import React, {Component} from 'react';
import {Text, TouchableOpacity, StyleSheet, View} from 'react-native';
export class CustomTabs extends Component {
  constructor(props) {
    super(props);
  }
  render() {
    let {
      onPress,
      title,
      textSize,
      color,
      width,
      tab1,
      tab2,
      tab3,
      tab4,
      tab5,
      tab1Width,
      tab2Width,
      tab3Width,
      tab4Width,
      tab5Width,
      ActiveTab,
      backgroundColor,
      borderRadius,
      height,
    } = this.props;
    //console.log(ActiveTab);
    return (
      <View
        style={{
          width: width,
          height: height,
          alignItems: 'center',
          alignSelf: 'center',
          flexDirection: 'row',
          borderRadius: borderRadius,
          padding: 5,
          backgroundColor: backgroundColor,
          marginVertical: 10,
        }}>
        {tab1 ? (
          <TouchableOpacity
            onPress={() => {
              onPress(tab1);
            }}
            style={{
              //flex: 1,
              width: tab1Width,
              height: '100%',
              justifyContent: 'center',
              alignItems: 'center',
              borderRadius: borderRadius,
              backgroundColor: ActiveTab == tab1 ? color : 'transparent',
            }}>
            <Text
              style={[
                styles.text,
                {
                  fontSize: textSize,
                  color: ActiveTab == tab1 ? 'white' : color,
                },
              ]}>
              {tab1}
            </Text>
          </TouchableOpacity>
        ) : null}
        {tab2 ? (
          <TouchableOpacity
            onPress={() => {
              onPress(tab2);
            }}
            style={{
              width: tab2Width,
              height: '100%',
              justifyContent: 'center',
              alignItems: 'center',
              borderRadius: borderRadius,
              backgroundColor: ActiveTab == tab2 ? color : 'transparent',
            }}>
            <Text
              style={[
                styles.text,
                {
                  fontSize: textSize,
                  color: ActiveTab == tab2 ? 'white' : color,
                },
              ]}>
              {tab2}
            </Text>
          </TouchableOpacity>
        ) : null}
        {tab3 ? (
          <TouchableOpacity
            onPress={() => {
              onPress(tab3);
            }}
            style={{
              width: tab3Width,
              height: '100%',
              justifyContent: 'center',
              alignItems: 'center',
              borderRadius: borderRadius,
              backgroundColor: ActiveTab == tab3 ? color : 'transparent',
            }}>
            <Text
              style={[
                styles.text,
                {
                  fontSize: textSize,
                  color: ActiveTab == tab3 ? 'white' : color,
                },
              ]}>
              {tab3}
            </Text>
          </TouchableOpacity>
        ) : null}
        {tab4 ? (
          <TouchableOpacity
            onPress={() => {
              onPress(tab4);
            }}
            style={{
              width: tab4Width,
              height: '100%',
              justifyContent: 'center',
              alignItems: 'center',
              borderRadius: borderRadius,
              backgroundColor: ActiveTab == tab4 ? color : 'transparent',
            }}>
            <Text
              style={[
                styles.text,
                {
                  fontSize: textSize,
                  color: ActiveTab == tab4 ? 'white' : color,
                },
              ]}>
              {tab4}
            </Text>
          </TouchableOpacity>
        ) : null}
        {tab5 ? (
          <TouchableOpacity
            onPress={() => {
              onPress(tab5);
            }}
            style={{
              width: tab5Width,
              height: '100%',
              justifyContent: 'center',
              alignItems: 'center',
              borderRadius: borderRadius,
              backgroundColor: ActiveTab == tab5 ? color : 'transparent',
            }}>
            <Text
              style={[
                styles.text,
                {
                  fontSize: textSize,
                  color: ActiveTab == tab5 ? 'white' : color,
                },
              ]}>
              {tab5}
            </Text>
          </TouchableOpacity>
        ) : null}
      </View>
    );
  }
}
const styles = StyleSheet.create({
  text: {
    fontFamily: 'Poppins-Regular',
    fontSize: 15,
    color: 'white',
    paddingHorizontal: 5,
  },
});
