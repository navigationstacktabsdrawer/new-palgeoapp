import React from 'react';
import {ScrollView, StyleSheet, Text, View} from 'react-native';
import {Icon} from 'native-base';
import Modal from 'react-native-modal';

const CustomModal = (props) => {
  return (
    <Modal
      onBackButtonPress={props.deleteIconPress}
      isVisible={props.isVisible}>
      <View style={styles.modalContainer}>
        <View
          style={{
            position: 'absolute',
            right: 0,
            top: -10,
            backgroundColor: 'red',
            borderRadius: 20,
          }}>
          {!props.doNotShowDeleteIcon && (
            <Icon
              name="times-circle"
              type="FontAwesome5"
              style={{color: '#fff'}}
              onPress={props.deleteIconPress}
            />
          )}
        </View>
        <ScrollView>
          {props.title && (
            <View style={styles.center}>
              <Text style={[styles.modalHeader, props.headerTextStyle]}>
                {props.title}
              </Text>
            </View>
          )}
          {props.subTitle && (
            <View style={styles.center}>
              <Text style={[styles.modalHeader, props.headerTextStyle]}>
                {props.subTitle}
              </Text>
            </View>
          )}
          {props.children}
        </ScrollView>
      </View>
    </Modal>
  );
};

export default CustomModal;

const styles = StyleSheet.create({
  center: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  headingContainer: {
    marginTop: 8,
  },
  modalContainer: {
    backgroundColor: '#fff',
    borderRadius: 10,
    padding: '5%',
  },
  modalHeader: {
    fontFamily: 'Poppins-SemiBold',
    fontSize: 13,
  },
});
