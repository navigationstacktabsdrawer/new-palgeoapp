import React from 'react';
import {StyleSheet, Text, View} from 'react-native';
import {Textarea} from 'native-base';

const CustomModalTextArea = (props) => {
  return (
    <View style={styles.item1}>
      <Textarea
        rowSpan={props.rowSpan || 3}
        bordered={props.bordered || true}
        style={styles.textarea}
        value={props.value}
        onChangeText={props.onChangeText}
        disabled={props.disabled || false}
        blurOnSubmit={true}
      />
    </View>
  );
};

export default CustomModalTextArea;

const styles = StyleSheet.create({
  item1: {
    borderRadius: 8,
    backgroundColor: '#ffffff',
    borderColor: '#f1f1f1',
    borderWidth: 1.5,
  },
  textarea: {
    borderLeftWidth: 0,
    borderBottomWidth: 0,
    borderTopWidth: 0,
    borderRightWidth: 0,
    borderRadius: 8,
  },
});
