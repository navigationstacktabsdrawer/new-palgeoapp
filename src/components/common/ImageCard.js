import React from 'react';
import {StyleSheet, Text, View, Image, TouchableOpacity} from 'react-native';
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';
import {Colors} from '../../utils/configs/Colors';
import CustomLabel from './CustomLabel';

const ImageCard = (props) => {
  return (
    <TouchableOpacity onPress={props.onPress}>
      <View
        style={{
          //padding: 8,

          borderColor: Colors.overlay,

          borderRadius: 10,
          marginBottom: 10,
          borderWidth: 2,
          // width: props.width || wp('40'),
          // height: props.height || 150,
        }}>
        <View
          style={{
            width: props.width || wp('60'),
            height: props.height || 150,
          }}>
          <Image
            source={props.source}
            style={[{width: '100%', height: '100%'}, props.imageStyle]}
            resizeMode="contain"
          />
        </View>
        {props.title && (
          <CustomLabel labelStyle={{textAlign: 'center'}} title={props.title} />
        )}
      </View>
    </TouchableOpacity>
  );
};

export default ImageCard;

const styles = StyleSheet.create({});
