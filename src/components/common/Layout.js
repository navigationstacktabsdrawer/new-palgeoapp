import {Container, Content} from 'native-base';
import React from 'react';
import {StyleSheet, Text, View} from 'react-native';
import SubHeader from './SubHeader';

const Layout = (props) => {
  return (
    <Container>
      <SubHeader
        title={props.headerTitle}
        showBack={props.goBack || true}
        backScreen={props.backScreen || 'Home'}
        showBell={false}
        navigation={props.navigation}
      />
      {!props.noContentTag ? (
        <Content
          contentContainerStyle={{
            width: props.width || '95%',
            alignSelf: 'center',
          }}>
          {props.children}
        </Content>
      ) : (
        <View style={{width: props.width || '95%', alignSelf: 'center'}}>
          {props.children}
        </View>
      )}
    </Container>
  );
};

export default Layout;

const styles = StyleSheet.create({});
