import React, {useState} from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Modal,
  Dimensions,
  Image,
} from 'react-native';
import {Row} from 'native-base';
import {RNCamera} from 'react-native-camera';
import {Icon} from 'react-native-elements';
import {Colors} from '../../utils/configs/Colors';
import {useIsFocused} from '@react-navigation/core';
import CustomLabel from './CustomLabel';
import {useEffect} from 'react';
import {PermissionsAndroid} from 'react-native';

const CameraComp = (props) => {
  const [torch, setTorch] = useState(false);

  const isFocused = useIsFocused();
  console.log('isFocused', isFocused);

  const styles = StyleSheet.create({
    container: {
      flex: 1,
      flexDirection: 'column',
      zIndex: 1,
    },
    preview: {
      flex: 1,
    },
    capture: {
      flex: 0,
      width: Dimensions.get('screen').width,
      backgroundColor: '#f05760',
      padding: 15,
      paddingHorizontal: 20,
      alignSelf: 'center',
      justifyContent: 'flex-end',
      alignItems: 'center',
    },
  });
  return (
    <Modal
      visible={props.visible}
      onRequestClose={props.onClose}
      onDismiss={props.onClose}
      style={styles.container}>
      {props.reduce && props.pressed && (
        <View
          style={{
            height: Dimensions.get('screen').height * 0.4,
            marginBottom: 30,
            alignItems: 'center',
            justifyContent: 'center',
          }}>
          <Image
            source={require('../../assets/update.jpg')}
            style={{
              width: 100,
              height: 100,
              margin: 0,
            }}
            resizeMode="contain"
          />
          <Text
            style={{
              fontSize: 20,
              textAlign: 'center',
              fontFamily: 'Poppins-SemiBold',
            }}>
            {props.question} and {props.text}
          </Text>
        </View>
      )}

      {isFocused && (
        <RNCamera
          style={styles.preview}
          type={
            props.onBarCodeRead || props.back
              ? RNCamera.Constants.Type.back
              : RNCamera.Constants.Type.front
          }
          flashMode={
            torch
              ? RNCamera.Constants.FlashMode.torch
              : RNCamera.Constants.FlashMode.auto
          }
          onBarCodeRead={props.onBarCodeRead}
          onRecordingStart={props.onRecordingStart}
          onRecordingEnd={props.onRecordingEnd}
          captureAudio={false}
          playSoundOnRecord={true}
          onPictureTaken={props.onPictureTaken}
          onCameraReady={() => console.log('Camera is ready now!')}
          onMountError={(error) =>
            alert(
              'Error mounting camera:' +
                error.message +
                '. Please restart the app again.',
            )
          }
          androidCameraPermissionOptions={{
            title: 'Permission to use camera',
            message: 'We need your permission to use your camera',
            buttonPositive: 'Ok',
            buttonNegative: 'Cancel',
          }}>
          {({camera, status}) => {
            if (status !== 'READY') return <Text>Waiting</Text>;
            return (
              <>
                <View
                  style={{
                    flex: 1,
                    justifyContent: 'flex-end',
                    alignSelf: 'center',
                  }}>
                  <Row>
                    <View
                      style={{
                        width: 30,
                        backgroundColor: '#f05760',
                        marginRight: Dimensions.get('window').width - 30,
                      }}></View>

                    <View
                      style={{
                        width: 30,
                        backgroundColor: '#f05760',
                      }}></View>
                  </Row>
                  {!props.onBarCodeRead && (
                    <Icon
                      name={torch ? 'md-flash' : 'md-flash-off'}
                      color={Colors.white}
                      size={30}
                      type={'ionicon'}
                      onPress={() => setTorch(!torch)}
                      style={{margin: 100}}
                    />
                  )}
                  <TouchableOpacity
                    onPress={() => props.onPress(camera)}
                    style={styles.capture}
                    disabled={props.pressed}>
                    <Text
                      style={{
                        fontSize: 17,
                        color: 'white',
                        textTransform: 'uppercase',
                        letterSpacing: 2,
                        fontFamily: 'Poppins-Regular',
                        fontWeight: 'bold',
                      }}>
                      {' '}
                      {props.btnText}{' '}
                    </Text>
                  </TouchableOpacity>
                </View>
              </>
            );
          }}
        </RNCamera>
      )}
    </Modal>
  );
};
export default CameraComp;
